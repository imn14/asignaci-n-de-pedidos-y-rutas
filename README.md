# Asignación de Paradas y Rutas

##Descripción

Este proyecto software consiste en la obtención de un programa que pueda crear rutas de transporte y asignarlas recursos para que sean cubiertas.

Para ello se ha utilizado Java para crear la lógica de negocio. 
La manera de interactuar entre el usuario y la aplicación es mediante un servidor, utilizando para ello JSP.

**
Este proyecto ha sido presentado como trabajo de fin de carrera de Grado en Ingeniería Informática por la Universidad de Burgos.
**


##Autor
Ismael Moral Núñez

##Tutora
María Belén Vaquerizo García