<!DOCTYPE html>
<html>
<head>
<title>Selecci�n de variables</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<%
		String localizacion = request.getParameter("localizacion");
		String distancias = request.getParameter("distancias");
		String demandas = request.getParameter("demandas");
		String flota = request.getParameter("flota");
		String paradas = request.getParameter("paradas");
		String tiempo = request.getParameter("tiempo");
		String log = request.getParameter("log");
		String logdetallado = request.getParameter("logdetallado");
		String save = request.getParameter("save");
		Boolean manual = Boolean.parseBoolean(request
				.getParameter("manual"));
		Boolean mejmanual = Boolean.parseBoolean(request
				.getParameter("mejmanual"));
		String saverutas = request.getParameter("saverutas");
		String semi = request.getParameter("semi");
		Boolean rutassemi = Boolean.parseBoolean(request
				.getParameter("rutassemi"));
		Boolean mejsemi = Boolean.parseBoolean(request
				.getParameter("mejsemi"));
		if (manual && rutassemi) {
			out.print("NO PUEDE MARCARSE RUTAS SEMIAUTOM�TICAS Y MANUALES A LA VEZ.");
	%><%@ include file="archivos.html"%>
	<%
		} else {
			session.setAttribute("localizacion", localizacion);
			session.setAttribute("distancias", distancias);
			session.setAttribute("demandas", demandas);
			session.setAttribute("flota", flota);
			session.setAttribute("paradas", paradas);
			session.setAttribute("tiempo", tiempo);
			session.setAttribute("log", log);
			session.setAttribute("logdetallado", logdetallado);
			session.setAttribute("save", save);
			session.setAttribute("manual", manual);
			session.setAttribute("mejmanual", mejmanual);
			session.setAttribute("saverutas", saverutas);
			session.setAttribute("semi", semi);
			session.setAttribute("mejsemi", mejsemi);
			session.setAttribute("rutassemi", rutassemi);
			if (localizacion.equals("pais")) {
				out.print("�mbito Nacional");
	%>
	<form method="post" action="crea.jsp">
		Distancia m�xima de recorrido por sesi�n<input type="number"
			name="distmaxdia" min="5" max="150" step="1" value="60"><br>
		N�mero de iteraciones: <input type="number" name="numiter" min="1"
			max="30" step="1" value="10"><br> M�nima demanda
		cubierta: <input type="number" name="demper" min="0" max="1"
			step="0.05" value="0.8"><br> Se ha de volver al punto de
		origen: <input type="radio" name="volver" value="true"> Si <input
			type="radio" name="volver" value="false"> No<br> <input
			type="radio" name="carga" value="true"> Carga <input
			type="radio" name="carga" value="false"> Descarga<br>
		Tiempo medio de descarga de cada unidad<input type="number"
			name="tiemedpar" min="0" max="30" step="1" value="1"><br>
		<input type="submit" value="Enviar">
	</form>
	<%
		} else if (localizacion.equals("ciudad") && !manual
					&& !rutassemi) {
				out.print("�mbito Local");
	%>
	<form method="post" action="crea.jsp">
		N�mero m�ximo de paradas permitidas: <input type="number"
			name="maxpara" min="1" max="30" step="1" value="20"><br>
		N�mero de lineas: <input type="number" name="lintot" min="1" max="10"
			step="1" value="4"><br> Tiempo m�ximo de l�nea: <input
			type="number" name="tiemax" min="1" max="150" step="1" value="60"><br>
		Tiempo m�nimo de l�nea: <input type="number" name="tiemin" min="0"
			max="20" step="1" value="10"><br> N�mero de iteraciones:
		<input type="number" name="numiter" min="1" max="30" step="1"
			value="10"><br> M�nima demanda cubierta: <input
			type="number" name="demper" min="0" max="1" step="0.05" value="0.8"><br>
		Se ha de volver al punto de origen: <input type="radio" name="volver"
			value="true"> Si <input type="radio" name="volver"
			value="false"> No<br> <input type="radio" name="carga"
			value="true"> Carga <input type="radio" name="carga"
			value="false"> Descarga<br> Tiempo medio de descarga de
		cada unidad<input type="number" name="tiemedpar" min="0" max="30"
			step="1" value="1"><br> <input type="submit"
			value="Enviar">
	</form>
	<%
		} else if (localizacion.equals("ciudad") && manual) {
				out.print("�mbito Local, Rutas Manuales");
	%>
	<form method="post" action="crea.jsp">
		N�mero m�ximo de paradas permitidas: <input type="number"
			name="maxpara" min="1" max="30" step="1" value="20"><br>
		Tiempo m�ximo de l�nea: <input type="number" name="tiemax" min="1"
			max="150" step="1" value="60"><br> Tiempo m�nimo de
		l�nea: <input type="number" name="tiemin" min="0" max="20" step="1"
			value="10"><br> M�nima demanda cubierta: <input
			type="number" name="demper" min="0" max="1" step="0.05" value="0.8"><br>
		<input type="radio" name="carga" value="true"> Carga <input
			type="radio" name="carga" value="false"> Descarga<br>
		Tiempo medio de descarga de cada unidad <input type="number"
			name="tiemedpar" min="0" max="30" step="1" value="1"><br>
		<input type="submit" value="Enviar">
	</form>
	<%
		} else if (localizacion.equals("ciudad") && rutassemi

			) {
				out.print("�mbito Local, Rutas Semiautom�ticas");
	%>
	<form method="post" action="crea.jsp">
		N�mero m�ximo de paradas permitidas: <input type="number"
			name="maxpara" min="1" max="30" step="1" value="20"><br>
		Tiempo m�ximo de l�nea: <input type="number" name="tiemax" min="1"
			max="150" step="1" value="60"><br> Tiempo m�nimo de
		l�nea: <input type="number" name="tiemin" min="0" max="20" step="1"
			value="10"><br> N�mero de iteraciones: <input
			type="number" name="numiter" min="1" max="30" step="1" value="10"><br>
		M�nima demanda cubierta: <input type="number" name="demper" min="0"
			max="1" step="0.05" value="0.8"><br> <input type="radio"
			name="carga" value="true"> Carga <input type="radio"
			name="carga" value="false"> Descarga<br> Tiempo medio de
		descarga de cada unidad<input type="number" name="tiemedpar" min="0"
			max="30" step="1" value="1"><br> <input type="submit"
			value="Enviar">
	</form>
	<%
		}
		}
	%>

	<a href="index.html"><font color="blue">Inicio</font></a>
</body>
</html>
