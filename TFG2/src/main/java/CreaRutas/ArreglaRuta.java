package CreaRutas;

import java.util.ArrayList;

/**
 * Clase que se encarga de arreglar las rutas sin asignación de transporte.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class ArreglaRuta {

	/**
	 * Objeto Grasp con los datos del problema.
	 */
	private Grasp grasp;

	/**
	 * ArrayList con los errores de las rutas.
	 */
	private ArrayList<ArrayList<Integer>> errores;

	/**
	 * Array con el tiempo máximo de cada ruta total.
	 */
	private int[] distanciamax;

	/**
	 * Array con la capaciadad máxima de cada ruta total.
	 */
	private int[] capacidadmax;

	/**
	 * Demanda cubierta en la anterior asignación de transportes.
	 */
	private int demandacubiertaanterior;

	/**
	 * Constructor, crea una nueva instancia del tipo ArreglaRuta.
	 * 
	 * @param ordenaRutas
	 *            Recibe un ArrayList con el orden de la rutas.
	 * @param errores
	 *            Recibe un ArrayList con los errores de las rutas.
	 * @param grasp
	 *            Recibe el grasp con las rutas creadas y el resto de los datos.
	 * @param ruta
	 *            Localización del fichero donde se guarda el resultado.
	 * @param rutarutas
	 *            Localización del fichero donde se guardan las rutas creadas.
	 * @param distanciamax
	 *            Recibe un array con el tiempo máximo de cada ruta total.
	 * @param capacidadmax
	 *            Recibe un array con la capacidad máxima de cada ruta total.
	 * @param demandacubiertaanterior
	 *            Recibe un entero con la demanda cubierta en la anterior
	 *            asignación de transportes.
	 */
	public ArreglaRuta(ArrayList<Integer> ordenaRutas,
			ArrayList<ArrayList<Integer>> errores, Grasp grasp, String ruta,
			String rutarutas, int[] distanciamax, int[] capacidadmax,
			int demandacubiertaanterior) {
		this.grasp = grasp;
		this.errores = errores;
		this.distanciamax = distanciamax;
		this.capacidadmax = capacidadmax;
		this.demandacubiertaanterior = demandacubiertaanterior;
		arregla(ruta, rutarutas);
	}// Constructor

	/**
	 * Clase que se encarga de gestionar la creación de rutas que solucionen los
	 * fallos.
	 * 
	 * @param save
	 *            Localización del fichero donde se guarda el resultado.
	 * @param saverutas
	 *            Localización del fichero donde se guardan las rutas creadas.
	 */
	private void arregla(String save, String saverutas) {
		ArrayList<Integer> rutasACambiar = new ArrayList<Integer>();
		int i = 0; // Se indican las rutas sin cubrir para que sean modoficadas
		for (ArrayList<Integer> error : errores) {
			if (!error.contains(-4)) {
				rutasACambiar.add(i);
				rutasACambiar.add(i + 1);
			}// if
			i += 2;
		}// for
		Grasp graspaux = grasp;
		graspaux.logdetalladostring = "\r\n\r\nMEJORA DE RUTAS\r\n\r\n";
		graspaux.logstring = "\r\n\r\nMEJORA DE RUTAS\r\n\r\n";
		// Se duplican las matrices de demandaporlinea, parescubiertos y
		// distancias para compararlas más adelante.
		ArrayList<ArrayList<Integer>> rutasfinalesaux = graspaux
				.getRutasFinales();
		int[] demandaporlineaaux = graspaux.getDemandaPorLinea();
		int[] demandaporlineaaux0 = graspaux.getDemandaPorLinea().clone();
		int[][] distanciasaux = new int[graspaux.getDatosRed().getNumeroNodos()][graspaux
				.getDatosRed().getNumeroNodos()];
		for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
			for (int j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
				distanciasaux[i][j] = graspaux.distanciaoriginal[i][j];
			}
		}// for
		int[][] parescubiertosaux = graspaux.getParescubiertos();
		int[][] parescubiertosaux0 = new int[graspaux.getDatosRed()
				.getNumeroNodos()][graspaux.getDatosRed().getNumeroNodos()];

		for (int j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
			for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
				parescubiertosaux0[j][k] = graspaux.getParescubiertos()[j][k];
			}
		}

		for (int j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
			for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
				if (parescubiertosaux[j][k] == -1) {
					parescubiertosaux[j][k] = 0;
				}
				if (rutasACambiar.contains(parescubiertosaux[j][k] - 1)) {
					parescubiertosaux[j][k] = 0;
				}
			}
		}
		// Si el problema de asignación de transportes fue causado por una
		// restricción de una parada se marcara esa parada como inhabilitada.
		boolean[] paserestriccion = new boolean[3];
		paserestriccion[0] = paserestriccion[1] = paserestriccion[2] = false;
		for (ArrayList<Integer> error : errores) {
			if (error.contains(0) && !error.contains(-4) && !paserestriccion[0]) {
				paserestriccion[0] = true;
				for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
					if (graspaux.getDatosRed().getParadas()[i][0]) {
						for (int j = 0; j < graspaux.getDatosRed()
								.getNumeroNodos(); j++) {
							if (i != j) {
								distanciasaux[i][j] = graspaux.inf;
								distanciasaux[j][i] = graspaux.inf;
							}// if
						}// for
					}// if
				}// for
			}// if
			if (error.contains(1) && !error.contains(-4) && !paserestriccion[1]) {
				paserestriccion[1] = true;
				for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
					if (graspaux.getDatosRed().getParadas()[i][1]) {
						for (int j = 0; j < graspaux.getDatosRed()
								.getNumeroNodos(); j++) {
							if (i != j) {
								distanciasaux[i][j] = graspaux.inf;
								distanciasaux[j][i] = graspaux.inf;
							}// if
						}// for
					}// if
				}// for
			}// if
			if (error.contains(2) && !error.contains(-4) && !paserestriccion[2]) {
				paserestriccion[2] = true;
				for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
					if (graspaux.getDatosRed().getParadas()[i][2]) {
						for (int j = 0; j < graspaux.getDatosRed()
								.getNumeroNodos(); j++) {
							if (i != j) {
								distanciasaux[i][j] = graspaux.inf;
								distanciasaux[j][i] = graspaux.inf;
							}// if
						}// for
					}// if
				}// for
			}// if
		}// for
			// Se duplica el ArrayList de rutasfinales para comprarlo más
			// adelante.
		ArrayList<ArrayList<Integer>> ruf = (ArrayList<ArrayList<Integer>>) grasp
				.getRutasFinales().clone();
		DistanciasMinimas floyd = new DistanciasMinimas(graspaux.getDatosRed()
				.getNumeroNodos(), distanciasaux);
		floyd.floyd();

		ArrayList<Integer> paradasañadidas = new ArrayList<Integer>();
		for (i = 0; i < errores.size(); i++) {
			if (errores.get(i).contains(-2)) {
				// El error -2 indica que la ruta no cubre demanda, por lo que
				// reducimos su tiempo máximo, esperando que así no sobrepase el
				// tiempo máximo real.
				if (!((distanciamax[i] - graspaux.tmaxlinea / 4) < graspaux.tminlinea)) {
					distanciamax[i] -= graspaux.tmaxlinea / 4;
				}
			}
			if (errores.get(i).contains(-3)) {
				// El error -3 indica que la ruta cubre demasiada demanda, por
				// lo que redicimos su demanda máxima, esperando que así un
				// transporte pueda ser asignado aunque sea más pequeño.
				int m = 0;
				for (TipoCamion camion : graspaux.getDatosRed().getCamiones()) {
					if (camion.getCapacidad() > m
							&& camion.getCapacidad() < capacidadmax[i]) {
						m = camion.getCapacidad();
					} // if
				}// for
				if (m > 0) {
					capacidadmax[i] = m;
				}
			}// if
		}// for
			// Se vuelven a calcular las rutas a modificar.
		int[] extremos = new int[errores.size()];
		int[] pcentrosalida = new int[errores.size()];
		int[] pcentrollegada = new int[errores.size()];
		for (i = 0; i < errores.size(); i++) {
			extremos[i] = -1;
			pcentrollegada[i] = -1;
			pcentrosalida[i] = -1;
			if (!errores.get(i).contains(-4)
					&& !(distanciamax[i] < graspaux.tminlinea)) {
				if (graspaux instanceof GraspCarga) {
					// Ordeno las paradas por demanda urgente
					ArrayList<Integer> urgentesordenadas = new ArrayList<Integer>();
					int sumamaxima;
					do {
						sumamaxima = 0;
						int parada = -1;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (graspaux.getDatosRed().getDemandaUrgente()[j][k] == true
										&& !urgentesordenadas.contains(k)
										&& parescubiertosaux[j][k] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[j][k];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								parada = k;
							}
						}// for
						if (parada != -1)
							urgentesordenadas.add(parada);
					} while (sumamaxima > 0);
					// Calculamos el extremo (la parada compartida por las dos
					// rutas parciales).
					sumamaxima = 0;
					for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
						if (!paradasañadidas.contains(k)) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[j][k] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[j][k];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								extremos[i] = k;
							}
						}
					}// for
					if (extremos[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[j][k] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[j][k];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								extremos[i] = k;
							}
						}// for
					}// if
					if (urgentesordenadas.size() > 0) {
						if (urgentesordenadas.contains(extremos[i])) {
							urgentesordenadas.remove(urgentesordenadas
									.indexOf(extremos[i]));
						} else {
							extremos[i] = urgentesordenadas.get(0);
							urgentesordenadas.remove(0);
						}
					}// if
						// Calculamos la parada centro llegada (pcentrollegada)
					sumamaxima = 0;
					for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
						if (k != extremos[i]
								&& !paradasañadidas.contains(k)
								&& !(capacidadmax[i] < graspaux.getDatosRed()
										.getDemanda()[extremos[i]][k])
								&& !(distanciamax[i] < distanciasaux[extremos[i]][k])
								&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] > 0
								&& parescubiertosaux[extremos[i]][k] < 1) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[j][k] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[j][k];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								pcentrollegada[i] = k;
							}
						}// if
					}// for
					if (pcentrollegada[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (k != extremos[i]
									&& !paradasañadidas.contains(k)
									&& !(capacidadmax[i] < graspaux
											.getDatosRed().getDemanda()[extremos[i]][k])
									&& !(distanciamax[i] < distanciasaux[extremos[i]][k])) {
								int suma = 0;
								for (int j = 0; j < graspaux.getDatosRed()
										.getNumeroNodos(); j++) {
									if (parescubiertosaux[j][k] < 1) {
										suma += graspaux.getDatosRed()
												.getDemanda()[j][k];
									}// if
								}// for
								if (sumamaxima < suma) {
									sumamaxima = suma;
									pcentrollegada[i] = k;
								}
							}// if
						}// for
					}
					if (pcentrollegada[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (k != extremos[i]
									&& !(capacidadmax[i] < graspaux
											.getDatosRed().getDemanda()[extremos[i]][k])
									&& !(distanciamax[i] < distanciasaux[extremos[i]][k])) {
								int suma = 0;
								for (int j = 0; j < graspaux.getDatosRed()
										.getNumeroNodos(); j++) {
									if (parescubiertosaux[j][k] < 1) {
										suma += graspaux.getDatosRed()
												.getDemanda()[j][k];
									}// if
								}// for
								if (sumamaxima < suma) {
									sumamaxima = suma;
									pcentrollegada[i] = k;
								}
							}// if
						}// for
					}// if
					if (urgentesordenadas.size() > 0) {
						if (urgentesordenadas.contains(pcentrollegada[i])) {
							urgentesordenadas.remove(urgentesordenadas
									.indexOf(pcentrollegada[i]));
						} else if (urgentesordenadas.get(0) != extremos[i]
								&& !(capacidadmax[i] < graspaux.getDatosRed()
										.getDemanda()[extremos[i]][urgentesordenadas
										.get(0)])
								&& !(distanciamax[i] < distanciasaux[extremos[i]][urgentesordenadas
										.get(0)])) {
							pcentrollegada[i] = urgentesordenadas.get(0);
							urgentesordenadas.remove(0);
						}
					}// if
					if (pcentrollegada[i] == -1) {
						int maximadistancia = 0;
						int maxparada = -1;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (maximadistancia < distanciasaux[extremos[i]][k]
									&& k != extremos[i]
									&& parescubiertosaux[extremos[i]][k] < 1
									&& !paradasañadidas.contains(k)
									&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] != 0
									&& distanciasaux[extremos[i]][k] <= distanciamax[i]
									&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] <= capacidadmax[i]) {
								maximadistancia = distanciasaux[extremos[i]][k];
								maxparada = k;
							}// if
						}// for
						pcentrollegada[i] = maxparada;
						if (pcentrollegada[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[extremos[i]][k]
										&& k != extremos[i]
										&& parescubiertosaux[extremos[i]][k] < 1
										&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] != 0
										&& distanciasaux[extremos[i]][k] <= distanciamax[i]
										&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[extremos[i]][k];
									maxparada = k;
								}// if
							}// for
							pcentrollegada[i] = maxparada;
						}// if
					}// if
						// Calculamos la parada centro salida (pcentrosalida)
					if (graspaux.volver)
						pcentrosalida[i] = pcentrollegada[i];
					else {
						int maximadistancia = 0;
						int maxparada = -1;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (maximadistancia < distanciasaux[k][extremos[i]]
									&& k != extremos[i]
									&& parescubiertosaux[k][extremos[i]] < 1
									&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] != 0
									&& distanciasaux[k][extremos[i]]
											+ distanciasaux[extremos[i]][pcentrollegada[i]] >= graspaux.tminlinea
									&& distanciasaux[k][extremos[i]]
											+ distanciasaux[extremos[i]][pcentrollegada[i]] <= distanciamax[i]
									&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] <= capacidadmax[i]) {
								maximadistancia = distanciasaux[k][extremos[i]];
								maxparada = k;
							}// if
						}// for
						pcentrosalida[i] = maxparada;
						if (pcentrosalida[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[k][extremos[i]]
										&& k != extremos[i]
										&& parescubiertosaux[k][extremos[i]] < 1
										&& distanciasaux[k][extremos[i]]
												+ distanciasaux[extremos[i]][pcentrollegada[i]] >= graspaux.tminlinea
										&& distanciasaux[k][extremos[i]]
												+ distanciasaux[extremos[i]][pcentrollegada[i]] <= distanciamax[i]
										&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[k][extremos[i]];
									maxparada = k;
								}// if
							}// for
							pcentrosalida[i] = maxparada;
						}// if
						if (pcentrosalida[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[k][extremos[i]]
										&& k != extremos[i]
										&& parescubiertosaux[k][extremos[i]] < 1
										&& distanciasaux[k][extremos[i]]
												+ distanciasaux[extremos[i]][pcentrollegada[i]] >= graspaux.tminlinea
										&& distanciasaux[k][extremos[i]]
												+ distanciasaux[extremos[i]][pcentrollegada[i]] <= graspaux.tmaxlinea
										&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[k][extremos[i]];
									maxparada = k;
								}// if
							}// for
							pcentrosalida[i] = maxparada;
						}// if
					}// else
				} else {
					// Ordeno las paradas por demanda urgente
					ArrayList<Integer> urgentesordenadas = new ArrayList<Integer>();
					int sumamaxima;
					do {
						sumamaxima = 0;
						int parada = -1;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (graspaux.getDatosRed().getDemandaUrgente()[k][j] == true
										&& !urgentesordenadas.contains(k)
										&& parescubiertosaux[k][j] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[k][j];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								parada = k;
							}
						}// for
						if (parada != -1)
							urgentesordenadas.add(parada);
					} while (sumamaxima > 0);
					// Calculamos la parada centro salida (pcentrosalida).
					sumamaxima = 0;
					for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
						if (!paradasañadidas.contains(k)) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[k][j] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[k][j];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								pcentrosalida[i] = k;
							}
						}// if
					}// for
					if (pcentrosalida[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							// if (k != origen && k!=destino) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[k][j] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[k][j];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								pcentrosalida[i] = k;
							}// if
						}// for
					}
					if (urgentesordenadas.size() > 0) {
						if (urgentesordenadas.contains(pcentrosalida[i])) {
							urgentesordenadas.remove(urgentesordenadas
									.indexOf(pcentrosalida[i]));
						} else {
							pcentrosalida[i] = urgentesordenadas.get(0);
							urgentesordenadas.remove(0);
						}
					}// if
						// Calculamos el extremo (la parada compartida por las
						// dos rutas parciales).
					sumamaxima = 0;
					for (int k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
						if (k != pcentrosalida[i]
								&& !paradasañadidas.contains(k)
								&& !(capacidadmax[i] < graspaux.getDatosRed()
										.getDemanda()[pcentrosalida[i]][k])
								&& !(distanciamax[i] < distanciasaux[pcentrosalida[i]][k])
								&& graspaux.getDatosRed().getDemanda()[pcentrosalida[i]][k] > 0
								&& parescubiertosaux[pcentrosalida[i]][k] < 1) {
							int suma = 0;
							for (int j = 0; j < graspaux.getDatosRed()
									.getNumeroNodos(); j++) {
								if (parescubiertosaux[k][j] < 1) {
									suma += graspaux.getDatosRed().getDemanda()[k][j];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								extremos[i] = k;
							}
						}// if
					}// for

					if (extremos[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (k != pcentrosalida[i]
									&& !paradasañadidas.contains(k)
									&& !(capacidadmax[i] < graspaux
											.getDatosRed().getDemanda()[pcentrosalida[i]][k])
									&& !(distanciamax[i] < distanciasaux[pcentrosalida[i]][k])) {
								int suma = 0;
								for (int j = 0; j < graspaux.getDatosRed()
										.getNumeroNodos(); j++) {
									if (parescubiertosaux[k][j] < 1) {
										suma += graspaux.getDatosRed()
												.getDemanda()[k][j];
									}// if
								}// for
								if (sumamaxima < suma) {
									sumamaxima = suma;
									extremos[i] = k;
								}
							}// if
						}// for
					}
					if (extremos[i] == -1) {
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (k != pcentrosalida[i]
									&& !(capacidadmax[i] < graspaux
											.getDatosRed().getDemanda()[pcentrosalida[i]][k])
									&& !(distanciamax[i] < distanciasaux[pcentrosalida[i]][k])) {
								int suma = 0;
								for (int j = 0; j < graspaux.getDatosRed()
										.getNumeroNodos(); j++) {
									if (parescubiertosaux[k][j] < 1) {
										suma += graspaux.getDatosRed()
												.getDemanda()[k][j];
									}// if
								}// for
								if (sumamaxima < suma) {
									sumamaxima = suma;
									extremos[i] = k;
								}// if
							}
						}// for
					}
					if (urgentesordenadas.size() > 0) {
						if (urgentesordenadas.contains(extremos[i])) {
							urgentesordenadas.remove(urgentesordenadas
									.indexOf(extremos[i]));
						} else if (urgentesordenadas.get(0) != pcentrosalida[i]
								&& !(capacidadmax[i] < graspaux.getDatosRed()
										.getDemanda()[pcentrosalida[i]][urgentesordenadas
										.get(0)])
								&& !(distanciamax[i] < distanciasaux[pcentrosalida[i]][urgentesordenadas
										.get(0)])) {
							extremos[i] = urgentesordenadas.get(0);
							urgentesordenadas.remove(0);
						}
					}// if
					if (extremos[i] == -1) {
						int maxparada = -1;
						int maximadistancia = 0;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (maximadistancia < distanciasaux[pcentrosalida[i]][k]
									&& k != pcentrosalida[i]
									&& parescubiertosaux[pcentrosalida[i]][k] < 1
									&& !paradasañadidas.contains(k)
									&& graspaux.getDatosRed().getDemanda()[pcentrosalida[i]][k] != 0
									&& distanciasaux[pcentrosalida[i]][k] <= distanciamax[i]
									&& graspaux.getDatosRed().getDemanda()[pcentrosalida[i]][k] <= capacidadmax[i]) {
								maximadistancia = distanciasaux[pcentrosalida[i]][k];
								maxparada = k;
							}// if
						}// for
						extremos[i] = maxparada;
						if (extremos[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[pcentrosalida[i]][k]
										&& k != pcentrosalida[i]
										&& parescubiertosaux[pcentrosalida[i]][k] < 1
										&& graspaux.getDatosRed().getDemanda()[pcentrosalida[i]][k] != 0
										&& distanciasaux[pcentrosalida[i]][k] <= distanciamax[i]
										&& graspaux.getDatosRed().getDemanda()[pcentrosalida[i]][k] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[pcentrosalida[i]][k];
									maxparada = k;
								}// if
							}// for
							extremos[i] = maxparada;
						}// if
					}// if
						// Calculamos la parada centro llegada (pcentrollegada)
					if (graspaux.volver)
						pcentrollegada[i] = pcentrosalida[i];
					else {
						int maxparada = -1;
						int maximadistancia = 0;
						for (int k = 0; k < graspaux.getDatosRed()
								.getNumeroNodos(); k++) {
							if (maximadistancia < distanciasaux[extremos[i]][k]
									&& k != extremos[i]
									&& parescubiertosaux[extremos[i]][k] < 1
									&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] != 0
									&& distanciasaux[extremos[i]][k]
											+ distanciasaux[pcentrosalida[i]][extremos[i]] >= graspaux.tminlinea
									&& distanciasaux[extremos[i]][k]
											+ distanciasaux[pcentrosalida[i]][extremos[i]] <= distanciamax[i]
									&& graspaux.getDatosRed().getDemanda()[extremos[i]][k] <= capacidadmax[i]) {
								maximadistancia = distanciasaux[extremos[i]][k];
								maxparada = k;
							}// if
						}// for
						pcentrollegada[i] = maxparada;
						if (pcentrollegada[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[k][extremos[i]]
										&& k != extremos[i]
										&& parescubiertosaux[k][extremos[i]] < 1
										&& distanciasaux[extremos[i]][k]
												+ distanciasaux[pcentrosalida[i]][extremos[i]] >= graspaux.tminlinea
										&& distanciasaux[extremos[i]][k]
												+ distanciasaux[pcentrosalida[i]][extremos[i]] <= distanciamax[i]
										&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[extremos[i]][k];
									maxparada = k;
								}// if
							}// for
							pcentrollegada[i] = maxparada;
						}// if
						if (pcentrollegada[i] == -1) {
							for (int k = 0; k < graspaux.getDatosRed()
									.getNumeroNodos(); k++) {
								if (maximadistancia < distanciasaux[k][extremos[i]]
										&& k != extremos[i]
										&& parescubiertosaux[k][extremos[i]] < 1
										&& distanciasaux[extremos[i]][k]
												+ distanciasaux[pcentrosalida[i]][extremos[i]] >= graspaux.tminlinea
										&& distanciasaux[extremos[i]][k]
												+ distanciasaux[pcentrosalida[i]][extremos[i]] <= graspaux.tmaxlinea
										&& graspaux.getDatosRed().getDemanda()[k][extremos[i]] <= capacidadmax[i]) {
									maximadistancia = distanciasaux[extremos[i]][k];
									maxparada = k;
								}// if
							}// for
							pcentrollegada[i] = maxparada;
						}// if
					}// else
				}// else

				if (pcentrosalida[i] != -1 && extremos[i] != -1) {
					if (parescubiertosaux[pcentrosalida[i]][extremos[i]] < 1) {
						parescubiertosaux[pcentrosalida[i]][extremos[i]] = i * 2 + 1;
					}
				}
				if (pcentrollegada[i] != -1 && extremos[i] != -1) {
					if (parescubiertosaux[extremos[i]][pcentrollegada[i]] < 1) {
						parescubiertosaux[extremos[i]][pcentrollegada[i]] = i * 2 + 2;
					}
				}
				paradasañadidas.add(extremos[i]);
				if (graspaux instanceof GraspCarga) {
					paradasañadidas.add(pcentrollegada[i]);
				} else {
					paradasañadidas.add(pcentrosalida[i]);
				}
			}// if
		}// for
			// Se añaden los puntos origen y destino a las rutas parciales.
		for (i = 0; i < rutasfinalesaux.size(); i++) {
			if (rutasACambiar.contains(i) && pcentrollegada[i / 2] != -1
					&& pcentrosalida[i / 2] != -1 && extremos[i / 2] != -1) {
				ArrayList<Integer> ruta = new ArrayList<Integer>();
				if (i % 2 == 0) {
					ruta.add(pcentrosalida[i / 2]);
					ruta.add(extremos[i / 2]);
				} else {
					ruta.add(extremos[i / 2]);
					ruta.add(pcentrollegada[i / 2]);
				}
				demandaporlineaaux[i] = graspaux.getDatosRed().getDemanda()[ruta
						.get(0)][ruta.get(1)];
				ArrayList rutaparcial = new ArrayList();
				rutaparcial.add(ruta);
				if (parescubiertosaux[ruta.get(0)][ruta.get(1)] < 1) {
					parescubiertosaux[ruta.get(0)][ruta.get(1)] = i + 1;
				}
				rutaparcial.add(distanciasaux[ruta.get(0)][ruta.get(1)]
						+ graspaux.getDatosRed().getDemanda()[ruta.get(0)][ruta
								.get(1)] * graspaux.tiempomedioparada);
				rutasfinalesaux.set(i, rutaparcial);
			}
		}// for
		rehacerRutas(distanciasaux, demandaporlineaaux, demandaporlineaaux0,
				rutasfinalesaux, ruf, graspaux, parescubiertosaux,
				parescubiertosaux0, rutasACambiar, floyd, save, saverutas);
	}// arregla

	/**
	 * Clase que se encarga de rehacer las rutas.
	 * 
	 * @param distanciasaux
	 *            Matriz con las distancias entre nodos.
	 * @param demandaporlineaaux
	 *            Array con la demanda cubierta por cada ruta.
	 * @param demandaporlineaaux0
	 *            Matriz con la demanda cubierta por cada ruta en la solución
	 *            anterior.
	 * @param rutasfinalesaux
	 *            ArrayList con las rutas a seguir por los transportes.
	 * @param ruf
	 *            ArrayList con las rutas a seguir por los transportes en la
	 *            solución anterior.
	 * @param graspaux
	 *            Objeto Grasp con las rutas creadas y todos los datos.
	 * @param parescubiertosaux
	 *            Matriz que indica que pares de paradas estan cubiertos y la
	 *            ruta que los cubre.
	 * @param parescubiertosaux0
	 *            Matriz que indica que pares de paradas estan cubiertos y la
	 *            ruta que los cubre en la solución anterior.
	 * @param rutasACambiar
	 *            ArrayList con las rutas a modificar.
	 * @param floyd
	 *            Objeto DistanciasMinimas que calcula la distancia entre
	 *            paradas.
	 * @param save
	 *            Localización del fichero donde se guarda el resultado.
	 * @param saverutas
	 *            Localización del fichero donde se guardan las rutas creadas.
	 */
	private void rehacerRutas(int[][] distanciasaux, int[] demandaporlineaaux,
			int[] demandaporlineaaux0,
			ArrayList<ArrayList<Integer>> rutasfinalesaux,
			ArrayList<ArrayList<Integer>> ruf, Grasp graspaux,
			int[][] parescubiertosaux, int[][] parescubiertosaux0,
			ArrayList<Integer> rutasACambiar, DistanciasMinimas floyd,
			String save, String saverutas) {
		// En t se almacenan los tiempos de las rutas de la solución anterior.
		int[] t = new int[grasp.getRutasFinales().size()];
		for (int i = 0; i < grasp.getRutasFinales().size(); i++) {
			t[i] = grasp.getRutasFinales().get(i).get(1);
		}

		int i = 0, j = 0, k = 0, k0 = 0, incDem = 0, incDem0 = 0, incDis = 0, incDis0 = 0, nIter = 0;
		// atributos con los datos mejores
		int im = 0, jm = 0, km = 0, incDemm = 0, incDism = 0;
		// indican los cocientes y el cociente mejor
		double coc = 0, coc0 = 0, cocm = -graspaux.inf + 1;
		boolean posible;
		// Se van añadiendo paradas mientras su cociente (demanda/distancia) no
		// sea muy malo.
		while (cocm != -graspaux.inf) {
			nIter++;
			graspaux.logdetalladostring += ("\r\nNúmero de iteración :" + nIter);
			graspaux.logdetalladostring += ("\r\n nodo linea sen  pos0   dem0  dis0    coc0 \r\n");
			cocm = -graspaux.inf;
			for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
				for (j = 0; j < rutasfinalesaux.size(); j++) {
					ArrayList<Integer> ruta = new ArrayList<Integer>();
					String[] rutaAux = new String[rutasfinalesaux.size()];
					rutaAux = ("" + rutasfinalesaux.get(j).get(0))
							.split(", |\\[|\\]");
					for (int l = 1; l < rutaAux.length; l++) {
						ruta.add(Integer.parseInt(rutaAux[l]));
					}// for
					posible = false;
					if (graspaux instanceof GraspCarga) {
						if (graspaux.getDatosRed().getDemanda()[i][ruta
								.get(ruta.size() - 1)] > 0) {
							posible = true;
						}
					} else {
						if (graspaux.getDatosRed().getDemanda()[ruta.get(0)][i] > 0) {
							posible = true;
						}
					}// else
					if (posible && rutasACambiar.contains(j)
							&& !ruta.contains(i)) {
						if (ruta.size() < graspaux.maximasparadas) {
							coc0 = -graspaux.inf;
							// Para desde la parada 2 hasta el número de paradas
							// que tenemos en la ruta calculamos la mejor
							// posición para meter una nueva parada.
							for (k = 1; k < ruta.size(); k++) {
								// Calculamos la distancia de la parada anterior
								// (k-1) a la que vamos a insertar (i) + la
								// distancia de la nueva parada (i) a lo que
								// antes era la siguiente parada (k) y le
								// restamos la distancia de k-1 a k.
								if (graspaux instanceof GraspCarga) {
									incDis = distanciasaux[ruta.get(k - 1)][i]
											+ distanciasaux[i][ruta.get(k)]
											- distanciasaux[ruta.get(k - 1)][ruta
													.get(k)]
											+ graspaux.getDatosRed()
													.getDemanda()[i][ruta
													.get(ruta.size() - 1)]
											* graspaux.tiempomedioparada;
									incDem = 0;
									// Si la demanda de i a destino no esta
									// cubierta aumento incDem con la demanda
									// que hay entre este par de paradas.
									if (parescubiertosaux[i][ruta.get(ruta
											.size() - 1)] == 0) {
										incDem = incDem
												+ graspaux.getDatosRed()
														.getDemanda()[i][ruta
														.get(ruta.size() - 1)];
									}
								} else {
									incDis = distanciasaux[ruta.get(k - 1)][i]
											+ distanciasaux[i][ruta.get(k)]
											- distanciasaux[ruta.get(k - 1)][ruta
													.get(k)]
											+ graspaux.getDatosRed()
													.getDemanda()[ruta.get(0)][i]
											* graspaux.tiempomedioparada;
									incDem = 0;
									// Si la demanda de origen a i no esta
									// cubierta aumento incDem con la demanda
									// que hay entre este par de paradas.
									if (parescubiertosaux[ruta.get(0)][i] == 0) {
										incDem = incDem
												+ graspaux.getDatosRed()
														.getDemanda()[ruta
														.get(0)][i];
									}
								}// else
								if (incDis == 0) {
									coc = graspaux.inf * incDem;
								} else {
									coc = (double) incDem / (double) (incDis);
									if (graspaux instanceof GraspCarga) {
										if (graspaux.getDatosRed()
												.getDemandaUrgente()[i][ruta
												.get(ruta.size() - 1)] == true) {
											// Si la demanda es urgente el
											// cociente se multiplica para que
											// sea más prioritario.
											coc *= graspaux.getDatosRed()
													.getMaximaCapacidad();
										}
									} else {
										if (graspaux.getDatosRed()
												.getDemandaUrgente()[ruta
												.get(0)][i] == true) {
											// Si la demanda es urgente el
											// ociente se multiplica para que
											// sea más prioritario.
											coc *= graspaux.getDatosRed()
													.getMaximaCapacidad();
										}
									}
								}// else
								if (coc > coc0
										&& rutasfinalesaux.get(j).get(1)
												+ incDis < distanciamax[j / 2]
										&& demandaporlineaaux[j] + incDem < capacidadmax[j / 2]
										&& !ruta.get(k).equals(i)) {
									coc0 = coc;
									k0 = k;
									incDem0 = incDem;
									incDis0 = incDis;
								}// if
							}// for
							graspaux.logdetalladostring += (i + "     "
									+ ((j / 2) + 1) + "     " + ((j % 2) + 1)
									+ "     " + k0 + "     " + incDem0
									+ "     " + incDis0 + "     " + coc0 + "\r\n");

							if (coc0 > cocm) {
								im = i;
								jm = j;
								km = k0;
								incDemm = incDem0;
								incDism = incDis0;
								cocm = coc0;
							}// if
						}// if
					}// if
				}// for
			}// for
			graspaux.logdetalladostring += ("\r\n" + im + "     "
					+ ((jm / 2) + 1) + "     " + ((jm % 2) + 1) + "     " + km
					+ "     " + incDemm + "     " + incDism + "     " + cocm + "     *\r\n");
			graspaux.logdetalladostring += ("--------------------------\r\n");
			// Si cociente (demanda/distancia) no es menos infinito se añade la
			// parada a la ruta en la posición obtenida.
			if (cocm > -9999) {
				ArrayList<Integer> ruta = new ArrayList<Integer>();
				String[] rutaAux = new String[rutasfinalesaux.size()];
				rutaAux = ("" + rutasfinalesaux.get(jm).get(0))
						.split(", |\\[|\\]");
				for (int l = 1; l < rutaAux.length; l++) {
					ruta.add(Integer.parseInt(rutaAux[l]));
				}// for
				ruta.add(km, im);
				int maximamejordistancia, mejordistancia, i1m = 0, aux;
				// Se reordenan las paradas dentro de la ruta parcial para que
				// sea más eficiente.
				do {
					maximamejordistancia = 0;
					for (i = 1; i < ruta.size() - 2; i++) {
						for (int i1 = i + 1; i1 < ruta.size() - 1; i1++) {
							if (i1 == i + 1) {
								mejordistancia = distanciasaux[ruta.get(i - 1)][ruta
										.get(i)]
										+ distanciasaux[ruta.get(i)][ruta
												.get(i + 1)]
										+ distanciasaux[ruta.get(i + 1)][ruta
												.get(i + 2)]
										- distanciasaux[ruta.get(i - 1)][ruta
												.get(i + 1)]
										- distanciasaux[ruta.get(i + 1)][ruta
												.get(i)]
										- distanciasaux[ruta.get(i)][ruta
												.get(i + 2)];
							} else {
								mejordistancia = distanciasaux[ruta.get(i - 1)][ruta
										.get(i)]
										+ distanciasaux[ruta.get(i)][ruta
												.get(i + 1)]
										+ distanciasaux[ruta.get(i1 - 1)][ruta
												.get(i1)]
										+ distanciasaux[ruta.get(i1)][ruta
												.get(i1 + 1)]
										- distanciasaux[ruta.get(i - 1)][ruta
												.get(i1)]
										- distanciasaux[ruta.get(i1)][ruta
												.get(i + 1)]
										- distanciasaux[ruta.get(i1 - 1)][ruta
												.get(i)]
										- distanciasaux[ruta.get(i)][ruta
												.get(i1 + 1)];
							}// else
							if (mejordistancia > maximamejordistancia) {
								im = i;
								i1m = i1;
								maximamejordistancia = mejordistancia;
							}// if
						}// for
					}// for
					if (maximamejordistancia > 0) {
						aux = ruta.get(im);
						ruta.set(im, ruta.get(i1m));
						ruta.set(i1m, aux);
					}// if
				} while (maximamejordistancia != 0);
				// Se completa la ruta añadiendo paradas intermedias que
				// faltaran.
				ArrayList rutaparcial = new ArrayList();
				for (int l = 1; l < ruta.size(); l++) {
					if (floyd.getRecorrido(ruta.get(l - 1), ruta.get(l)).size() > 2) {
						for (int n = floyd.getRecorrido(ruta.get(l - 1),
								ruta.get(l)).size() - 2; n > 0; n--) {
							ruta.add(
									l,
									floyd.getRecorrido(ruta.get(l - 1),
											ruta.get(l)).get(n));
						}
					}
				}
				rutaparcial.add(ruta);
				if (graspaux instanceof GraspCarga) {
					if (parescubiertosaux[im][ruta.get(ruta.size() - 1)] == 0) {
						parescubiertosaux[im][ruta.get(ruta.size() - 1)] = jm + 1;
					}
				} else {
					if (parescubiertosaux[ruta.get(0)][im] == 0) {
						parescubiertosaux[ruta.get(0)][im] = jm + 1;
					}
				}
				rutaparcial.add(rutasfinalesaux.get(jm).get(1) + incDism);
				rutasfinalesaux.set(jm, rutaparcial);
				for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
					for (j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
						if (parescubiertosaux[i][j] > 0) {
							demandaporlineaaux[parescubiertosaux[i][j] - 1] += graspaux
									.getDatosRed().getDemanda()[i][j];
						}// if
					}// for
				}// for
			}// if
		}// while
			// Se completan las rutas añadiendo paradas intermedias que
			// faltaran.
		for (j = 0; j < rutasfinalesaux.size(); j++) {
			if (rutasACambiar.contains(j)) {
				ArrayList<Integer> ruta = new ArrayList<Integer>();
				String[] rutaAux = new String[rutasfinalesaux.size()];
				rutaAux = ("" + rutasfinalesaux.get(j).get(0))
						.split(", |\\[|\\]");
				for (int l = 1; l < rutaAux.length; l++) {
					ruta.add(Integer.parseInt(rutaAux[l]));
				}// for
				if (ruta.size() == 2) {
					if (floyd.getRecorrido(ruta.get(0), ruta.get(1)).size() > 2) {
						for (int n = floyd.getRecorrido(ruta.get(0),
								ruta.get(1)).size() - 2; n > 0; n--) {
							ruta.add(
									1,
									floyd.getRecorrido(ruta.get(0), ruta.get(1))
											.get(n));
						}// for
					}// if
				}// if
				for (int l = 0; l < ruta.size() - 1; l++) {
					if (floyd.getRecorrido(ruta.get(l), ruta.get(l + 1)).size() > 2) {
						for (int n = floyd.getRecorrido(ruta.get(l),
								ruta.get(l + 1)).size() - 2; n > 0; n--) {
							ruta.add(
									l + 1,
									floyd.getRecorrido(ruta.get(l),
											ruta.get(l + 1)).get(n));
						}// for
					}// if
				}// for

				ArrayList rutaparcial = new ArrayList();
				rutaparcial.add(ruta);
				rutaparcial.add(rutasfinalesaux.get(j).get(1));
				rutasfinalesaux.set(j, rutaparcial);
			}// if

			ArrayList<Integer> ruta = new ArrayList<Integer>();
			String[] rutaAux = new String[rutasfinalesaux.size()];
			rutaAux = ("" + rutasfinalesaux.get(j).get(0)).split(", |\\[|\\]");
			for (int l = 1; l < rutaAux.length; l++) {
				ruta.add(Integer.parseInt(rutaAux[l]));
			}// for
			graspaux.logstring += ("\r\nRuta " + (j + 1) + ": ");
			graspaux.logdetalladostring += ("\r\nRuta " + (j + 1) + ": ");
			for (int l = 0; l < ruta.size(); l++) {
				graspaux.logstring += (ruta.get(l) + "     ");
				graspaux.logdetalladostring += (ruta.get(l) + "     ");
			}
		}// for

		graspaux.setDemandaPorLinea(demandaporlineaaux);
		graspaux.setRutasFinales(rutasfinalesaux);
		graspaux.setParescubiertos(parescubiertosaux);
		graspaux.setFloyd(floyd);
		// Se completan las rutas eliminando y añadiendo paradas para aprovechar
		// mejor los recursos.
		try {
			if (graspaux instanceof GraspCarga) {
				((GraspCarga) graspaux).evaluarParesParadas();
				((GraspCarga) graspaux).añadirParadas();
				((GraspCarga) graspaux).recalcularTiempos();
				((GraspCarga) graspaux).calcularDemandaCubierta();
				((GraspCarga) graspaux).eliminarParadas();
				((GraspCarga) graspaux).añadirParadas();
				((GraspCarga) graspaux).recalcularTiempos();
				((GraspCarga) graspaux).calcularDemandaCubierta();
			} else {
				((GraspDescarga) graspaux).evaluarParesParadas();
				((GraspDescarga) graspaux).añadirParadas();
				((GraspDescarga) graspaux).recalcularTiempos();
				((GraspDescarga) graspaux).calcularDemandaCubierta();
				((GraspDescarga) graspaux).eliminarParadas();
				((GraspDescarga) graspaux).añadirParadas();
				((GraspDescarga) graspaux).recalcularTiempos();
				((GraspDescarga) graspaux).calcularDemandaCubierta();
			}

			ArrayList<ArrayList<Integer>> rf = new ArrayList<ArrayList<Integer>>();
			for (i = 0; i < errores.size(); i++) {
				// Si se asigno un transporte a una ruta en la solución anterior
				// esa ruta no se modifica.
				if (errores.get(i).contains(-4)) {
					ArrayList al = new ArrayList();
					al.add(ruf.get(2 * i).get(0));
					al.add(ruf.get(2 * i).get(1));
					rf.add(al);
					al = new ArrayList();
					al.add(ruf.get(2 * i + 1).get(0));
					al.add(ruf.get(2 * i + 1).get(1));
					rf.add(al);
					for (j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
						for (k = 0; k < graspaux.getDatosRed().getNumeroNodos(); k++) {
							if (graspaux.getParescubiertos()[j][k] == 2 * i + 1) {
								if (parescubiertosaux0[j][k] != 2 * i + 1) {
									graspaux.getParescubiertos()[j][k] = 0;
								}
							}
							if (parescubiertosaux0[j][k] == 2 * i + 1) {
								graspaux.getParescubiertos()[j][k] = 2 * i + 1;
							}
							if (graspaux.getParescubiertos()[j][k] == 2 * i + 2) {
								if (parescubiertosaux0[j][k] != 2 * i + 2) {
									graspaux.getParescubiertos()[j][k] = 0;
								}
							}
							if (parescubiertosaux0[j][k] == 2 * i + 2) {
								graspaux.getParescubiertos()[j][k] = 2 * i + 2;
							}
						}
					}
				} else {
					rf.add(graspaux.getRutasFinales().get(2 * i));
					rf.add(graspaux.getRutasFinales().get(2 * i + 1));
				}
			}
			graspaux.setRutasFinales(rf);
			graspaux.escribeLog(false);
			graspaux.escribeLogDetallado(false);
			if (graspaux instanceof GraspCarga) {
				((GraspCarga) graspaux).recalcularTiempos();
				((GraspCarga) graspaux).calcularDemandaCubierta();
			} else {
				((GraspDescarga) graspaux).recalcularTiempos();
				((GraspDescarga) graspaux).calcularDemandaCubierta();
			}
			// Se comprueba si la solución obtenida y la anterior son iguales.
			boolean camb = false;
			for (i = 0; i < graspaux.getDatosRed().getNumeroNodos(); i++) {
				for (j = 0; j < graspaux.getDatosRed().getNumeroNodos(); j++) {
					if (graspaux.getParescubiertos()[i][j] > 0
							&& graspaux.getDatosRed().getDemanda()[i][j] > 0
							&& graspaux.getParescubiertos()[i][j] != parescubiertosaux0[i][j]) {
						if (!errores.get(
								(graspaux.getParescubiertos()[i][j] - 1) / 2)
								.contains(-4)) {
							camb = true;
						}
					}
				}
			}
			for (i = 0; i < graspaux.lineastotales * 2; i++) {
				if (!errores.get(i / 2).contains(-4)) {
					if (graspaux.getDemandaPorLinea()[i] != demandaporlineaaux0[i]) {
						camb = true;
					}
					if (graspaux.getRutasFinales().get(i).get(1) != ruf.get(i)
							.get(1)) {
						camb = true;
					}
					ArrayList<Integer> ruta = new ArrayList<Integer>();
					ArrayList<Integer> ruta2 = new ArrayList<Integer>();
					String[] rutaAux = new String[graspaux.lineastotales * 2];
					rutaAux = ("" + graspaux.getRutasFinales().get(i).get(0))
							.split(", |\\[|\\]");
					for (int l = 1; l < rutaAux.length; l++) {
						ruta.add(Integer.parseInt(rutaAux[l]));
					}// for
					rutaAux = new String[graspaux.lineastotales * 2];
					rutaAux = ("" + ruf.get(i).get(0)).split(", |\\[|\\]");
					for (int l = 1; l < rutaAux.length; l++) {
						ruta2.add(Integer.parseInt(rutaAux[l]));
					}// for
					if (ruta.size() != ruta2.size()) {
						camb = true;
					} else {
						for (int l = 0; l < ruta.size(); l++) {
							if (ruta.get(l) != ruta2.get(l)) {
								camb = true;
							}
						}
					}
				}// if
			}// for
				// Se asignan transportes a las nuevas rutas.
			AsignaTransportes asignaTransportes = new AsignaTransportes(
					graspaux, distanciamax, capacidadmax, errores,
					demandacubiertaanterior);
			asignaTransportes.asigna(save, saverutas, graspaux, camb);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}// rehacerRutas
}// ArreglaRuta
