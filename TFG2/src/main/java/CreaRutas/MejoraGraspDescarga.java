package CreaRutas;

import java.util.ArrayList;

/**
 * Clase que se encarga de mejorar las rutas de descarga.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class MejoraGraspDescarga extends ApoyoGraspDescarga {

	/**
	 * Elimina paradas que aumentan el tiempo de recorrido y no tienen demanda.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void eliminarParadas() throws Exception {
		ArrayList rutasfinalesaux = rutasfinales;
		int[][] parescubiertosaux = new int[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		ArrayList<Integer> rutaparcial = new ArrayList<Integer>();
		ArrayList rutaparcial2 = new ArrayList();
		ArrayList<Integer> aBorrar;
		rutasfinales = new ArrayList();
		int[] demandaj = new int[lineastotales];
		int aux = 0;
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (parescubiertos[i][j] % 2 == 0 && parescubiertos[i][j] > 0) {
					demandaj[parescubiertos[i][j] / 2 - 1] += demanda[i][j]
							* tiempomedioparada;
				}
				parescubiertosaux[i][j] = parescubiertos[i][j];
				parescubiertos[i][j] = 0;
			}
		}
		for (int i = 1; i <= lineastotales; i++) {
			rutaparcial = new ArrayList<Integer>();
			String[] rutaAux = new String[rutasfinalesaux.size()];
			rutaAux = ("" + ((ArrayList) rutasfinalesaux.get((i - 1) * 2))
					.get(0)).split(", |\\[|\\]");
			for (int l = 1; l < rutaAux.length; l++) {
				rutaparcial.add(Integer.parseInt(rutaAux[l]));
			}// for
			int distanciaj = 0;
			for (int j = 0; j < rutaparcial.size(); j++) {
				if (j > 0) {
					distanciaj += datosred.getDistancia()[rutaparcial
							.get(j - 1)][rutaparcial.get(j)];
				}
				if (demandaATiempo(rutaparcial.get(j), distanciaj)) {
					distanciaj += demanda[rutaparcial.get(0)][rutaparcial
							.get(j)] * tiempomedioparada;
					if (parescubiertos[rutaparcial.get(0)][rutaparcial.get(j)] < 1
							|| parescubiertosaux[rutaparcial.get(0)][rutaparcial
									.get(j)] == 2 * (i - 1) + 1) {
						parescubiertos[rutaparcial.get(0)][rutaparcial.get(j)] = 2 * (i - 1) + 1;
					}
				}
			}
			distanciaj += demandaj[i - 1];
			rutaparcial = new ArrayList<Integer>();
			rutaAux = new String[rutasfinalesaux.size()];
			rutaAux = ("" + ((ArrayList) rutasfinalesaux.get((i - 1) * 2 + 1))
					.get(0)).split(", |\\[|\\]");
			for (int l = 1; l < rutaAux.length; l++) {
				rutaparcial.add(Integer.parseInt(rutaAux[l]));
			}// for
			for (int j = 0; j < rutaparcial.size(); j++) {
				if (j > 0) {
					distanciaj += datosred.getDistancia()[rutaparcial
							.get(j - 1)][rutaparcial.get(j)];
				}
				if (demandaATiempo(rutaparcial.get(j), distanciaj)) {
					distanciaj += demanda[rutaparcial.get(0)][rutaparcial
							.get(j)] * tiempomedioparada;
					if (parescubiertos[rutaparcial.get(0)][rutaparcial.get(j)] < 1
							|| parescubiertosaux[rutaparcial.get(0)][rutaparcial
									.get(j)] == 2 * (i - 1) + 2) {
						parescubiertos[rutaparcial.get(0)][rutaparcial.get(j)] = 2 * (i - 1) + 2;
					}
				}
			}
		}
		for (int i = 1; i <= lineastotales; i++) {
			for (int j = 0; j < 2; j++) {
				rutaparcial = new ArrayList<Integer>();
				String[] rutaAux = new String[rutasfinalesaux.size()];
				rutaAux = ("" + ((ArrayList) rutasfinalesaux.get((i - 1) * 2
						+ j)).get(0)).split(", |\\[|\\]");
				for (int l = 1; l < rutaAux.length; l++) {
					rutaparcial.add(Integer.parseInt(rutaAux[l]));
				}// for
				if (rutaparcial.size() == 2 && rutaparcial.get(0) == 0
						&& rutaparcial.get(1) == 0) {
					rutaparcial2 = new ArrayList();
					rutaparcial2.add(rutaparcial);
					rutaparcial2.add(0);
					rutasfinales.add(rutaparcial2);
					rutaparcial2 = new ArrayList();
				} else {
					if (j == 0) {
						aux = 1;
					}
					if (j == 1) {
						aux = 2;
					}// Se borran aquellas paradas que no cubren demanda y
						// alargan el recorrido.
					aBorrar = new ArrayList<Integer>();
					for (int h = 0; h < rutaparcial.size() - 2; h++) {
						if (!aBorrar.contains(h)) {
							ArrayList<Integer> recorrido = new ArrayList<Integer>();
							boolean haydemanda = false;
							for (int g = h; g < rutaparcial.size() - 2; g++) {
								if (!haydemanda) {
									if (demanda[rutaparcial.get(0)][rutaparcial
											.get(g + 1)] == 0
											|| parescubiertosaux[rutaparcial
													.get(0)][rutaparcial
													.get(g + 1)] != (i - 1) * 2
													+ j + 1) {
										if (distancia[rutaparcial.get(h)][rutaparcial
												.get(g + 1)]
												+ distancia[rutaparcial
														.get(g + 1)][rutaparcial
														.get(g + 2)] > distancia[rutaparcial
													.get(h)][rutaparcial
												.get(g + 2)]) {
											if (rutasCubiertas[rutaparcial
													.get(h)][rutaparcial
													.get(g + 2)]) {
												recorrido.add(rutaparcial
														.get(g + 1));
												for (Integer borra : recorrido) {
													aBorrar.add(borra);
												}
												recorrido = new ArrayList<Integer>();
											}
										} else {
											recorrido.add(rutaparcial
													.get(g + 1));
										}
									} else {
										if (rutasCubiertas[rutaparcial.get(h)][rutaparcial
												.get(g + 1)]) {
											for (Integer borra : recorrido) {
												aBorrar.add(borra);
											}
										}
										haydemanda = true;
									}// else
								}// if
							}// for
						}// if
					}// for
						// Se comprueba que la ruta sigue un recorrido válido.
					int inicio = rutaparcial.get(0);
					int fin = rutaparcial.get(rutaparcial.size() - 1);
					rutaparcial.removeAll(aBorrar);
					ArrayList<Integer> rutaparcialaux = new ArrayList<Integer>();
					if (rutaparcial.size() < 1 || rutaparcial.get(0) != inicio) {
						rutaparcialaux.add(inicio);
					}
					if (rutaparcial.size() > 0) {
						rutaparcialaux.add(rutaparcial.get(0));
					}
					for (int k = 1; k < rutaparcial.size(); k++) {
						if (rutaparcial.get(k - 1) != rutaparcial.get(k)) {
							if (floyd.getRecorrido(rutaparcial.get(k - 1),
									rutaparcial.get(k)).size() > 2) {
								for (int r = 1; r < floyd.getRecorrido(
										rutaparcial.get(k - 1),
										rutaparcial.get(k)).size() - 1; r++) {
									rutaparcialaux.add(floyd.getRecorrido(
											rutaparcial.get(k - 1),
											rutaparcial.get(k)).get(r));
								}
							}
							rutaparcialaux.add(rutaparcial.get(k));
						}// if
					}// for
					if (rutaparcialaux.get(rutaparcialaux.size() - 1) != fin) {
						rutaparcialaux.add(fin);
					}
					if (rutaparcialaux.size() == 2) {
						if (floyd.getRecorrido(rutaparcialaux.get(0),
								rutaparcialaux.get(1)).size() > 2) {
							for (int r = 1; r < floyd.getRecorrido(
									rutaparcialaux.get(0),
									rutaparcialaux.get(1)).size() - 1; r++) {
								rutaparcialaux.add(
										rutaparcialaux.size() - 1,
										floyd.getRecorrido(
												rutaparcialaux.get(0),
												rutaparcialaux.get(1)).get(r));
							}
						}
					}
					int distanciah = 0;
					if (aux == 2) {
						distanciah = (int) ((ArrayList) rutasfinalesaux
								.get(2 * i - 2)).get(1);
					}
					for (int h = 1; h < rutaparcialaux.size(); h++) {
						distanciah += datosred.getDistancia()[rutaparcialaux
								.get(h - 1)][rutaparcialaux.get(h)];
						if (demandaATiempo(rutaparcialaux.get(h), distanciah)) {
							if (parescubiertos[rutaparcialaux.get(0)][rutaparcialaux
									.get(h)] == 0) {
								distanciah += tiempomedioparada
										* demanda[rutaparcialaux.get(0)][rutaparcialaux
												.get(h)];
								parescubiertos[rutaparcialaux.get(0)][rutaparcialaux
										.get(h)] = (i - 1) * 2 + j + 1;
							}
						}// if
					}// for
					int demandah = 0;
					for (int h = 1; h < rutaparcialaux.size(); h++) {
						if (parescubiertos[rutaparcialaux.get(0)][rutaparcialaux
								.get(h)] == (i - 1) * 2 + aux) {
							demandah += demanda[rutaparcialaux.get(0)][rutaparcialaux
									.get(h)];
						}
					}
					if (demandaporlinea[(i - 1) * 2 + aux - 1] <= demandah) {
						rutaparcial = rutaparcialaux;
						rutaparcial2.add(rutaparcial);
						rutaparcial2.add(calcularTiempoRuta(rutaparcial, 2
								* (i - 1) + aux));
						rutasfinales.add(rutaparcial2);
						rutaparcial2 = new ArrayList();
						for (int h = 0; h < aBorrar.size(); h++) {
							logstring += "\r\nSe elimina la parada "
									+ aBorrar.get(h) + " de la ruta "
									+ (2 * (i - 1) + aux) + ".";
							logdetalladostring += "\r\nSe elimina la parada "
									+ aBorrar.get(h) + " de la ruta "
									+ (2 * (i - 1) + aux) + ".";
						}
					} else {
						rutaparcial2 = new ArrayList();
						rutaparcial2.add(((ArrayList) rutasfinalesaux
								.get(((i - 1) * 2 + aux - 1))).get(0));
						rutaparcial2.add(((ArrayList) rutasfinalesaux
								.get(((i - 1) * 2 + aux - 1))).get(1));
						rutasfinales.add(rutaparcial2);
						rutaparcial2 = new ArrayList();
					}// else
					demandaPorLinea((i - 1) * 2 + aux - 1);
					if (distanciah > tmaxlinea) {
						boolean borra = true;
						while (borra) {
							borra = borraDemanda((i - 1) * 2 + aux - 1);
						}// while
					}// if
				}// if
			}// for
		}// for
		recalcularTiempos();
		calcularDemandaCubierta();
	}// eliminarParadas

	/**
	 * Borra demanda para que la ruta no sobrepase el límite de tiempo.
	 * 
	 * @param i
	 *            Ruta a la que hay que reducir la demanda cubierta.
	 * @return True si la ruta aún no es válida. False en caso contrario.
	 */
	private boolean borraDemanda(int i) {
		if (i % 2 == 0) {
			ArrayList<Integer> ruta = new ArrayList<Integer>();
			String[] rutaaux = ("" + rutasfinales.get(i).get(0))
					.split(", |\\[|\\]");
			for (int j = 1; j < rutaaux.length; j++) {
				ruta.add(Integer.parseInt(rutaaux[j]));
			}// for
			int demandaminima = inf;
			int paradaminima = -1;
			// Se busca la menor demanda para que al borrarla empeore lo mínimo
			// la solución.
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDemanda()[ruta.get(0)][j] < demandaminima
						&& datosred.getDemanda()[ruta.get(0)][j] > 0
						&& parescubiertos[ruta.get(0)][j] == i + 1
						&& !datosred.getDemandaUrgente()[ruta.get(0)][j]) {
					demandaminima = datosred.getDemanda()[ruta.get(0)][j];
					paradaminima = j;
				}
			}// for
			if (paradaminima == -1) {
				return false;
			} else {
				parescubiertos[ruta.get(0)][paradaminima] = 0;
				logstring += "\r\nSe elimina la demanda cubierta por la parada "
						+ paradaminima + " en la ruta " + (i + 1) + ".";
				logdetalladostring += "\r\nSe elimina la demanda cubierta por la parada "
						+ paradaminima + " en la ruta " + (i + 1) + ".";
				boolean pase[][] = new boolean[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				for (int q = 0; q < datosred.getNumeroNodos(); q++) {
					for (int p = 0; p < datosred.getNumeroNodos(); p++) {
						pase[q][p] = false;
					}
				}
				int distanciah = 0;
				for (int h = 1; h < ruta.size(); h++) {
					distanciah += datosred.getDistancia()[ruta.get(h - 1)][ruta
							.get(h)];
					if (demandaATiempo(ruta.get(h), distanciah)) {
						if (parescubiertos[ruta.get(0)][ruta.get(h)] == i + 1
								&& !pase[ruta.get(0)][ruta.get(h)]) {
							distanciah += demanda[ruta.get(0)][ruta.get(h)]
									* tiempomedioparada;
							pase[ruta.get(0)][ruta.get(h)] = true;
						}
					}// if
				}// for
				if (distanciah > tmaxlinea) {
					return true;
				} else {
					return false;
				}
			}// else
		} else {
			ArrayList<Integer> rutaida = new ArrayList<Integer>();
			ArrayList<Integer> rutavuelta = new ArrayList<Integer>();
			boolean ida = false;
			String[] rutaaux = ("" + rutasfinales.get(i - 1).get(0))
					.split(", |\\[|\\]");
			for (int j = 1; j < rutaaux.length; j++) {
				rutaida.add(Integer.parseInt(rutaaux[j]));
			}// for
			rutaaux = ("" + rutasfinales.get(i).get(0)).split(", |\\[|\\]");
			for (int j = 1; j < rutaaux.length; j++) {
				rutavuelta.add(Integer.parseInt(rutaaux[j]));
			}// for
			int demandaminima = inf;
			int paradaminima = -1;
			// Se busca la menor demanda para que al borrarla empeore lo mínimo
			// la solución.
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDemanda()[rutaida.get(0)][j] < demandaminima
						&& datosred.getDemanda()[rutaida.get(0)][j] > 0
						&& parescubiertos[rutaida.get(0)][j] == i
						&& !datosred.getDemandaUrgente()[rutaida.get(0)][j]) {
					demandaminima = datosred.getDemanda()[rutaida.get(0)][j];
					paradaminima = j;
					ida = true;
				}
			}// for
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDemanda()[rutavuelta.get(0)][j] < demandaminima
						&& datosred.getDemanda()[rutavuelta.get(0)][j] > 0
						&& parescubiertos[rutavuelta.get(0)][j] == i + 1
						&& !datosred.getDemandaUrgente()[rutavuelta.get(0)][j]) {
					demandaminima = datosred.getDemanda()[rutavuelta.get(0)][j];
					paradaminima = j;
					ida = false;
				}
			}// for
			if (paradaminima == -1) {
				return false;
			} else {
				if (ida) {
					parescubiertos[rutaida.get(0)][paradaminima] = 0;
					logstring += "\r\nSe elimina la demanda cubierta por la parada "
							+ paradaminima + " en la ruta " + i + ".";
					logdetalladostring += "\r\nSe elimina la demanda cubierta por la parada "
							+ paradaminima + " en la ruta " + i + ".";
				} else {
					parescubiertos[rutavuelta.get(0)][paradaminima] = 0;
					logstring += "\r\nSe elimina la demanda cubierta por la parada "
							+ paradaminima + " en la ruta " + (i + 1) + ".";
					logdetalladostring += "\r\nSe elimina la demanda cubierta por la parada "
							+ paradaminima + " en la ruta " + (i + 1) + ".";
				}
				boolean pase[][] = new boolean[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				for (int q = 0; q < datosred.getNumeroNodos(); q++) {
					for (int p = 0; p < datosred.getNumeroNodos(); p++) {
						pase[q][p] = false;
					}
				}
				int distanciah = 0;
				for (int h = 1; h < rutaida.size(); h++) {
					distanciah += datosred.getDistancia()[rutaida.get(h - 1)][rutaida
							.get(h)];
					if (demandaATiempo(rutaida.get(h), distanciah)) {
						if (parescubiertos[rutaida.get(0)][rutaida.get(h)] == i
								&& !pase[rutaida.get(0)][rutaida.get(h)]) {
							distanciah += demanda[rutaida.get(0)][rutaida
									.get(h)] * tiempomedioparada;
							pase[rutaida.get(0)][rutaida.get(h)] = true;
						}
					}// if
				}// for
				for (int h = 1; h < rutavuelta.size(); h++) {
					if (parescubiertos[rutavuelta.get(0)][rutavuelta.get(h)] == i + 1) {
						distanciah += demanda[rutavuelta.get(0)][rutavuelta
								.get(h)] * tiempomedioparada;
					}
				}
				for (int h = 1; h < rutavuelta.size(); h++) {
					distanciah += datosred.getDistancia()[rutavuelta.get(h - 1)][rutavuelta
							.get(h)];
					if (demandaATiempo(rutavuelta.get(h), distanciah)) {
						if (parescubiertos[rutavuelta.get(0)][rutavuelta.get(h)] == i + 1
								&& !pase[rutavuelta.get(0)][rutavuelta.get(h)]) {
							distanciah += demanda[rutavuelta.get(0)][rutavuelta
									.get(h)] * tiempomedioparada;
							pase[rutavuelta.get(0)][rutavuelta.get(h)] = true;
						}
					}// if
				}// for
				if (distanciah > tmaxlinea) {
					return true;
				} else {
					return false;
				}
			}// else
		}// else
	}// borraDemanda

	/**
	 * Añade paradas a las rutas en las que sea posible para cubrir la demanda
	 * no cubierta.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void añadirParadas() throws Exception {
		ArrayList<ArrayList<Integer>> rutas = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> rutaAux;
		String[] ruta;
		for (int i = 0; i < rutasfinales.size(); i++) {
			ruta = new String[rutasfinales.size()];
			ruta = ("" + rutasfinales.get(i).get(0)).split(", |\\[|\\]");
			rutaAux = new ArrayList<Integer>();
			for (int j = 1; j < ruta.length; j++) {
				rutaAux.add(Integer.parseInt(ruta[j]));
			}// for
			rutas.add(rutaAux);
		}// for
		boolean cambio = true;
		while (cambio) {
			demandaporlinea = new int[lineastotales * 2];
			for (int i = 0; i < lineastotales * 2; i++) {
				for (int q = 0; q < datosred.getNumeroNodos(); q++) {
					for (int p = 0; p < datosred.getNumeroNodos(); p++) {
						if (parescubiertos[q][p] == i + 1) {
							demandaporlinea[i] = demandaporlinea[i]
									+ demanda[q][p];
						}// if
					}// for
				}// for
			}// for
			cambio = false;
			int maxDem = 0;
			int[] parACubrir = new int[2];
			int[] parurgente = paradaurgente();
			if (parurgente[0] >= 0 && parurgente[1] >= 0) {
				parACubrir[0] = parurgente[0];
				parACubrir[1] = parurgente[1];
			} else {
				for (int i = 0; i < datosred.getNumeroNodos(); i++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						if (parescubiertos[i][j] == 0
								&& datosred.getDemanda()[i][j] > maxDem) {
							maxDem = datosred.getDemanda()[i][j];
							parACubrir[0] = i;
							parACubrir[1] = j;
						}// if
					}// for
				}// for
			}// else
			if (parACubrir[0] != parACubrir[1]) {
				int par;
				int disMin = inf;
				int posicioni = -1;
				int posicionj = -1;
				for (int i = 0; i < rutas.size(); i++) {
					if (rutas.get(i).size() < maximasparadaspermitidas
							&& demandaporlinea[i]
									+ demanda[parACubrir[0]][parACubrir[1]] < datosred
										.getMaximaCapacidad()) {
						if (rutas.get(i).get(0) == parACubrir[0]) {
							if (!rutas.get(i).contains(parACubrir[1])) {
								if (i % 2 == 0) {
									par = 1;
								} else {
									par = -1;
								}// else
								for (int j = 1; j < rutas.get(i).size() - 1; j++) {
									if (i % 2 == 0) {
										if (disMin > distancia[rutas.get(i)
												.get(j - 1)][parACubrir[1]]
												+ distancia[parACubrir[1]][rutas
														.get(i).get(j)]
												- distancia[rutas.get(i).get(
														j - 1)][rutas.get(i)
														.get(j)]
												+ demanda[parACubrir[0]][parACubrir[1]]
												* tiempomedioparada
												&& rutasfinales.get(i).get(1)
														+ rutasfinales.get(
																i + par).get(1)
														+ distancia[rutas
																.get(i).get(
																		j - 1)][parACubrir[1]]
														+ distancia[parACubrir[1]][rutas
																.get(i).get(j)]
														- distancia[rutas
																.get(i).get(
																		j - 1)][rutas
																.get(i).get(j)]
														+ demanda[parACubrir[0]][parACubrir[1]]
														* tiempomedioparada < tmaxlinea) {
											disMin = distancia[rutas.get(i)
													.get(j - 1)][parACubrir[1]]
													+ distancia[parACubrir[1]][rutas
															.get(i).get(j)]
													- distancia[rutas.get(i)
															.get(j - 1)][rutas
															.get(i).get(j)]
													+ demanda[parACubrir[0]][parACubrir[1]]
													* tiempomedioparada;
											posicioni = i;
											posicionj = j;
										}// if
									} else {
										if (disMin > distancia[rutas.get(i)
												.get(j - 1)][parACubrir[1]]
												+ distancia[parACubrir[1]][rutas
														.get(i).get(j)]
												- distancia[rutas.get(i).get(
														j - 1)][rutas.get(i)
														.get(j)]
												+ demanda[parACubrir[0]][parACubrir[1]]
												* tiempomedioparada * 2
												&& rutasfinales.get(i).get(1)
														+ rutasfinales.get(
																i + par).get(1)
														+ distancia[rutas
																.get(i).get(
																		j - 1)][parACubrir[1]]
														+ distancia[parACubrir[1]][rutas
																.get(i).get(j)]
														- distancia[rutas
																.get(i).get(
																		j - 1)][rutas
																.get(i).get(j)]
														+ demanda[parACubrir[0]][parACubrir[1]]
														* tiempomedioparada < tmaxlinea) {
											disMin = distancia[rutas.get(i)
													.get(j - 1)][parACubrir[1]]
													+ distancia[parACubrir[1]][rutas
															.get(i).get(j)]
													- distancia[rutas.get(i)
															.get(j - 1)][rutas
															.get(i).get(j)]
													+ demanda[parACubrir[0]][parACubrir[1]]
													* tiempomedioparada * 2;
											posicioni = i;
											posicionj = j;
										}// if
									}// else
								}// for
							}// if
						}// if
					}// if
				}// for
				int demandah = 0, distanciah = 0;
				if (posicioni != -1 && posicionj != -1) {
					if (posicioni % 2 != 0) {
						distanciah = rutasfinales.get(posicioni - 1).get(1);
					}
					for (int h = 1; h < rutas.get(posicioni).size(); h++) {
						if (h == posicionj) {
							distanciah += distancia[parACubrir[1]][rutas.get(
									posicioni).get(h)];
							if (demandaATiempo(rutas.get(posicioni).get(h),
									distanciah)) {
								demandah += demanda[rutas.get(posicioni).get(0)][rutas
										.get(posicioni).get(h)];
								distanciah += tiempomedioparada
										* demanda[rutas.get(posicioni).get(0)][rutas
												.get(posicioni).get(h)];
							}
						} else if (h == posicionj - 1) {
							distanciah += distancia[rutas.get(posicioni).get(
									h - 1)][rutas.get(posicioni).get(h)];
							if (parescubiertos[rutas.get(posicioni).get(0)][rutas
									.get(posicioni).get(h)] == posicioni + 1) {
								if (demandaATiempo(rutas.get(posicioni).get(h),
										distanciah)) {
									distanciah += tiempomedioparada
											* demanda[rutas.get(posicioni).get(
													0)][rutas.get(posicioni)
													.get(h)];
									demandah += demanda[rutas.get(posicioni)
											.get(0)][rutas.get(posicioni)
											.get(h)];
								}
							}
							distanciah += distancia[rutas.get(posicioni).get(h)][parACubrir[1]];
							if (demandaATiempo(parACubrir[1], distanciah)) {
								demandah += demanda[rutas.get(posicioni).get(0)][parACubrir[1]];
								distanciah += tiempomedioparada
										* demanda[rutas.get(posicioni).get(0)][parACubrir[1]];
							}
						} else {
							distanciah += distancia[rutas.get(posicioni).get(
									h - 1)][rutas.get(posicioni).get(h)];
							if (parescubiertos[rutas.get(posicioni).get(0)][rutas
									.get(posicioni).get(h)] == posicioni + 1) {
								if (demandaATiempo(rutas.get(posicioni).get(h),
										distanciah)) {
									distanciah += tiempomedioparada
											* demanda[rutas.get(posicioni).get(
													0)][rutas.get(posicioni)
													.get(h)];
									demandah += demanda[rutas.get(posicioni)
											.get(0)][rutas.get(posicioni)
											.get(h)];
								}
							}
						}
					}// for
				}// if
					// Solo se añade esta nueva demanda si aumenta la demanda
					// cubierta total.
				if (posicioni != -1 && posicionj != -1
						&& demandaporlinea[posicioni] < demandah) {
					rutas.get(posicioni).add(posicionj, parACubrir[1]);
					if (maximasparadas < rutas.get(posicioni).size()) {
						maximasparadas = rutas.get(posicioni).size();
					}// if
					parescubiertos[parACubrir[0]][parACubrir[1]] = posicioni + 1;
					demandacub[parACubrir[0]][parACubrir[1]] = true;
					cambio = true;
					TipoParadasRutaParcial tprp = new TipoParadasRutaParcial(
							maximasparadas);
					for (int i = 0; i < rutas.get(posicioni).size(); i++) {
						tprp.setParadasRutaParcial(i + 1, rutas.get(posicioni)
								.get(i));
					}// for
					if (posicioni % 2 == 0) {
						ST[(posicioni / 2) + 1].getRutaInicial()
								.setRutaParcial(tprp);
						ST[(posicioni / 2) + 1].getRutaInicial()
								.setNumeroParadas(rutas.get(posicioni).size());
						ST[(posicioni / 2) + 1].getRutaInicial()
								.aumentarTiempo(disMin);
						postProcesamiento((posicioni / 2) + 1, true);
					} else {
						ST[(posicioni / 2) + 1].getRutaFinal().setRutaParcial(
								tprp);
						ST[(posicioni / 2) + 1].getRutaFinal()
								.setNumeroParadas(rutas.get(posicioni).size());
						ST[(posicioni / 2) + 1].getRutaFinal().aumentarTiempo(
								disMin);
						postProcesamiento((posicioni / 2) + 1, false);
					}// else
					logstring += "\r\nSe añade la parada " + parACubrir[1]
							+ " en la ruta " + (posicioni + 1) + ".";
					logdetalladostring += "\r\nSe añade la parada "
							+ parACubrir[1] + " en la ruta " + (posicioni + 1)
							+ ".";
					ArrayList rutaparcial = new ArrayList();
					ArrayList<Integer> rutaañadida = expandirRuta(
							posicioni / 2 + 1, posicioni % 2 + 1);
					rutaparcial.add(rutaañadida);
					rutaparcial.add(calcularTiempoRuta(rutaañadida,
							posicioni + 1));
					rutasfinales.set(posicioni, rutaparcial);
				} else if (parACubrir[0] != 0 || parACubrir[1] != 0) {
					cambio = true;
					parescubiertos[parACubrir[0]][parACubrir[1]] = -1;
				}// else if
			}// if
			demandacubierta = 0;
			for (int i = 0; i < datosred.getNumeroNodos(); i++) {
				for (int j = 0; j < datosred.getNumeroNodos(); j++) {
					if (parescubiertos[i][j] > 0) {
						demandacubierta = demandacubierta + demanda[i][j];
					}// if
				}// for
			}// for
		}// while
	}// añadirParadas

	/**
	 * Devuelve el par de paradas urgentes sin cubrir con más demanda para que
	 * sean añadidas.
	 */
	private int[] paradaurgente() {
		int maxdemanda = 0;
		int[] parurgente = new int[2];
		parurgente[0] = -1;
		parurgente[1] = -1;
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDemandaUrgente()[i][j] && !demandacub[i][j]
						&& maxdemanda < demanda[i][j]
						&& parescubiertos[i][j] >= 0) {
					parurgente[0] = i;
					parurgente[1] = j;
					maxdemanda = demanda[i][j];
				}
			}
		}
		return parurgente;
	}// paradaurgente

	/**
	 * Calcula la demanda cubierta por una ruta.
	 * 
	 * @param r
	 *            Identificador de la ruta de la que se va a calcular la
	 *            demanda.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void demandaPorLinea(int r) throws Exception {
		ArrayList<Integer> ruta;
		String[] rutaAux;
		rutaAux = new String[rutasfinales.size()];
		rutaAux = ("" + rutasfinales.get(r).get(0)).split(", |\\[|\\]");
		ruta = new ArrayList<Integer>();
		for (int j = 1; j < rutaAux.length; j++) {
			ruta.add(Integer.parseInt(rutaAux[j]));
		}// for
		demandacubierta = 0;
		int[] demandaporlineaaux = new int[lineastotales * 2];
		for (int q = 0; q < datosred.getNumeroNodos(); q++) {
			for (int p = 0; p < datosred.getNumeroNodos(); p++) {
				if (parescubiertos[q][p] == r + 1) {
					demandaporlineaaux[r] += demanda[q][p];
				}// if
			}// for
		}// for
		if (demandaporlineaaux[r] == 0
				|| demandaporlineaaux[r] == demanda[ruta.get(0)][ruta.get(ruta
						.size() - 1)]) {
			// Si la linea no cubre demanda se minimiza
			minimizarRuta(r);
		}// if
	}// demandaPorLinea

	/**
	 * Minimiza la ruta, haciendo que vaya del origen al destino por el camino
	 * más corto.
	 * 
	 * @param i
	 *            Ruta a minimizar.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void minimizarRuta(int i) throws Exception {
		String[] rutaAux = ("" + rutasfinales.get(i).get(0))
				.split(", |\\[|\\]");
		TipoParadasRutaParcial tprp = new TipoParadasRutaParcial(maximasparadas);
		tprp.setParadasRutaParcial(1, Integer.parseInt(rutaAux[1]));
		tprp.setParadasRutaParcial(2,
				Integer.parseInt(rutaAux[rutaAux.length - 1]));
		if (i % 2 == 0) {
			ST[i / 2 + 1].getRutaInicial().setRutaParcial(tprp);
			ST[i / 2 + 1].getRutaInicial().setNumeroParadas(2);
			ST[i / 2 + 1].getRutaInicial().reducirTiempo(
					ST[i / 2 + 1].getRutaInicial().getTiempo());
			ST[i / 2 + 1].getRutaInicial().aumentarTiempo(
					distancia[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])]);
			postProcesamiento((i / 2) + 1, true);
		} else {
			ST[i / 2 + 1].getRutaFinal().setRutaParcial(tprp);
			ST[i / 2 + 1].getRutaFinal().setNumeroParadas(2);
			ST[i / 2 + 1].getRutaFinal().reducirTiempo(
					ST[i / 2 + 1].getRutaFinal().getTiempo());
			ST[i / 2 + 1].getRutaFinal().aumentarTiempo(
					distancia[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])]);
			postProcesamiento((i / 2) + 1, false);
		}// else
		ArrayList<Integer> rutaparcial = new ArrayList<Integer>();
		ArrayList rutaparcial2 = new ArrayList();
		rutaparcial = expandirRuta(i / 2 + 1, i % 2 + 1);
		rutaparcial2.add(rutaparcial);
		rutaparcial2.add(calcularTiempoRuta(rutaparcial, i + 1));
		rutasfinales.set(i, rutaparcial2);
	}// minimizarRuta
}
