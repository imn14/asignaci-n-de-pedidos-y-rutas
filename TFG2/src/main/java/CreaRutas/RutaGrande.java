package CreaRutas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Clase que se encarga de construir las rutas de ámbito nacional y asignarlas
 * transporte.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class RutaGrande {

	/**
	 * Método que ejecuta el algoritmo.
	 * 
	 * @param rutared
	 *            localización del archivo de red.
	 * @param rutademandas
	 *            localizacion del archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutalog
	 *            Ruta donde guardar el archivo log.
	 * @param rutalogdetallado
	 *            Ruta donde guardar el archivo logdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param distanciadiariamaxima
	 *            Indica el tiempo máximo que se puede alcanzar sin descansar.
	 * @param numiterbl
	 *            Numero de iteraciones de la busqueda local.
	 * @param demandapermit
	 *            Demanda permitida sin cubrir.
	 * @param volver
	 *            Indica si las rutas tienen que acabar en la parada en la que
	 *            comienzan o no es necesario.
	 * @param carga
	 *            Indica si las rutas son de carga o de descarga.
	 * @param tiempoMedioParada
	 *            Tiempo medio que se tarda en cargar o descargar en cada parada
	 *            una unidad transportada.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse un error inesperado en tiempo
	 *             de ejecución.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos necesarios.
	 */
	public RutaGrande(String rutared, String rutademandas, String rutaflota,
			String rutaparadas, String rutalog, String rutalogdetallado,
			String rutasave, int distanciadiariamaxima, int numiterbl,
			double demandapermit, boolean volver, boolean carga,
			int tiempoMedioParada) throws Exception, IOException {
		boolean[] limitacionesruta = new boolean[3];
		String[] rutaAux;
		ArrayList<Integer> demandaporlinea = new ArrayList<Integer>();
		boolean aux;
		Grasp graspgrande;
		if (carga) {
			graspgrande = new GraspCarga(rutared, rutademandas, rutaflota,
					rutaparadas, rutalog, rutalogdetallado, rutasave, 50, 1,
					9999, 1, numiterbl, demandapermit, volver,
					tiempoMedioParada);
		} else {
			graspgrande = new GraspDescarga(rutared, rutademandas, rutaflota,
					rutaparadas, rutalog, rutalogdetallado, rutasave, 50, 1,
					9999, 1, numiterbl, demandapermit, volver,
					tiempoMedioParada);
		}// else
		for (int j = 0; j < 3; j++) {
			aux = false;
			rutaAux = ("" + graspgrande.getRutasFinales().get(0).get(0))
					.split(", |\\[|\\]");
			for (int k = 1; k < rutaAux.length; k++) {
				if (graspgrande.getDatosRed().getParadas()[Integer
						.parseInt(rutaAux[k])][j]) {
					aux = true;
				}// if
			}// for
			rutaAux = ("" + graspgrande.getRutasFinales().get(1).get(0))
					.split(", |\\[|\\]");
			for (int k = 1; k < rutaAux.length; k++) {
				if (graspgrande.getDatosRed().getParadas()[Integer
						.parseInt(rutaAux[k])][j]) {
					aux = true;
				}// if
			}// for
			limitacionesruta[j] = aux;
		}// for
		int capacidadmaxima = 0;
		TipoCamion camionasignado = null;
		for (TipoCamion camion : graspgrande.getDatosRed().getCamiones()) {
			aux = false;
			for (int j = 0; j < 3; j++) {
				if (camion.getTipo()[j] != limitacionesruta[j]
						&& limitacionesruta[j] == true) {
					aux = true;
				}// if
			}// for
			if (aux == false && camion.getCapacidad() > capacidadmaxima) {
				camionasignado = camion;
				capacidadmaxima = camion.getCapacidad();
			}// if
		}// for
		if (camionasignado != null) {
			ArrayList<ArrayList<Integer>> rutas = new ArrayList<ArrayList<Integer>>();
			ArrayList<Integer> ruta = new ArrayList<Integer>();
			int distanciadiaria = 0;
			int demandadiaria = 0;
			boolean[][] pase = new boolean[graspgrande.getDatosRed()
					.getNumeroNodos()][graspgrande.getDatosRed()
					.getNumeroNodos()];
			rutaAux = ("" + graspgrande.getRutasFinales().get(0).get(0))
					.split(", |\\[|\\]");
			ruta.add(Integer.parseInt(rutaAux[1]));
			ruta.add(Integer.parseInt(rutaAux[2]));
			distanciadiaria = graspgrande.getDatosRed().getDistancia()[Integer
					.parseInt(rutaAux[1])][Integer.parseInt(rutaAux[2])];
			// Tras obtener una ruta total se divide para cumplir con la
			// distancia diaria máxima.
			if (carga) {
				if (!pase[Integer.parseInt(rutaAux[1])][Integer
						.parseInt(rutaAux[rutaAux.length - 1])]) {
					demandadiaria = graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])];
					pase[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])] = true;
				}
				if (!pase[Integer.parseInt(rutaAux[2])][Integer
						.parseInt(rutaAux[rutaAux.length - 1])]) {
					demandadiaria += graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[2])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])];
					pase[Integer.parseInt(rutaAux[2])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])] = true;
				}
			} else {
				if (!pase[Integer.parseInt(rutaAux[1])][Integer
						.parseInt(rutaAux[2])]) {
					demandadiaria = graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[1])][Integer.parseInt(rutaAux[2])];
					pase[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[2])] = true;
				}
			}// else
			for (int k = 2; k < rutaAux.length - 1; k++) {
				if (distanciadiaria
						+ graspgrande.getDatosRed().getDistancia()[Integer
								.parseInt(rutaAux[k])][Integer
								.parseInt(rutaAux[k + 1])] <= distanciadiariamaxima) {
					distanciadiaria += graspgrande.getDatosRed().getDistancia()[Integer
							.parseInt(rutaAux[k])][Integer
							.parseInt(rutaAux[k + 1])];
					if (carga) {
						if (!pase[Integer.parseInt(rutaAux[k + 1])][Integer
								.parseInt(rutaAux[rutaAux.length - 1])]) {
							demandadiaria += graspgrande.getDatosRed()
									.getDemanda()[Integer
									.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])];
							pase[Integer.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])] = true;
						}
					} else {
						if (!pase[Integer.parseInt(rutaAux[1])][Integer
								.parseInt(rutaAux[k + 1])]) {
							demandadiaria += graspgrande.getDatosRed()
									.getDemanda()[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])];
							pase[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])] = true;
						}
					}// else
					ruta.add(Integer.parseInt(rutaAux[k + 1]));
				} else {
					ArrayList arrayListAux = new ArrayList();
					arrayListAux.add(ruta);
					arrayListAux.add(distanciadiaria);
					demandaporlinea.add(demandadiaria);
					rutas.add(arrayListAux);
					ruta = new ArrayList<Integer>();
					distanciadiaria = graspgrande.getDatosRed().getDistancia()[Integer
							.parseInt(rutaAux[k])][Integer
							.parseInt(rutaAux[k + 1])];
					if (carga) {
						if (!pase[Integer.parseInt(rutaAux[k + 1])][Integer
								.parseInt(rutaAux[rutaAux.length - 1])]) {
							demandadiaria = graspgrande.getDatosRed()
									.getDemanda()[Integer
									.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])];
							pase[Integer.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])] = true;
						}
					} else {
						if (!pase[Integer.parseInt(rutaAux[1])][Integer
								.parseInt(rutaAux[k + 1])]) {
							demandadiaria = graspgrande.getDatosRed()
									.getDemanda()[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])];
							pase[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])] = true;
						}
					}// else
					ruta.add(Integer.parseInt(rutaAux[k]));
					ruta.add(Integer.parseInt(rutaAux[k + 1]));
				}// else
			}// for
			if (distanciadiaria > 0) {
				ArrayList arrayListAux = new ArrayList();
				arrayListAux.add(ruta);
				arrayListAux.add(distanciadiaria);
				demandaporlinea.add(demandadiaria);
				rutas.add(arrayListAux);
				ruta = new ArrayList<Integer>();
				distanciadiaria = 0;
				demandadiaria = 0;
			}// if
			rutaAux = ("" + graspgrande.getRutasFinales().get(1).get(0))
					.split(", |\\[|\\]");
			ruta.add(Integer.parseInt(rutaAux[1]));
			ruta.add(Integer.parseInt(rutaAux[2]));
			distanciadiaria = graspgrande.getDatosRed().getDistancia()[Integer
					.parseInt(rutaAux[1])][Integer.parseInt(rutaAux[2])];
			if (carga) {
				if (!pase[Integer.parseInt(rutaAux[1])][Integer
						.parseInt(rutaAux[rutaAux.length - 1])]) {
					demandadiaria = graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])];
					pase[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])] = true;
				}
				if (!pase[Integer.parseInt(rutaAux[2])][Integer
						.parseInt(rutaAux[rutaAux.length - 1])]) {
					demandadiaria += graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[2])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])];
					pase[Integer.parseInt(rutaAux[2])][Integer
							.parseInt(rutaAux[rutaAux.length - 1])] = true;
				}
			} else {
				if (!pase[Integer.parseInt(rutaAux[1])][Integer
						.parseInt(rutaAux[2])]) {
					demandadiaria = graspgrande.getDatosRed().getDemanda()[Integer
							.parseInt(rutaAux[1])][Integer.parseInt(rutaAux[2])];
					pase[Integer.parseInt(rutaAux[1])][Integer
							.parseInt(rutaAux[2])] = true;
				}
			}// else
			for (int k = 2; k < rutaAux.length - 1; k++) {
				if (distanciadiaria
						+ graspgrande.getDatosRed().getDistancia()[Integer
								.parseInt(rutaAux[k])][Integer
								.parseInt(rutaAux[k + 1])] <= distanciadiariamaxima) {
					distanciadiaria += graspgrande.getDatosRed().getDistancia()[Integer
							.parseInt(rutaAux[k])][Integer
							.parseInt(rutaAux[k + 1])];
					if (carga) {
						if (!pase[Integer.parseInt(rutaAux[k + 1])][Integer
								.parseInt(rutaAux[rutaAux.length - 1])]) {
							demandadiaria += graspgrande.getDatosRed()
									.getDemanda()[Integer
									.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])];
							pase[Integer.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])] = true;
						}
					} else {
						if (!pase[Integer.parseInt(rutaAux[1])][Integer
								.parseInt(rutaAux[k + 1])]) {
							demandadiaria += graspgrande.getDatosRed()
									.getDemanda()[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])];
							pase[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])] = true;
						}
					}// else
					ruta.add(Integer.parseInt(rutaAux[k + 1]));
				} else {
					ArrayList arrayListAux = new ArrayList();
					arrayListAux.add(ruta);
					arrayListAux.add(distanciadiaria);
					demandaporlinea.add(demandadiaria);
					rutas.add(arrayListAux);
					ruta = new ArrayList<Integer>();
					distanciadiaria = graspgrande.getDatosRed().getDistancia()[Integer
							.parseInt(rutaAux[k])][Integer
							.parseInt(rutaAux[k + 1])];
					if (carga) {
						if (!pase[Integer.parseInt(rutaAux[k + 1])][Integer
								.parseInt(rutaAux[rutaAux.length - 1])]) {
							demandadiaria = graspgrande.getDatosRed()
									.getDemanda()[Integer
									.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])];
							pase[Integer.parseInt(rutaAux[k + 1])][Integer
									.parseInt(rutaAux[rutaAux.length - 1])] = true;
						}
					} else {
						if (!pase[Integer.parseInt(rutaAux[1])][Integer
								.parseInt(rutaAux[k + 1])]) {
							demandadiaria = graspgrande.getDatosRed()
									.getDemanda()[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])];
							pase[Integer.parseInt(rutaAux[1])][Integer
									.parseInt(rutaAux[k + 1])] = true;
						}
					}// else
					ruta.add(Integer.parseInt(rutaAux[k]));
					ruta.add(Integer.parseInt(rutaAux[k + 1]));
				}// else
			}// for
			if (distanciadiaria > 0) {
				ArrayList arrayListAux = new ArrayList();
				arrayListAux.add(ruta);
				arrayListAux.add(distanciadiaria);
				demandaporlinea.add(demandadiaria);
				rutas.add(arrayListAux);
				ruta = new ArrayList<Integer>();
				distanciadiaria = 0;
				demandadiaria = 0;
			}// if
			escribirResultadoRutaGrande(rutas, rutasave, graspgrande,
					camionasignado, demandaporlinea);
		} else {
			PrintWriter fichero = new PrintWriter(new BufferedWriter(
					new FileWriter(rutasave)));
			System.out.println("No hay camiones disponibles para esta ruta");
			fichero.println("No hay camiones disponibles para esta ruta");
		}// else
	}// rutaGrande

	/**
	 * Escribe los resultados en un fichero.
	 * 
	 * @param rutas
	 *            Rutas obtenidas a seguir por los transportes.
	 * @param path
	 *            Ruta del archivo donde se va a guardar.
	 * @param grasp
	 *            Objeto grasp utilizado para crear las rutas.
	 * @param camion
	 *            Transporte asignado para cubrir la ruta.
	 * @param demandaporlinea
	 *            Lista en la que se indica de forma ordenada la demanda
	 *            cubierta por cada ruta.
	 * @throws java.io.IOException
	 *             Lanzada si no encuentra la ruta.
	 */
	private static void escribirResultadoRutaGrande(
			ArrayList<ArrayList<Integer>> rutas, String path, Grasp grasp,
			TipoCamion camion, ArrayList<Integer> demandaporlinea)
			throws IOException {
		int demandacubierta = 0;
		PrintWriter fichero = new PrintWriter(new BufferedWriter(
				new FileWriter(path)));
		String cump = "\n";
		if (grasp instanceof GraspCarga) {
			cump += ((GraspCarga) grasp).cumpleTiempo();
		} else if (grasp instanceof GraspDescarga) {
			cump += ((GraspDescarga) grasp).cumpleTiempo();
		}
		if (!cump.equals("\n\n")) {
			System.out.println(cump);
			fichero.println(cump);
			System.out.println();
			fichero.println();
		} // if
		System.out.println("\n Un camión de tipo " + camion.getIdTipo()
				+ " con " + camion.getCapacidad()
				+ " unidades de capacidad ha sido asignado a esta ruta.\n");
		fichero.println("\n Un camión de tipo " + camion.getIdTipo() + " con "
				+ camion.getCapacidad()
				+ " unidades de capacidad ha sido asignado a esta ruta.\n");
		for (int j = 0; j < rutas.size(); j++) {
			System.out.println("Ruta :" + j);
			System.out.println((((ArrayList) rutas.get(j)).get(0)).toString());
			System.out.println("Tiempo : "
					+ (((ArrayList) rutas.get(j)).get(1)).toString());
			System.out.println("Demanda cubierta : " + demandaporlinea.get(j));
			System.out.println();
			fichero.println("Ruta :" + j);
			fichero.println((((ArrayList) rutas.get(j)).get(0)).toString());
			fichero.println("Tiempo : "
					+ (((ArrayList) rutas.get(j)).get(1)).toString());
			fichero.println("Demanda cubierta : " + demandaporlinea.get(j));
			fichero.println();
			demandacubierta += demandaporlinea.get(j);
		}// for
		System.out.println("Demanda Total : " + grasp.getDemandaTotal());
		System.out.println("Demanda Cubierta : " + demandacubierta);
		fichero.println("Demanda Total : " + grasp.getDemandaTotal());
		fichero.println("Demanda Cubierta : " + demandacubierta);
		System.out.println();
		fichero.println();
		if (demandacubierta < grasp.getDemandaTotal()
				* grasp.getDemandaPermitida()) {
			System.out
					.println("*ATENCION!!! DEMANDA MÍNIMA NO CUBIERTA POR LAS RUTAS*");
			fichero.println("*ATENCION!!! DEMANDA MÍNIMA NO CUBIERTA POR LAS RUTAS*");
		}// if
		fichero.close();
	}// escribirResultadoRutaGrande
}
