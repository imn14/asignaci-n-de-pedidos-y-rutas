package CreaRutas;

/**
 * Clase que contiene la estructura de datos de las rutas parciales.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class TipoRutaParcial {

	/**
	 * Numero de paradas que tiene la ruta.
	 */
	private int numeroparadas;

	/**
	 * Tiempo que se tarda en recorrer esa ruta.
	 */
	private int tiempo = 0;

	/**
	 * Paradas concretas de una ruta.
	 */
	private TipoParadasRutaParcial paradasrutaparcial;

	/**
	 * Constructor, crea una nueva instancia del tipo RutaParcial.
	 * 
	 * @param maximasparadas
	 *            Recibe el número máximo de paradas que puede tener una ruta
	 *            parcial.
	 */
	public TipoRutaParcial(int maximasparadas) {
		numeroparadas = 0;
		paradasrutaparcial = new TipoParadasRutaParcial(maximasparadas);
	}

	/**
	 * Modifica el numero de paradas del objeto por el nuevo número de paradas
	 * recibido.
	 * 
	 * @param numero
	 *            Numero de paradas.
	 * @see #getNumeroParadas
	 */
	public void setNumeroParadas(int numero) {
		numeroparadas = numero;

	}

	/**
	 * Establece un objeto del tipo ParadasRutaParcial como ruta parcial.
	 * 
	 * @param paradas
	 *            Objeto de tipo ParadasRutaParcial.
	 */
	public void setRutaParcial(TipoParadasRutaParcial paradas) {
		paradasrutaparcial = paradas;
	}

	/**
	 * Consulta del numero de paradas de la ruta parcial.
	 * 
	 * @return Numero de paradas de la ruta parcial.
	 * @see #setNumeroParadas
	 */
	public int getNumeroParadas() {
		return numeroparadas;
	}

	/**
	 * Consulta de un objeto del tipo ParadasRutaParcial.
	 * 
	 * @return Objeto del tipo ParadasRutaParcial instanciado dentro del tipo
	 *         RutaParcial.
	 */
	public TipoParadasRutaParcial getParadasRutaParcial() {
		return paradasrutaparcial;
	}

	/**
	 * Consulta el tiempo de la ruta.
	 * 
	 * @return Devuelve un entero con el tiempo que se tarda en recorrer una
	 *         ruta.
	 */
	public int getTiempo() {
		return tiempo;
	}

	/**
	 * Aumenta el tiempo de una linea.
	 * 
	 * @param t
	 *            Tiempo a añadir.
	 */
	public void aumentarTiempo(int t) {
		tiempo += t;
	}

	/**
	 * Reduce tiempo a una ruta.
	 * 
	 * @param t
	 *            Reduce en t el tiempo total de la ruta.
	 */
	public void reducirTiempo(int t) {
		tiempo -= t;
	}
}
