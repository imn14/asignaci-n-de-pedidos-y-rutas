package CreaRutas;

import java.io.*;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Clase que se encarga de leer los datos de entrada.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class LeerDatos {

	/**
	 * Número de nodos de la red.
	 */
	private int numNodos;

	/**
	 * Demanda total de la red.
	 */
	private int demandatotal;

	/**
	 * Variable con el valor de infinito.
	 */
	private int inf = 9999;

	/**
	 * Variable con la capacidad del transporte que la tiene mayor.
	 */
	private int maximacapacidad = 0;

	/**
	 * Matriz con los datos de las demandas de la red.
	 */
	private int demanda[][];

	/**
	 * Matriz que indica si las demandas de la red son urgentes o no.
	 */
	private boolean demandaurgente[][];

	/**
	 * Matriz con los datos de las distancias de la red.
	 */
	private int distancia[][];

	/**
	 * ArrayList de camiones.
	 */
	private ArrayList<TipoCamion> camiones;

	/**
	 * Matriz con los datos de las paradas especiales de la red.
	 */
	private boolean paradas[][];

	/**
	 * Matriz con las ventana de tiempo correspondientes a cada parada.
	 */
	private LocalTime[][] ventanasTiempo;

	/**
	 * Matriz con los origenes y destinos de las rutas semiautomáticas.
	 */
	private int[][] rutasSemi;

	/**
	 * ArrayList con las rutas manuales.
	 */
	private ArrayList<ArrayList<Integer>> rutasManuales;

	/**
	 * Número de minutos máximos que puede durar el recorrido de una ruta total.
	 */
	private int tiempomaxlinea;

	/**
	 * Hora de inicio de las rutas.
	 */
	private LocalTime horainicio;

	/**
	 * Método usado para leer los datos de la red.
	 *
	 * @param pathred
	 *            Localizacion del fichero de distancias.
	 * @param pathdemandas
	 *            Localizacion del fichero de demandas.
	 * @param pathflota
	 *            Localizacion del fichero de flota.
	 * @param pathparadas
	 *            Localizacion del fichero de paradas.
	 * @param pathtiempo
	 *            Localizacion del fichero de tiempos.
	 * @param tiempomaxlinea
	 *            Número de minutos máximos que puede durar el recorrido de una
	 *            ruta total.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             ficheros.
	 */
	public LeerDatos(String pathred, String pathdemandas, String pathflota,
			String pathparadas, String pathtiempo, int tiempomaxlinea)
			throws IOException {
		this.tiempomaxlinea = tiempomaxlinea;
		this.leerDatosRed(pathred);
		this.leerDatosDemandas(pathdemandas);
		this.leerDatosFlota(pathflota);
		this.leerDatosParadas(pathparadas);
		this.leerVentanasTiempo(pathtiempo);
		paradasInutilizadas();
	}// Constructor

	/**
	 * Método usado para leer los datos de la red.
	 *
	 * @param pathred
	 *            Localizacion del fichero de distancias.
	 * @param pathdemandas
	 *            Localizacion del fichero de demandas.
	 * @param pathflota
	 *            Localizacion del fichero de flota.
	 * @param pathparadas
	 *            Localizacion del fichero de paradas.
	 * @param tiempomaxlinea
	 *            Número de minutos máximos que puede durar el recorrido de una
	 *            ruta total.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             ficheros.
	 */
	public LeerDatos(String pathred, String pathdemandas, String pathflota,
			String pathparadas, int tiempomaxlinea) throws IOException {
		this.tiempomaxlinea = tiempomaxlinea;
		this.leerDatosRed(pathred);
		this.leerDatosDemandas(pathdemandas);
		this.leerDatosFlota(pathflota);
		this.leerDatosParadas(pathparadas);
		horainicio = LocalTime.of(0, 0);
		ventanasTiempo = new LocalTime[numNodos][2];
		// Inicializamos la matriz de tiempos desde la hora de salida hasta que
		// llega al tiempo máximo de recorrido
		for (int i = 0; i < numNodos; i++) {
			ventanasTiempo[i][0] = LocalTime.of(0, 0);
			ventanasTiempo[i][1] = LocalTime.of(0, 0).plusMinutes(
					tiempomaxlinea);
		}// for
		paradasInutilizadas();
	}// Constructor

	/**
	 * Método usado para leer los datos de la red.
	 *
	 * @param pathred
	 *            Localizacion del fichero de distancias.
	 * @param pathdemandas
	 *            Localizacion del fichero de demandas.
	 * @param pathflota
	 *            Localizacion del fichero de flota.
	 * @param pathparadas
	 *            Localizacion del fichero de paradas.
	 * @param pathtiempo
	 *            Localizacion del fichero de tiempos.
	 * @param pathsemi
	 *            Localizacion del fichero de rutas semiautomáticas.
	 * @param tiempomaxlinea
	 *            Número de minutos máximos que puede durar el recorrido de una
	 *            ruta total.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             ficheros.
	 */
	public LeerDatos(String pathred, String pathdemandas, String pathflota,
			String pathparadas, String pathtiempo, String pathsemi,
			int tiempomaxlinea) throws IOException {
		this.tiempomaxlinea = tiempomaxlinea;
		this.leerDatosRed(pathred);
		this.leerDatosDemandas(pathdemandas);
		this.leerDatosFlota(pathflota);
		this.leerDatosParadas(pathparadas);
		this.leerVentanasTiempo(pathtiempo);
		this.leerRutasSemiautomaticas(pathsemi);
		paradasInutilizadas();
	}// Constructor

	/**
	 * Método usado para leer los datos de la red.
	 *
	 * @param pathred
	 *            Localizacion del fichero de distancias.
	 * @param pathdemandas
	 *            Localizacion del fichero de demandas.
	 * @param pathflota
	 *            Localizacion del fichero de flota.
	 * @param pathparadas
	 *            Localizacion del fichero de paradas.
	 * @param pathtiempo
	 *            Localizacion del fichero de tiempos.
	 * @param tiempomaxlinea
	 *            Número de minutos máximos que puede durar el recorrido de una
	 *            ruta total.
	 * @param pathmanual
	 *            Localizacion del fichero de rutas manuales.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             ficheros.
	 */
	public LeerDatos(String pathred, String pathdemandas, String pathflota,
			String pathparadas, String pathtiempo, int tiempomaxlinea,
			String pathmanual) throws IOException {
		this.tiempomaxlinea = tiempomaxlinea;
		this.leerDatosRed(pathred);
		this.leerDatosDemandas(pathdemandas);
		this.leerDatosFlota(pathflota);
		this.leerDatosParadas(pathparadas);
		this.leerVentanasTiempo(pathtiempo);
		this.leerRutasManuales(pathmanual);
		paradasInutilizadas();
	}// Constructor

	/**
	 * Inicializa la matriz de distancias con todos los valores a infinito.
	 * 
	 * @param nNodos
	 *            Numero de nodos.
	 */
	private void incializarDistancia(int nNodos) {

		distancia = new int[nNodos][nNodos];
		for (int i = 0; i < nNodos; i++) {
			for (int j = 0; j < nNodos; j++) {
				distancia[i][j] = inf;
			}
		}
	}// inicializarDistancia

	/**
	 * Lee una linea de un archivo de texto.
	 * 
	 * @param linea
	 *            Recibe la linea a leer.
	 * @return Devuelve un array con los datos leidos en orden.
	 */
	private String[] leerLinea(String linea) {
		String aux = linea.trim();// Quita los espacios al principio de cada
									// línea
		String auxArray[] = aux.split("\\s+");
		return auxArray;
	}// leerLinea

	/**
	 * Método que lee el archivo de red y genera un matriz con los datos de las
	 * paradas interconectadas y sus distancias correspondientes.
	 * 
	 * @param pathred
	 *            Localización del archivo con los datos de la red.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerDatosRed(String pathred) throws IOException {
		String s;
		BufferedReader archivoRed = new BufferedReader(new FileReader(pathred));

		numNodos = 0;
		s = archivoRed.readLine();// Lee la primera línea del archivo de red
		String lineaArray[] = leerLinea(s);
		// Obtenemos el número de nodos del archivo de red
		numNodos = Integer.parseInt(lineaArray[0]);
		incializarDistancia(numNodos);

		while ((s = archivoRed.readLine()) != null) {// Mientras haya lineas
			// Lee la línea del fichero (nodoInicial,nodoFinal,distancia)
			lineaArray = leerLinea(s);
			distancia[Integer.parseInt(lineaArray[0])][Integer
					.parseInt(lineaArray[1])] = Integer.parseInt(lineaArray[2]);
		}// while
		archivoRed.close();
	}// leerDatosRed

	/**
	 * Método que lee el archivo de demandas y genera una matriz con los datos
	 * de las demandas de la red.
	 * 
	 * @param pathdemandas
	 *            Localización del archivo con los datos de las demandas.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerDatosDemandas(String pathdemandas) throws IOException {
		String s;
		BufferedReader archivoDemandas = new BufferedReader(new FileReader(
				pathdemandas));
		demanda = new int[numNodos][numNodos];
		demandaurgente = new boolean[numNodos][numNodos];
		// Inicializamos a cero la matriz de demandas
		for (int i = 0; i < numNodos; i++) {
			for (int j = 0; j < numNodos; j++) {
				demanda[i][j] = 0;
				demandaurgente[i][j] = false;
			}// for
		}// for
			// Inicializamos a cero demandaTotal
		demandatotal = 0;
		while ((s = archivoDemandas.readLine()) != null) {// Mientras haya
															// líneas
			// Lee la línea del fichero
			// (nodoInicial,nodoFinal,demanda,(urgente))
			String lineaArray[] = leerLinea(s);
			demanda[Integer.parseInt(lineaArray[0])][Integer
					.parseInt(lineaArray[1])] = Integer.parseInt(lineaArray[2]);
			demandatotal = demandatotal + Integer.parseInt(lineaArray[2]);
			if (lineaArray.length > 3 && lineaArray[3].equals("urgente")) {
				demandaurgente[Integer.parseInt(lineaArray[0])][Integer
						.parseInt(lineaArray[1])] = true;
			}// if
		}// while
		archivoDemandas.close();
	}// leerDatosDemandas

	/**
	 * Método que lee la flota de camiones de la red y la almacena.
	 * 
	 * @param pathflota
	 *            Localización del archivo con los datos de la flota
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerDatosFlota(String pathflota) throws IOException {
		String s;
		BufferedReader archivoFlota = new BufferedReader(new FileReader(
				pathflota));
		camiones = new ArrayList<TipoCamion>();
		s = archivoFlota.readLine();
		while ((s = archivoFlota.readLine()) != null) {// Mientras haya líneas
			// Lee la línea del fichero (numeroCamiones, tipo, capacidad)
			String lineaArray[] = leerLinea(s);
			camiones.add(new TipoCamion(Integer.parseInt(lineaArray[0]),
					Integer.parseInt(lineaArray[1]), Integer
							.parseInt(lineaArray[2])));
			if (Integer.parseInt(lineaArray[2]) > maximacapacidad) {
				maximacapacidad = Integer.parseInt(lineaArray[2]);
			}// if
		}// while
		archivoFlota.close();
	}// leerDatosFlota

	/**
	 * Método que lee el tipo de parada y lo almacena.
	 * 
	 * @param pathparadas
	 *            Localización del archivo con los datos de las paradas
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerDatosParadas(String pathparadas) throws IOException {
		String s;
		int aux = 0;
		BufferedReader archivoParadas = new BufferedReader(new FileReader(
				pathparadas));
		paradas = new boolean[numNodos][4];
		// Inicializamos a false la matriz de paradas
		for (int i = 0; i < numNodos; i++) {
			for (int j = 0; j < 4; j++) {
				paradas[i][j] = false;
			}// for
		}// for
		while ((s = archivoParadas.readLine()) != null) {// Mientras haya líneas
			// Lee la línea del fichero, si es parada la añado y si es de tipo
			// de parada aumento aux
			String lineaArray[] = leerLinea(s);
			if (lineaArray[0].equals("Estrecha")) {
				aux = 0;
			} else if (lineaArray[0].equals("Tunel")) {
				aux = 1;
			} else if (lineaArray[0].equals("Ligero")) {
				aux = 2;
			} else if (lineaArray[0].equals("Inutilizada")) {
				aux = 3;
			} else {
				paradas[Integer.parseInt(lineaArray[0])][aux] = true;
			}
		}// while
		archivoParadas.close();
	}// fin leerDatosParadas

	/**
	 * Método que lee el archivo de ventanas de tiempo y genera una matriz con
	 * los datos de las ventanas de tiempo de cada parada.
	 * 
	 * @param pathtiempo
	 *            Localización del archivo con las ventanas de tiempo de las
	 *            paradas.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerVentanasTiempo(String pathtiempo) throws IOException {
		String s;
		BufferedReader archivoVentanasdeTiempo = new BufferedReader(
				new FileReader(pathtiempo));
		ventanasTiempo = new LocalTime[numNodos][2];

		s = archivoVentanasdeTiempo.readLine();// Lee la primera línea del
												// archivo de red (horaInicio)
		String lineaArray[] = leerLinea(s);
		horainicio = LocalTime.of(Integer.parseInt(lineaArray[0]),
				Integer.parseInt(lineaArray[1]));
		// Inicializamos a tiempo máximo de línea la matriz de tiempos
		for (int i = 0; i < numNodos; i++) {
			ventanasTiempo[i][0] = LocalTime.of(
					Integer.parseInt(lineaArray[0]),
					Integer.parseInt(lineaArray[1]));
			ventanasTiempo[i][1] = LocalTime.of(
					Integer.parseInt(lineaArray[0]),
					Integer.parseInt(lineaArray[1]))
					.plusMinutes(tiempomaxlinea);
		}// for
		while ((s = archivoVentanasdeTiempo.readLine()) != null) {
			// Mientras haya líneas lee la línea del fichero (parada,
			// tiempoInicial,tiempoFinal)
			lineaArray = leerLinea(s);
			ventanasTiempo[Integer.parseInt(lineaArray[0])][0] = LocalTime.of(
					Integer.parseInt(lineaArray[1]),
					Integer.parseInt(lineaArray[2]));
			ventanasTiempo[Integer.parseInt(lineaArray[0])][1] = LocalTime.of(
					Integer.parseInt(lineaArray[3]),
					Integer.parseInt(lineaArray[4]));
		}// while
		archivoVentanasdeTiempo.close();
	}// leerVentanasTiempo

	/**
	 * Método que lee los orígenes y destinos de las rutas semiautomáticas.
	 * 
	 * @param pathsemi
	 *            Localización del archivo con los orígenes y destinos de las
	 *            rutas semiautomáticas.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerRutasSemiautomaticas(String pathsemi) throws IOException {
		String s;
		BufferedReader archivoRS = new BufferedReader(new FileReader(pathsemi));
		int i = 0;
		int lineast = 0;
		while ((s = archivoRS.readLine()) != null) {
			lineast++;
		}
		archivoRS.close();
		archivoRS = new BufferedReader(new FileReader(pathsemi));
		i = 0;
		rutasSemi = new int[lineast][3];
		while ((s = archivoRS.readLine()) != null) {// Mientras haya líneas lee
													// la línea del fichero
													// (paradasalida, extremo,
													// paradallegada)
			String lineaArray[] = leerLinea(s);
			rutasSemi[i][0] = (Integer.parseInt(lineaArray[0]));
			rutasSemi[i][1] = (Integer.parseInt(lineaArray[1]));
			rutasSemi[i][2] = (Integer.parseInt(lineaArray[2]));
			i++;
		}// while
		archivoRS.close();
	}// leerRutasSemiautomáticas

	/**
	 * Método que lee los orígenes y destinos de las rutas manuales.
	 * 
	 * @param pathmanual
	 *            Localización del archivo con las rutas manuales.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar el archivo.
	 */
	private void leerRutasManuales(String pathmanual) throws IOException {
		String s;
		BufferedReader archivoRM = new BufferedReader(
				new FileReader(pathmanual));
		rutasManuales = new ArrayList<ArrayList<Integer>>();
		int o = 0;
		while ((s = archivoRM.readLine()) != null) {// Mientras haya líneas leo
													// la línea del fichero
			String lineaArray[] = leerLinea(s);
			int i = Integer.parseInt(lineaArray[1]) - 1;
			while (o < i) {
				rutasManuales.add(null);
				o++;
			}
			o++;
			rutasManuales.add(new ArrayList<Integer>());
			s = archivoRM.readLine();
			lineaArray = leerLinea(s);
			for (int j = 0; j < lineaArray.length; j++) {
				rutasManuales.get(i).add(Integer.parseInt(lineaArray[j]));
			}
		}// while
		archivoRM.close();
	}// leerRutasManuales

	/**
	 * Método que asigna valor infinito a las distancias con paradas
	 * inutilizadas.
	 */
	private void paradasInutilizadas() {
		for (int i = 0; i < numNodos; i++) {
			if (paradas[i][3]) {
				for (int j = 0; j < numNodos; j++) {
					distancia[i][j] = inf;
					distancia[j][i] = inf;
				}// for
			}// if
		}// for
	}

	/**
	 * Método usado para acceder a la matriz de demandas de la red.
	 * 
	 * @return Devuelve la matriz de demandas.
	 */
	public int[][] getDemanda() {
		return demanda;
	}// getDemanda

	/**
	 * Método usado para acceder a la matriz de demandas urgentes de la red.
	 * 
	 * @return Devuelve una matriz de demandas urgentes donde localizamos las
	 *         demandas que han de cumplirse con mayor prioridad de la red.
	 */
	public boolean[][] getDemandaUrgente() {
		return demandaurgente;
	}// getDemanda

	/**
	 * Consulta la matriz de distancias.
	 * 
	 * @return Devuelve la matriz con las distancias.
	 */
	public int[][] getDistancia() {
		return distancia;
	}// getDistancia

	/**
	 * Consulta el número de nodos.
	 * 
	 * @return Devuelve el número de nodos de la red.
	 */
	public int getNumeroNodos() {
		return numNodos;
	}// getNumeroNodos

	/**
	 * Consulta la demanda total de la red.
	 * 
	 * @return Devuelve la demanda total de la red.
	 */
	public int getDemandaTotal() {
		return demandatotal;
	}// getDemandaTotal

	/**
	 * Consulta la flota.
	 * 
	 * @return Devuelve la flota.
	 */
	public ArrayList<TipoCamion> getCamiones() {
		return camiones;
	}// getCamiones

	/**
	 * Consulta las paradas de la red.
	 * 
	 * @return Devuelve las paradas de la red.
	 */
	public boolean[][] getParadas() {
		return paradas;
	}// getParadas

	/**
	 * Consulta las ventanas de tiempo de la red.
	 * 
	 * @return Devuelve las ventanas de tiempo de la red.
	 */
	public LocalTime[][] getTiempos() {
		return ventanasTiempo;
	}// getTiempos

	/**
	 * Consulta la hora de inicio de la red.
	 * 
	 * @return Devuelve la hora de inicio de la red.
	 */
	public LocalTime getHorainicio() {
		return horainicio;
	}

	/**
	 * Consulta la matriz con los origenes y destinos de las rutas
	 * semiautomáticas.
	 * 
	 * @return Devuelve la matriz con los origenes y destinos de las rutas
	 *         semiautomáticas.
	 */
	public int[][] getRutasSemi() {
		return rutasSemi;
	}

	/**
	 * Consulta la lista con las rutas manuales.
	 * 
	 * @return Devuelve la lista con las rutas manuales.
	 */
	public ArrayList<ArrayList<Integer>> getRutasManual() {
		return rutasManuales;
	}

	/**
	 * Consulta la capacidad del transporte que la tiene mayor.
	 * 
	 * @return Devuelve la capacidad del transporte que la tiene mayor.
	 */
	public int getMaximaCapacidad() {
		return maximacapacidad;
	}// getMaximaCapacidad
}
