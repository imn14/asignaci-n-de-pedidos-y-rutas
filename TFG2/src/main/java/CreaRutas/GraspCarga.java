package CreaRutas;

import java.io.IOException;
import java.util.ArrayList;

import javax.enterprise.inject.New;

/**
 * Clase que se encarga de construir las rutas de carga.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class GraspCarga extends MejoraGraspCarga {

	/**
	 * Constructor.
	 * 
	 * @param rutared
	 *            Ruta con el archivo de red
	 * @param rutademandas
	 *            Ruta con el archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param maxparadaspermit
	 *            Maximas paradas permitidas para la ruta.
	 * @param lineastot
	 *            Numero de lineas de la ruta.
	 * @param tiempomaxlinea
	 *            Tiempo máximo de la linea.
	 * @param tiempominlinea
	 *            Tiempo mínimo de la linea.
	 * @param numiterbl
	 *            Numero de iteraciones de la busqueda local.
	 * @param demandapermit
	 *            Demanda permitida sin cubrir.
	 * @param volver
	 *            Indica si las rutas tienen que acabar en la parada en la que
	 *            comienzan o no es necesario.
	 * @param tiempoMedioParada
	 *            Tiempo medio que se tarda en cargar o descargar en cada parada
	 *            una unidad transportada.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar alguno de los archivos en las
	 *             rutas especificadas.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse algún error en tiempo de
	 *             ejecución.
	 */
	public GraspCarga(String rutared, String rutademandas, String rutaflota,
			String rutaparadas, String rutatiempo, String rutalog,
			String rutalogdetallado, String rutasave, int maxparadaspermit,
			int lineastot, int tiempomaxlinea, int tiempominlinea,
			int numiterbl, double demandapermit, boolean volver,
			int tiempoMedioParada) throws IOException, Exception {

		tiemp = System.currentTimeMillis();
		maximasparadaspermitidas = maxparadaspermit;
		lineastotales = lineastot;
		tmaxlinea = tiempomaxlinea;
		tminlinea = tiempominlinea;
		numiteracionesbl = numiterbl;
		demandapermitida = demandapermit;
		tiempomedioparada = tiempoMedioParada;
		this.volver = volver;

		ejecutarGRASP(rutared, rutademandas, rutaflota, rutaparadas,
				rutatiempo, rutalog, rutalogdetallado, rutasave);
	}// Constructor

	/**
	 * Constructor.
	 * 
	 * @param rutared
	 *            Ruta con el archivo de red
	 * @param rutademandas
	 *            Ruta con el archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param maxparadaspermit
	 *            Maximas paradas permitidas para la ruta.
	 * @param lineastot
	 *            Numero de lineas de la ruta.
	 * @param tiempomaxlinea
	 *            Tiempo máximo de la linea.
	 * @param tiempominlinea
	 *            Tiempo mínimo de la linea.
	 * @param numiterbl
	 *            Numero de iteraciones de la busqueda local.
	 * @param demandapermit
	 *            Demanda permitida sin cubrir.
	 * @param volver
	 *            Indica si las rutas tienen que acabar en la parada en la que
	 *            comienzan o no es necesario.
	 * @param tiempoMedioParada
	 *            Tiempo medio que se tarda en cargar o descargar en cada parada
	 *            una unidad transportada.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar alguno de los archivos en las
	 *             rutas especificadas.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse algún error en tiempo de
	 *             ejecución.
	 */
	public GraspCarga(String rutared, String rutademandas, String rutaflota,
			String rutaparadas, String rutalog, String rutalogdetallado,
			String rutasave, int maxparadaspermit, int lineastot,
			int tiempomaxlinea, int tiempominlinea, int numiterbl,
			double demandapermit, boolean volver, int tiempoMedioParada)
			throws IOException, Exception {

		tiemp = System.currentTimeMillis();
		maximasparadaspermitidas = maxparadaspermit;
		lineastotales = lineastot;
		tmaxlinea = tiempomaxlinea;
		tminlinea = tiempominlinea;
		numiteracionesbl = numiterbl;
		demandapermitida = demandapermit;
		tiempomedioparada = tiempoMedioParada;
		this.volver = volver;

		ejecutarGRASP(rutared, rutademandas, rutaflota, rutaparadas, rutalog,
				rutalogdetallado, rutasave);
	}// Constructor

	/**
	 * Constructor.
	 * 
	 * @param rutared
	 *            Ruta con el archivo de red
	 * @param rutademandas
	 *            Ruta con el archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param rutasemi
	 *            Ruta donde están los datos de las rutas semiautomáticas.
	 * @param maxparadaspermit
	 *            Maximas paradas permitidas para la ruta.
	 * @param tiempomaxlinea
	 *            Tiempo máximo de la linea.
	 * @param tiempominlinea
	 *            Tiempo mínimo de la linea.
	 * @param numiterbl
	 *            Numero de iteraciones de la busqueda local.
	 * @param demandapermit
	 *            Demanda permitida sin cubrir.
	 * @param tiempoMedioParada
	 *            Tiempo medio que se tarda en cargar o descargar en cada parada
	 *            una unidad transportada.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar alguno de los archivos en las
	 *             rutas especificadas.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse algún error en tiempo de
	 *             ejecución.
	 */
	public GraspCarga(String rutared, String rutademandas, String rutaflota,
			String rutaparadas, String rutatiempo, String rutalog,
			String rutalogdetallado, String rutasave, String rutasemi,
			int maxparadaspermit, int tiempomaxlinea, int tiempominlinea,
			int numiterbl, double demandapermit, int tiempoMedioParada)
			throws IOException, Exception {

		tiemp = System.currentTimeMillis();
		maximasparadaspermitidas = maxparadaspermit;
		tmaxlinea = tiempomaxlinea;
		tminlinea = tiempominlinea;
		numiteracionesbl = numiterbl;
		demandapermitida = demandapermit;
		tiempomedioparada = tiempoMedioParada;
		ejecutarGRASP(rutared, rutademandas, rutaflota, rutaparadas,
				rutatiempo, rutalog, rutalogdetallado, rutasave, rutasemi);
	}// Constructor

	/**
	 * Constructor.
	 * 
	 * @param rutared
	 *            Ruta con el archivo de red
	 * @param rutademandas
	 *            Ruta con el archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param maxparadaspermit
	 *            Maximas paradas permitidas para la ruta.
	 * @param tiempomaxlinea
	 *            Tiempo máximo de la linea.
	 * @param tiempominlinea
	 *            Tiempo mínimo de la linea.
	 * @param demandapermit
	 *            Demanda permitida sin cubrir.
	 * @param tiempoMedioParada
	 *            Tiempo medio que se tarda en cargar o descargar en cada parada
	 *            una unidad transportada.
	 * @param rutamanual
	 *            Ruta donde están los datos de las rutas manuales.
	 * @throws java.io.IOException
	 *             Lanzada en caso de no encontrar alguno de los archivos en las
	 *             rutas especificadas.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse algún error en tiempo de
	 *             ejecución.
	 */
	public GraspCarga(String rutared, String rutademandas, String rutaflota,
			String rutaparadas, String rutatiempo, String rutalog,
			String rutalogdetallado, String rutasave, int maxparadaspermit,
			int tiempomaxlinea, int tiempominlinea, double demandapermit,
			int tiempoMedioParada, String rutamanual) throws IOException,
			Exception {
		maximasparadaspermitidas = maxparadaspermit;
		tmaxlinea = tiempomaxlinea;
		tminlinea = tiempominlinea;
		demandapermitida = demandapermit;
		tiempomedioparada = tiempoMedioParada;
		ejecutarGRASPManual(rutared, rutademandas, rutaflota, rutaparadas,
				rutatiempo, rutalog, rutalogdetallado, rutasave, rutamanual);
	}// Constructor

	/**
	 * Método que ejecuta el algoritmo.
	 * 
	 * @param rutared
	 *            localización del archivo de red.
	 * @param rutademandas
	 *            localizacion del archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse un error inesperado en tiempo
	 *             de ejecución.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos necesarios.
	 */
	public void ejecutarGRASP(String rutared, String rutademandas,
			String rutaflota, String rutaparadas, String rutatiempo,
			String rutalog, String rutalogdetallado, String rutasave)
			throws IOException, Exception {
		boolean salir = false;
		logpath = rutalog;
		logdetalladopath = rutalogdetallado;
		while (!salir) {
			maximasparadas++;
			salir = true;
			try {
				if (maximasparadaspermitidas < maximasparadas)
					throw new Exception("Paradas máximas alcanzadas");
				obtenerMatrices(rutared, rutademandas, rutaflota, rutaparadas,
						rutatiempo, tmaxlinea);
				distanciaoriginal = new int[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				for (int i = 0; i < datosred.getNumeroNodos(); i++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						distanciaoriginal[i][j] = datosred.getDistancia()[i][j];
					}
				}
				rutasCubiertas = new boolean[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				floyd = new DistanciasMinimas(datosred.getNumeroNodos(),
						datosred.getDistancia());
				floyd.floyd();
				rutasDirectas();
				distancia = datosred.getDistancia();
				logstring = "";
				logdetalladostring = "";
				rutasIniciales();
				iniciar();
				calcularTiempoInicial();
				asignarPuntos();
				pasarRutasArrayList();
				evaluarParesParadas();
				siguenCubiertas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				eliminarParadas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				tiemp = System.currentTimeMillis() - tiemp;
				escribeLog(true);
				escribeLogDetallado(true);
			}// try
			catch (ArrayIndexOutOfBoundsException e) {
				salir = false;
			}// catch
			catch (NullPointerException e) {
				salir = false;
			}// catch
		}// while
	}// ejecutar

	/**
	 * Método que ejecuta el algoritmo.
	 * 
	 * @param rutared
	 *            localización del archivo de red.
	 * @param rutademandas
	 *            localizacion del archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse un error inesperado en tiempo
	 *             de ejecución.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos necesarios.
	 */
	public void ejecutarGRASP(String rutared, String rutademandas,
			String rutaflota, String rutaparadas, String rutalog,
			String rutalogdetallado, String rutasave) throws IOException,
			Exception {
		boolean salir = false;
		logpath = rutalog;
		logdetalladopath = rutalogdetallado;
		while (!salir) {
			maximasparadas++;
			salir = true;
			try {
				if (maximasparadaspermitidas < maximasparadas)
					throw new Exception("Paradas máximas alcanzadas");
				obtenerMatrices(rutared, rutademandas, rutaflota, rutaparadas,
						tmaxlinea);
				distanciaoriginal = new int[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				for (int i = 0; i < datosred.getNumeroNodos(); i++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						distanciaoriginal[i][j] = datosred.getDistancia()[i][j];
					}
				}
				rutasCubiertas = new boolean[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				floyd = new DistanciasMinimas(datosred.getNumeroNodos(),
						datosred.getDistancia());
				floyd.floyd();
				rutasDirectas();
				distancia = datosred.getDistancia();
				logstring = "";
				logdetalladostring = "";
				rutasIniciales();
				iniciar();
				calcularTiempoInicial();
				asignarPuntos();
				pasarRutasArrayList();
				evaluarParesParadas();
				siguenCubiertas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				eliminarParadas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				tiemp = System.currentTimeMillis() - tiemp;
				escribeLog(true);
				escribeLogDetallado(true);
			}// try
			catch (ArrayIndexOutOfBoundsException e) {
				salir = false;
			}// catch
			catch (NullPointerException e) {
				salir = false;
			}// catch
		}// while
	}// ejecutar

	/**
	 * Método que ejecuta el algoritmo.
	 * 
	 * @param rutared
	 *            localización del archivo de red.
	 * @param rutademandas
	 *            localizacion del archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param rutasemi
	 *            Ruta donde están los datos de las rutas semiautomáticas.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producirse un error inesperado en tiempo
	 *             de ejecución.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos necesarios.
	 */
	public void ejecutarGRASP(String rutared, String rutademandas,
			String rutaflota, String rutaparadas, String rutatiempo,
			String rutalog, String rutalogdetallado, String rutasave,
			String rutasemi) throws IOException, Exception {
		boolean salir = false;
		logpath = rutalog;
		logdetalladopath = rutalogdetallado;
		while (!salir) {
			maximasparadas++;
			salir = true;
			try {
				if (maximasparadaspermitidas < maximasparadas)
					throw new Exception("Paradas máximas alcanzadas");
				obtenerMatrices(rutared, rutademandas, rutaflota, rutaparadas,
						rutatiempo, rutasemi, tmaxlinea);
				lineastotales = datosred.getRutasSemi().length;
				volver = true;
				for (int i = 0; i < lineastotales; i++) {
					if (datosred.getRutasSemi()[i][2] != datosred
							.getRutasSemi()[i][0]) {
						volver = false;
					}
				}
				distanciaoriginal = new int[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				for (int i = 0; i < datosred.getNumeroNodos(); i++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						distanciaoriginal[i][j] = datosred.getDistancia()[i][j];
					}
				}
				rutasCubiertas = new boolean[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];
				floyd = new DistanciasMinimas(datosred.getNumeroNodos(),
						datosred.getDistancia());
				floyd.floyd();
				rutasDirectas();
				distancia = datosred.getDistancia();
				logstring = "";
				logdetalladostring = "";
				rutasInicialesManuales();
				iniciar();
				calcularTiempoInicial();
				asignarPuntos();
				pasarRutasArrayList();
				evaluarParesParadas();
				siguenCubiertas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				eliminarParadas();
				añadirParadas();
				recalcularTiempos();
				calcularDemandaCubierta();
				tiemp = System.currentTimeMillis() - tiemp;
				escribeLog(true);
				escribeLogDetallado(true);
			}// try
			catch (ArrayIndexOutOfBoundsException e) {
				salir = false;
			}// catch
			catch (NullPointerException e) {
				salir = false;
			}// catch
		}// while
	}// ejecutarSemi

	/**
	 * Método que configura las rutas manuales.
	 * 
	 * @param rutared
	 *            localización del archivo de red.
	 * @param rutademandas
	 *            localizacion del archivo de demandas.
	 * @param rutaflota
	 *            Ruta con el archivo de flota.
	 * @param rutaparadas
	 *            Ruta con el archivo de paradas.
	 * @param rutatiempo
	 *            Ruta con el archivo de tiempo.
	 * @param rutalog
	 *            Ruta para guardar el fichero de logs.
	 * @param rutalogdetallado
	 *            Ruta para guardar el fichero de logsdetallado.
	 * @param rutasave
	 *            Ruta donde guardar los resultados.
	 * @param rutamanual
	 *            Ruta donde están los datos de las rutas manuales.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos necesarios.
	 */
	public void ejecutarGRASPManual(String rutared, String rutademandas,
			String rutaflota, String rutaparadas, String rutatiempo,
			String rutalog, String rutalogdetallado, String rutasave,
			String rutamanual) throws IOException {
		logpath = rutalog;
		logdetalladopath = rutalogdetallado;
		obtenerMatrices(rutared, rutademandas, rutaflota, rutaparadas,
				rutatiempo, tmaxlinea, rutamanual);
		lineastotales = datosred.getRutasManual().size();
		if (lineastotales % 2 == 1) {
			lineastotales++;
		}
		lineastotales /= 2;
		volver = true;
		for (int i = 0; i < lineastotales * 2; i += 2) {
			if (datosred.getRutasManual().get(i) != null
					&& datosred.getRutasManual().get(i + 1) != null) {
				if (datosred.getRutasManual().get(i).get(0) != datosred
						.getRutasManual().get(i)
						.get(datosred.getRutasManual().get(i).size() - 1)) {
					volver = false;
				}
			}
		}
		distanciaoriginal = new int[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				distanciaoriginal[i][j] = datosred.getDistancia()[i][j];
			}
		}
		rutasCubiertas = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		ST = new TipoRutaTotal[lineastotales + 1];
		for (int i = 1; i <= lineastotales; i++) {
			ST[i] = new TipoRutaTotal(maximasparadas, maximasparadas);
		}
		floyd = new DistanciasMinimas(datosred.getNumeroNodos(),
				datosred.getDistancia());
		floyd.floyd();
		rutasDirectas();
		distancia = datosred.getDistancia();
		logstring = "";
		logdetalladostring = "";
		paresC = (ArrayList<Integer>[][]) new ArrayList[datosred
				.getNumeroNodos()][datosred.getNumeroNodos()];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				paresC[i][j] = new ArrayList<Integer>();
			}
		}
		for (int i = 0; i < datosred.getRutasManual().size(); i++) {
			if (datosred.getRutasManual().get(i) != null) {
				for (int j = 0; j < datosred.getRutasManual().get(i).size(); j++) {
					if (datosred.getDemanda()[datosred.getRutasManual().get(i)
							.get(j)][datosred.getRutasManual().get(i)
							.get(datosred.getRutasManual().get(i).size() - 1)] > 0) {
						paresC[datosred.getRutasManual().get(i).get(j)][datosred
								.getRutasManual()
								.get(i)
								.get(datosred.getRutasManual().get(i).size() - 1)]
								.add(i + 1);
					}
				}
			}
		}
		rutasfinales = new ArrayList();
		demandacub = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		for (int i = 0; i < datosred.getRutasManual().size(); i++) {
			if (datosred.getRutasManual().get(i) == null) {
				ArrayList<Integer> r = new ArrayList<Integer>();
				r.add(0);
				r.add(0);
				ArrayList r2 = new ArrayList();
				r2.add(r);
				r2.add(0);
				rutasfinales.add(r2);
			} else {
				ArrayList<Integer> r = new ArrayList<Integer>();
				for (int j = 0; j < datosred.getRutasManual().get(i).size(); j++) {
					if (j > 0) {
						if (floyd.getRecorrido(
								datosred.getRutasManual().get(i).get(j - 1),
								datosred.getRutasManual().get(i).get(j)).size() > 2) {
							for (int k = 1; k < floyd
									.getRecorrido(
											datosred.getRutasManual().get(i)
													.get(j - 1),
											datosred.getRutasManual().get(i)
													.get(j)).size() - 1; k++) {
								r.add(floyd
										.getRecorrido(
												datosred.getRutasManual()
														.get(i).get(j - 1),
												datosred.getRutasManual()
														.get(i).get(j)).get(k));
							}
						}
					}
					r.add(datosred.getRutasManual().get(i).get(j));
				}
				int distancia = 0;
				for (int j = 1; j < r.size(); j++) {
					distancia += datosred.getDistancia()[r.get(j - 1)][r.get(j)];
				}
				ArrayList r2 = new ArrayList<Integer>();
				r2.add(r);
				r2.add(distancia);
				rutasfinales.add(r2);
			}
		}// for
		if (datosred.getRutasManual().size() % 2 != 0) {
			ArrayList<Integer> r = new ArrayList<Integer>();
			r.add(0);
			r.add(0);
			ArrayList r2 = new ArrayList();
			r2.add(r);
			r2.add(0);
			rutasfinales.add(r2);
		}
		parescubiertos = new int[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		demandaporlinea = new int[lineastotales * 2];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				parescubiertos[i][j] = 0;
			}
		}
		for (int i = 0; i < lineastotales * 2; i++) {
			demandaporlinea[i] = 0;
		}
		// Se ordenan las demandas por urgencia.
		ArrayList<ArrayList<Integer>> urgentesordenadas = new ArrayList<ArrayList<Integer>>();
		int sumamaxima;
		do {
			sumamaxima = 0;
			int kurg = -1;
			int jurg = -1;
			for (int k = 0; k < getDatosRed().getNumeroNodos(); k++) {
				for (int j = 0; j < getDatosRed().getNumeroNodos(); j++) {
					if (getDatosRed().getDemandaUrgente()[k][j] == true
							&& parescubiertos[j][k] < 1) {
						ArrayList<Integer> urge = new ArrayList<Integer>();
						urge.add(k);
						urge.add(j);
						if (!urgentesordenadas.contains(urge)) {
							if (sumamaxima < getDatosRed().getDemanda()[k][j]) {
								sumamaxima = getDatosRed().getDemanda()[k][j];
								kurg = k;
								jurg = j;
							}
						}// if
					}
				}// for
			}// for
			if (kurg != -1 && jurg != -1) {
				ArrayList<Integer> urge = new ArrayList<Integer>();
				urge.add(kurg);
				urge.add(jurg);
				urgentesordenadas.add(urge);
			}
		} while (sumamaxima > 0);
		boolean cambio = true;
		while (cambio) {
			cambio = false;
			int maxi;
			int maxj;
			if (urgentesordenadas.size() > 0) {
				maxi = urgentesordenadas.get(0).get(0);
				maxj = urgentesordenadas.get(0).get(1);
				urgentesordenadas.remove(0);
			} else {
				int maxdemand = 0;
				maxi = -1;
				maxj = -1;
				for (int i = 0; i < datosred.getNumeroNodos(); i++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						if (maxdemand < datosred.getDemanda()[i][j]
								&& parescubiertos[i][j] == 0
								&& paresC[i][j].size() > 0) {
							maxdemand = datosred.getDemanda()[i][j];
							maxi = i;
							maxj = j;
						}
					}
				}
			}// else
				// Se asigna la demanda, empezando por la mayor, a la ruta más
				// corta en cada momento.
			if (maxi != -1 && maxj != -1) {
				parescubiertos[maxi][maxj] = -1;
				if (paresC[maxi][maxj].size() == 1) {
					if (demandaporlinea[paresC[maxi][maxj].get(0) - 1]
							+ datosred.getDemanda()[maxi][maxj] <= datosred
								.getMaximaCapacidad()) {
						if ((paresC[maxi][maxj].get(0) - 1) % 2 == 0) {
							if (rutasfinales.get(paresC[maxi][maxj].get(0))
									.get(1)
									+ rutasfinales.get(
											paresC[maxi][maxj].get(0) - 1).get(
											1)
									+ datosred.getDemanda()[maxi][maxj]
									* tiempomedioparada * 2 < tmaxlinea) {
								parescubiertos[maxi][maxj] = paresC[maxi][maxj]
										.get(0);
								demandaporlinea[paresC[maxi][maxj].get(0) - 1] += datosred
										.getDemanda()[maxi][maxj];
								rutasfinales
										.get(paresC[maxi][maxj].get(0) - 1)
										.set(1,
												rutasfinales.get(
														paresC[maxi][maxj]
																.get(0) - 1)
														.get(1)
														+ datosred.getDemanda()[maxi][maxj]
														* tiempomedioparada * 2);
							}
						} else {
							if (rutasfinales.get(paresC[maxi][maxj].get(0) - 2)
									.get(1)
									+ rutasfinales.get(
											paresC[maxi][maxj].get(0) - 1).get(
											1)
									+ datosred.getDemanda()[maxi][maxj]
									* tiempomedioparada < tmaxlinea) {
								parescubiertos[maxi][maxj] = paresC[maxi][maxj]
										.get(0);
								demandaporlinea[paresC[maxi][maxj].get(0) - 1] += datosred
										.getDemanda()[maxi][maxj];
								rutasfinales
										.get(paresC[maxi][maxj].get(0) - 1)
										.set(1,
												rutasfinales.get(
														paresC[maxi][maxj]
																.get(0) - 1)
														.get(1)
														+ datosred.getDemanda()[maxi][maxj]
														* tiempomedioparada);
							}
						}// else
					}
				} else {
					int menostiempo = inf;
					int ruta = -1;
					for (int i = 0; i < paresC[maxi][maxj].size(); i++) {
						if (demandaporlinea[paresC[maxi][maxj].get(i) - 1]
								+ datosred.getDemanda()[maxi][maxj] <= datosred
									.getMaximaCapacidad()) {
							if ((paresC[maxi][maxj].get(i) - 1) % 2 == 0) {
								if (rutasfinales.get(paresC[maxi][maxj].get(i))
										.get(1)
										+ rutasfinales.get(
												paresC[maxi][maxj].get(i) - 1)
												.get(1)
										+ datosred.getDemanda()[maxi][maxj]
										* tiempomedioparada * 2 < tmaxlinea) {
									if (rutasfinales.get(
											paresC[maxi][maxj].get(i)).get(1)
											+ rutasfinales
													.get(paresC[maxi][maxj]
															.get(i) - 1).get(1)
											+ datosred.getDemanda()[maxi][maxj]
											* tiempomedioparada * 2 < menostiempo) {
										menostiempo = rutasfinales.get(
												paresC[maxi][maxj].get(i)).get(
												1)
												+ rutasfinales.get(
														paresC[maxi][maxj]
																.get(i) - 1)
														.get(1)
												+ datosred.getDemanda()[maxi][maxj]
												* tiempomedioparada * 2;
										ruta = paresC[maxi][maxj].get(i) - 1;
									}
								}
							} else {
								if (rutasfinales.get(
										paresC[maxi][maxj].get(i) - 2).get(1)
										+ rutasfinales.get(
												paresC[maxi][maxj].get(i) - 1)
												.get(1)
										+ datosred.getDemanda()[maxi][maxj]
										* tiempomedioparada < tmaxlinea) {
									if (rutasfinales.get(
											paresC[maxi][maxj].get(i) - 2).get(
											1)
											+ rutasfinales
													.get(paresC[maxi][maxj]
															.get(i) - 1).get(1)
											+ datosred.getDemanda()[maxi][maxj]
											* tiempomedioparada < menostiempo) {
										menostiempo = rutasfinales.get(
												paresC[maxi][maxj].get(i) - 2)
												.get(1)
												+ rutasfinales.get(
														paresC[maxi][maxj]
																.get(i) - 1)
														.get(1)
												+ datosred.getDemanda()[maxi][maxj]
												* tiempomedioparada;
										ruta = paresC[maxi][maxj].get(i) - 1;
									}
								}
							}// else
						}
					}// for
					if (ruta != -1 && ruta % 2 == 0) {
						parescubiertos[maxi][maxj] = ruta + 1;
						demandaporlinea[ruta] += datosred.getDemanda()[maxi][maxj];
						rutasfinales.get(ruta).set(
								1,
								rutasfinales.get(ruta).get(1)
										+ datosred.getDemanda()[maxi][maxj]
										* tiempomedioparada * 2);
					} else if (ruta != -1 && ruta % 2 == 1) {
						parescubiertos[maxi][maxj] = ruta + 1;
						demandaporlinea[ruta] += datosred.getDemanda()[maxi][maxj];
						rutasfinales.get(ruta).set(
								1,
								rutasfinales.get(ruta).get(1)
										+ datosred.getDemanda()[maxi][maxj]
										* tiempomedioparada);
					}
				}
				cambio = true;
			}// if
		}// while
		recalcularTiempos();
		calcularDemandaCubierta();
	}// ejecutarGRASPManual

	/**
	 * Calcula unas posibles rutas, las mejores, en función de las paradas
	 * centro y los extremos.
	 * 
	 * @throws java.lang.Exception
	 *             Lanza la excepción en caso de no poder insertar la parada.
	 */
	private void rutasIniciales() throws Exception {
		int suma, sumamaxima, contarrutas = 0;
		int[] pcentrosalida = new int[lineastotales];
		int[] pcentrollegada = new int[lineastotales];
		ArrayList<Integer> paradasañadidas = new ArrayList<Integer>();

		// Se ordenan las paradas por demanda urgente
		ArrayList<Integer> urgentesordenadas = new ArrayList<Integer>();
		do {
			sumamaxima = 0;
			int parada = -1;
			for (int k = 0; k < datosred.getNumeroNodos(); k++) {
				suma = 0;
				for (int j = 0; j < datosred.getNumeroNodos(); j++) {
					if (datosred.getDemandaUrgente()[j][k] == true
							&& !urgentesordenadas.contains(k)) {
						suma += demanda[j][k];
					}// if
				}// for
				if (sumamaxima < suma) {
					sumamaxima = suma;
					parada = k;
				}
			}// for
			if (parada != -1)
				urgentesordenadas.add(parada);
		} while (sumamaxima > 0);

		// Es una matriz cuadrada de tamaño igual al número de paradas de la
		// red.
		// Indica que pares de paradas están cubiertas por las líneas poniendo
		// la parada inicio en el primer índice y la parada final en el segundo
		// índice
		parescubiertos = new int[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];

		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				parescubiertos[i][j] = 0;
			}// for
		}// for

		while (contarrutas != lineastotales) {
			contarrutas = 0;
			int extremos[] = new int[lineastotales];
			for (int i = 0; i < extremos.length; i++) {
				pcentrollegada[i] = pcentrosalida[i] = extremos[i] = -1;
			}
			// Se calculan tantos extremos de la ciudad como el máximo de rutas
			// siempre que haya demanda y se tarde más de un tiempo mínimo y no
			// se pase del tiempo máximo

			for (int i = 0; i < extremos.length; i++) {
				sumamaxima = 0;
				for (int k = 0; k < datosred.getNumeroNodos(); k++) {
					if (!paradasañadidas.contains(k)) {
						suma = 0;
						for (int j = 0; j < datosred.getNumeroNodos(); j++) {
							if (parescubiertos[j][k] == 0) {
								suma += demanda[j][k];
							}// if
						}// for
						if (sumamaxima < suma) {
							sumamaxima = suma;
							extremos[i] = k;
						}
					}
				}// for
				if (extremos[i] == -1) {
					for (int k = 0; k < datosred.getNumeroNodos(); k++) {
						suma = 0;
						for (int j = 0; j < datosred.getNumeroNodos(); j++) {
							if (parescubiertos[j][k] == 0) {
								suma += demanda[j][k];
							}// if
						}// for
						if (sumamaxima < suma) {
							sumamaxima = suma;
							extremos[i] = k;
						}
					}// for
				}// if
				if (urgentesordenadas.size() > 0) {
					if (urgentesordenadas.contains(extremos[i])) {
						urgentesordenadas.remove(urgentesordenadas
								.indexOf(extremos[i]));
					} else {
						extremos[i] = urgentesordenadas.get(0);
						urgentesordenadas.remove(0);
					}
				}// if
					// Se calcula la parada centro llegada (pcentrollegada)
				sumamaxima = 0;
				for (int k = 0; k < datosred.getNumeroNodos(); k++) {
					if (k != extremos[i] && !paradasañadidas.contains(k)) {
						suma = 0;
						for (int j = 0; j < datosred.getNumeroNodos(); j++) {
							if (parescubiertos[j][k] == 0) {
								suma += demanda[j][k];
							}// if
						}// for
						if (sumamaxima < suma) {
							sumamaxima = suma;
							pcentrollegada[i] = k;
						}
					}// if
				}// for
				if (pcentrollegada[i] == -1) {
					for (int k = 0; k < datosred.getNumeroNodos(); k++) {
						if (k != extremos[i]) {
							suma = 0;
							for (int j = 0; j < datosred.getNumeroNodos(); j++) {
								if (parescubiertos[j][k] == 0) {
									suma += demanda[j][k];
								}// if
							}// for
							if (sumamaxima < suma) {
								sumamaxima = suma;
								pcentrollegada[i] = k;
							}
						}
					}// for
				}// if
				if (urgentesordenadas.size() > 0) {
					if (urgentesordenadas.contains(pcentrollegada[i])) {
						urgentesordenadas.remove(urgentesordenadas
								.indexOf(pcentrollegada[i]));
					} else if (urgentesordenadas.get(0) != extremos[i]) {
						pcentrollegada[i] = urgentesordenadas.get(0);
						urgentesordenadas.remove(0);
					} else if (urgentesordenadas.size() > 1
							&& urgentesordenadas.get(1) != extremos[i]) {
						pcentrollegada[i] = urgentesordenadas.get(1);
						urgentesordenadas.remove(1);
					}
				}// if
				if (pcentrollegada[i] == -1) {
					int maximadistancia = 0;
					int maxparada = -1;
					for (int k = 0; k < getDatosRed().getNumeroNodos(); k++) {
						if (maximadistancia < distancia[extremos[i]][k]
								&& k != extremos[i]
								&& parescubiertos[extremos[i]][k] == 0
								&& !paradasañadidas.contains(k)
								&& demanda[extremos[i]][k] != 0
								&& distancia[extremos[i]][k] <= tmaxlinea
								&& demanda[extremos[i]][k] <= datosred
										.getMaximaCapacidad()) {
							maximadistancia = distancia[extremos[i]][k];
							maxparada = k;
						}// if
					}// for
					pcentrollegada[i] = maxparada;
					if (pcentrollegada[i] == -1) {
						for (int k = 0; k < getDatosRed().getNumeroNodos(); k++) {
							if (maximadistancia < distancia[extremos[i]][k]
									&& k != extremos[i]
									&& parescubiertos[extremos[i]][k] == 0
									&& demanda[extremos[i]][k] != 0
									&& distancia[extremos[i]][k] <= tmaxlinea
									&& demanda[extremos[i]][k] <= datosred
											.getMaximaCapacidad()) {
								maximadistancia = distancia[extremos[i]][k];
								maxparada = k;
							}// if
						}// for
						pcentrollegada[i] = maxparada;
					}
				}
				// Se calcula la parada centro salida (pcentrosalida)
				if (volver)
					pcentrosalida[i] = pcentrollegada[i];
				else {
					int maximadistancia = 0;
					int maxparada = -1;
					for (int k = 0; k < datosred.getNumeroNodos(); k++) {
						if (maximadistancia < distancia[k][extremos[i]]
								&& parescubiertos[k][extremos[i]] == 0
								&& k != extremos[i]
								&& demanda[k][extremos[i]] != 0
								&& distancia[k][extremos[i]] >= tminlinea
								&& distancia[k][extremos[i]] <= tmaxlinea
								&& demanda[k][extremos[i]] <= datosred
										.getMaximaCapacidad()) {
							maximadistancia = distancia[extremos[i]][k];
							maxparada = k;
						}// if
					}// for
					pcentrosalida[i] = maxparada;
					if (pcentrosalida[i] == -1) {
						for (int k = 0; k < datosred.getNumeroNodos(); k++) {
							if (maximadistancia < distancia[k][extremos[i]]
									&& parescubiertos[k][extremos[i]] == 0
									&& k != extremos[i]
									&& distancia[k][extremos[i]] >= tminlinea
									&& distancia[k][extremos[i]] <= tmaxlinea
									&& demanda[k][extremos[i]] <= datosred
											.getMaximaCapacidad()) {
								maximadistancia = distancia[extremos[i]][k];
								maxparada = k;
							}// if
						}// for
						pcentrosalida[i] = maxparada;
					}// if
				}// else
				if (pcentrosalida[i] != -1 && extremos[i] != -1) {
					parescubiertos[pcentrosalida[i]][extremos[i]] = i * 2 + 1;
				}
				if (pcentrollegada[i] != -1 && extremos[i] != -1) {
					parescubiertos[extremos[i]][pcentrollegada[i]] = i * 2 + 2;
				}
				paradasañadidas.add(extremos[i]);
				paradasañadidas.add(pcentrollegada[i]);
			}// for

			ST = new TipoRutaTotal[lineastotales + 1];

			int contarextremos = 0;

			int k = 0;

			while (k != lineastotales) {
				if (extremos[k] != -1) {
					contarextremos++;
				}
				k++;
			}
			// Se crean las rutas utilizando las paradas pcentrosalida, extremos
			// y pcentrollegada
			for (int i = 1; i <= lineastotales; i++) {
				ST[i] = new TipoRutaTotal(maximasparadas, maximasparadas);
				ST[i].getRutaInicial().setNumeroParadas(2);
				ST[i].getRutaFinal().setNumeroParadas(2);
			}
			for (int i = 1; i <= contarextremos; i++) {
				ST[i].getRutaInicial().getParadasRutaParcial()
						.setParadasRutaParcial(1, pcentrosalida[i - 1]);
				ST[i].getRutaInicial().getParadasRutaParcial()
						.setParadasRutaParcial(2, extremos[i - 1]);
				ST[i].getRutaFinal().getParadasRutaParcial()
						.setParadasRutaParcial(1, extremos[i - 1]);
				ST[i].getRutaFinal().getParadasRutaParcial()
						.setParadasRutaParcial(2, pcentrollegada[i - 1]);
			}// for

			contarrutas = contarextremos;

			if (contarrutas != lineastotales) {
				// Se calculan tantas rutas entre extremos como se necesite
				// según la
				// máxima demanda entre estos
				int distanciaextremos[][] = new int[datosred.getNumeroNodos()][datosred
						.getNumeroNodos()];

				for (int i = 0; i < contarextremos; i++) {
					for (int j = 0; j < contarextremos; j++)
						distanciaextremos[extremos[i]][extremos[j]] = distancia[extremos[i]][extremos[j]];
				}

				boolean salir = false;
				for (int i = contarextremos; (i < extremos.length && !salir); i++) {
					int maximademanda = 0;
					int inicio = -1;
					int fin = -1;
					for (int j = 0; j < contarextremos; j++) {
						for (int q = 0; q < contarextremos; q++) {
							if (maximademanda < demanda[extremos[j]][extremos[q]]
									&& distanciaextremos[extremos[j]][extremos[q]] > tminlinea
									&& distanciaextremos[extremos[j]][extremos[q]] < tmaxlinea
									&& demanda[i][j] <= datosred
											.getMaximaCapacidad()) {
								maximademanda = demanda[extremos[j]][extremos[q]];
								inicio = j;
								fin = q;
							}// if
						}// for
					}// for
					if (inicio != -1 && fin != -1) {
						ST[i + 1] = new TipoRutaTotal(maximasparadas,
								maximasparadas);
						ST[i + 1].getRutaInicial().setNumeroParadas(2);
						ST[i + 1].getRutaFinal().setNumeroParadas(2);
						ST[i + 1].getRutaInicial().getParadasRutaParcial()
								.setParadasRutaParcial(1, extremos[inicio]);
						ST[i + 1].getRutaInicial().getParadasRutaParcial()
								.setParadasRutaParcial(2, extremos[fin]);
						ST[i + 1].getRutaFinal().getParadasRutaParcial()
								.setParadasRutaParcial(1, extremos[fin]);
						ST[i + 1].getRutaFinal().getParadasRutaParcial()
								.setParadasRutaParcial(2, extremos[inicio]);
						distanciaextremos[extremos[inicio]][extremos[fin]] = 0;
						distanciaextremos[extremos[fin]][extremos[inicio]] = 0;
						contarrutas++;
					}// if
					else {
						tminlinea = tminlinea - 1;
						salir = true;
					}// else
				}// for
			}// if
		}// while
	}// rutasInciales

	/**
	 * Método que se usa para insertar puntos o paradas a las diferentes rutas
	 * según el incremento de la distancia y la satisfacción de demandas.
	 * Finaliza cuando no hay tiempo para meter más paradas en las rutas.
	 * 
	 * @throws java.io.IOException
	 *             Lanzada en caso de que no se pueda escribir en ese fichero.
	 * @throws java.lang.Exception
	 *             Error en caso de que no se puedan insertar paradas en una
	 *             ruta.
	 */
	private void asignarPuntos() throws IOException, Exception {
		int i = 0, j = 0, k = 0, k0 = 0, sen = 0, incDem = 0, incDem0 = 0, incDis = 0, incDis0 = 0, nIter = 0, r = 0;
		// atributos con los datos mejores
		int im = 0, jm = 0, km = 0, senm = 0, incDemm = 0, incDism = 0;
		// indican los cocientes y el cociente mejor
		double coc = 0, coc0 = 0, cocm = -inf + 1;

		while (cocm != -inf) {
			nIter++;
			logdetalladostring += ("\r\nNúmero de iteración :" + nIter);
			logdetalladostring += ("\r\n nodo linea sen  pos0   dem0  dis0    coc0 \r\n");
			cocm = -inf;
			for (i = 0; i < datosred.getNumeroNodos(); i++) {
				for (j = 1; j <= lineastotales; j++) {
					// Si la parada i no esta en la ruta(inicial) ST[j] y
					// todavía podemos meter más paradas
					if (!paradasenrutainicial[i][j]
							&& ST[j].getRutaInicial().getNumeroParadas() < maximasparadas) {
						sen = 1;
						coc0 = -inf;
						// Para desde la parada 2 hasta el número de paradas
						// que tenemos en la ruta calculamos la mejor
						// posición para meter una nueva parada
						for (k = 2; k <= ST[j].getRutaInicial()
								.getNumeroParadas(); k++) {
							// Calculamos la distancia de la anterior parada
							// (k-1) a la que vamos a insertar (i) + la
							// distancia de la nueva parada (i) a la siguiente
							// parada (k) y le restamos la distancia de k-1 a k
							incDis = distancia[ST[j].getRutaInicial()
									.getParadasRutaParcial()
									.getParadaConcretaRutaParcial(k - 1)][i]
									+ distancia[i][ST[j].getRutaInicial()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k)]
									- distancia[ST[j]
											.getRutaInicial()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k - 1)][ST[j]
											.getRutaInicial()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k)]
									+ demanda[i][ST[j]
											.getRutaInicial()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(
													ST[j].getRutaInicial()
															.getNumeroParadas())]
									* tiempomedioparada;
							incDem = 0;
							// Si la demanda de i a destino no esta cubierta
							// aumento incDem con la demanda que hay entre este
							// par de paradas
							if (parescubiertos[i][ST[j]
									.getRutaInicial()
									.getParadasRutaParcial()
									.getParadaConcretaRutaParcial(
											ST[j].getRutaInicial()
													.getNumeroParadas())] == 0) {
								incDem = incDem
										+ demanda[i][ST[j]
												.getRutaInicial()
												.getParadasRutaParcial()
												.getParadaConcretaRutaParcial(
														ST[j].getRutaInicial()
																.getNumeroParadas())];
							}
							if (incDis == 0) {
								coc = inf * incDem;
							}// if
							else {
								coc = (double) incDem / (double) (incDis);
								if (datosred.getDemandaUrgente()[i][ST[j]
										.getRutaInicial()
										.getParadasRutaParcial()
										.getParadaConcretaRutaParcial(
												ST[j].getRutaInicial()
														.getNumeroParadas())] == true) {
									// Si la demanda es urgente el cociente se
									// multiplica para que sea más prioritario
									coc *= datosred.getMaximaCapacidad();
								}
							}// else
							if (coc > coc0
									&& ST[j].getRutaInicial().getTiempo()
											+ incDis < tmaxlinea
									&& demandaporlinea[2 * j - 2] + incDem < datosred
											.getMaximaCapacidad()
									&& !estaEnRuta(i, j, sen, k)) {
								coc0 = coc;
								k0 = k;
								incDem0 = incDem;
								incDis0 = incDis;
							}// if
						}// for

						logdetalladostring += (i + "     " + j + "     " + sen
								+ "     " + k0 + "     " + incDem0 + "     "
								+ incDis0 + "     " + coc0 + "\r\n");

						if (coc0 > cocm) {
							im = i;
							jm = j;
							senm = sen;
							km = k0;
							incDemm = incDem0;
							incDism = incDis0;
							cocm = coc0;
						}// if
					}// if
						// Si la parada i no esta en la ruta(final) ST[j] y
						// todavía podemos meter más paradas
					if (!paradasenrutafinal[i][j]
							&& ST[j].getRutaFinal().getNumeroParadas() < maximasparadas) {
						sen = 2;
						coc0 = -inf;
						// Para desde la parada 2 hasta el número de paradas
						// que tenemos en la ruta calculamos la mejor
						// posición para meter una nueva parada
						for (k = 2; k <= ST[j].getRutaFinal()
								.getNumeroParadas(); k++) {
							// Calculamos la distancia de la anterior parada
							// (k-1) a la que vamos a insertar (i) + la
							// distancia de la nueva parada (i) a la siguiente
							// parada (k) y le restamos la distancia de k-1 a k
							incDis = distancia[ST[j].getRutaFinal()
									.getParadasRutaParcial()
									.getParadaConcretaRutaParcial(k - 1)][i]
									+ distancia[i][ST[j].getRutaFinal()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k)]
									- distancia[ST[j]
											.getRutaFinal()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k - 1)][ST[j]
											.getRutaFinal()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(k)]
									+ demanda[i][ST[j]
											.getRutaFinal()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(
													ST[j].getRutaFinal()
															.getNumeroParadas())]
									* tiempomedioparada;
							incDem = 0;
							// Si la demanda de i a destino no esta cubierta
							// dentro de la ruta j aumento incDem con la
							// demanda que hay entre este par de paradas
							if (parescubiertos[i][ST[j]
									.getRutaFinal()
									.getParadasRutaParcial()
									.getParadaConcretaRutaParcial(
											ST[j].getRutaFinal()
													.getNumeroParadas())] == 0) {
								incDem = incDem
										+ demanda[i][ST[j]
												.getRutaFinal()
												.getParadasRutaParcial()
												.getParadaConcretaRutaParcial(
														ST[j].getRutaFinal()
																.getNumeroParadas())];
							}// if
							if (incDis == 0) {
								coc = inf * incDem;
							}// if
							else {
								coc = (double) incDem / (double) (incDis);
								if (datosred.getDemandaUrgente()[i][ST[j]
										.getRutaFinal()
										.getParadasRutaParcial()
										.getParadaConcretaRutaParcial(
												ST[j].getRutaFinal()
														.getNumeroParadas())] == true) {
									// Si la demanda es urgente el cociente se
									// multiplica para que sea más prioritario
									coc *= datosred.getMaximaCapacidad();
								}
							}// else
							if (coc > coc0
									&& ST[j].getRutaInicial().getTiempo()
											+ ST[j].getRutaFinal().getTiempo()
											+ incDis < tmaxlinea
									&& demandaporlinea[2 * j - 1] + incDem < datosred
											.getMaximaCapacidad()
									&& !estaEnRuta(i, j, sen, k)) {
								coc0 = coc;
								k0 = k;
								incDem0 = incDem;
								incDis0 = incDis;
							}// if
						}// for
						logdetalladostring += (i + "     " + j + "     " + sen
								+ "     " + k0 + "     " + incDem0 + "     "
								+ incDis0 + "     " + coc0 + "\r\n");

						if (coc0 > cocm) {
							im = i;
							jm = j;
							senm = sen;
							km = k0;
							incDemm = incDem0;
							incDism = incDis0;
							cocm = coc0;
						}// if
					}// if
				}// for
			}// for
			logdetalladostring += ("\r\n" + im + "     " + jm + "     " + senm
					+ "     " + km + "     " + incDemm + "     " + incDism
					+ "     " + cocm + "     *\r\n");
			logdetalladostring += ("--------------------------\r\n");
			if (senm == 1) {
				// Desde el número de paradas totales +1 que tiene la mejor
				// linea hasta la posición mejor, movemos las paradas a una
				// posición posterior
				for (r = ST[jm].getRutaInicial().getNumeroParadas() + 1; r >= km + 1; r--) {
					ST[jm].getRutaInicial()
							.getParadasRutaParcial()
							.setParadasRutaParcial(
									r,
									ST[jm].getRutaInicial()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(r - 1));
				}// for
				ST[jm].getRutaInicial().getParadasRutaParcial()
						.setParadasRutaParcial(km, im);
				ST[jm].getRutaInicial().setNumeroParadas(
						ST[jm].getRutaInicial().getNumeroParadas() + 1);
				ST[jm].getRutaInicial().aumentarTiempo(incDism);

				postProcesamiento(jm, true);
				paradasenrutainicial[im][jm] = true;
				if (parescubiertos[im][ST[jm]
						.getRutaInicial()
						.getParadasRutaParcial()
						.getParadaConcretaRutaParcial(
								ST[jm].getRutaInicial().getNumeroParadas())] == 0) {
					parescubiertos[im][ST[jm]
							.getRutaInicial()
							.getParadasRutaParcial()
							.getParadaConcretaRutaParcial(
									ST[jm].getRutaInicial().getNumeroParadas())] = (jm - 1) * 2 + 1;
				}
			}// if
			else {
				// Desde el número de paradas totales +1 que tiene la mejor
				// linea hasta la posición mejor, movemos las paradas a una
				// posición posterior
				for (r = ST[jm].getRutaFinal().getNumeroParadas() + 1; r >= km + 1; r--) {
					ST[jm].getRutaFinal()
							.getParadasRutaParcial()
							.setParadasRutaParcial(
									r,
									ST[jm].getRutaFinal()
											.getParadasRutaParcial()
											.getParadaConcretaRutaParcial(r - 1));
				}// for
				ST[jm].getRutaFinal().getParadasRutaParcial()
						.setParadasRutaParcial(km, im);
				ST[jm].getRutaFinal().setNumeroParadas(
						ST[jm].getRutaFinal().getNumeroParadas() + 1);
				ST[jm].getRutaFinal().aumentarTiempo(incDism);

				postProcesamiento(jm, false);
				paradasenrutafinal[im][jm] = true;
				if (parescubiertos[im][ST[jm]
						.getRutaFinal()
						.getParadasRutaParcial()
						.getParadaConcretaRutaParcial(
								ST[jm].getRutaFinal().getNumeroParadas())] == 0) {
					parescubiertos[im][ST[jm]
							.getRutaFinal()
							.getParadasRutaParcial()
							.getParadaConcretaRutaParcial(
									ST[jm].getRutaFinal().getNumeroParadas())] = (jm - 1) * 2 + 2;
				}
			}// else
			demandacubierta = 0;
			for (i = 0; i < datosred.getNumeroNodos(); i++) {
				for (j = 0; j < datosred.getNumeroNodos(); j++) {
					if (parescubiertos[i][j] != 0) {
						demandacubierta = demandacubierta + demanda[i][j];
					}// if
				}// for
			}// for
		} // while
		for (int t = km; t < ST[jm].getRuta(senm).getNumeroParadas() - 1; t++) {
			ST[jm].getRuta(senm)
					.getParadasRutaParcial()
					.setParadasRutaParcial(
							t,
							ST[jm].getRuta(senm).getParadasRutaParcial()
									.getParadaConcretaRutaParcial(t + 2));
		}
		ST[jm].getRuta(senm)
				.getParadasRutaParcial()
				.setParadasRutaParcial(ST[jm].getRuta(senm).getNumeroParadas(),
						0);
		ST[jm].getRuta(senm).setNumeroParadas(
				ST[jm].getRuta(senm).getNumeroParadas() - 2);
		ST[jm].getRuta(senm).reducirTiempo(incDism * 2);

		demandacub = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		int rutaextendida[];
		for (j = 1; j <= lineastotales; j++) {
			logstring += ("\r\nRuta " + ((j - 1) * 2 + 1) + ": ");
			logdetalladostring += ("\r\nRuta " + ((j - 1) * 2 + 1) + ": ");
			rutaextendida = pasarArrayListToArray(expandirRuta(j, 1));
			for (i = 0; i < rutaextendida.length; i++) {
				logstring += (rutaextendida[i] + "     ");
				logdetalladostring += (rutaextendida[i] + "     ");
				for (int q = i + 1; q < rutaextendida.length; q++) {
					if (!demandacub[rutaextendida[i]][rutaextendida[q]]) {
						demandacub[rutaextendida[i]][rutaextendida[q]] = true;
					}
				}
			}// for
			logstring += ("\r\nRuta " + ((j - 1) * 2 + 2) + ": ");
			logdetalladostring += ("\r\nRuta " + ((j - 1) * 2 + 2) + ": ");
			rutaextendida = pasarArrayListToArray(expandirRuta(j, 2));

			for (i = 0; i < rutaextendida.length; i++) {
				logstring += (rutaextendida[i] + "     ");
				logdetalladostring += (rutaextendida[i] + "     ");
				for (int q = i + 1; q < rutaextendida.length; q++) {
					if (!demandacub[rutaextendida[i]][rutaextendida[q]]) {
						demandacub[rutaextendida[i]][rutaextendida[q]] = true;
					}
				}
			}// for
		}// for
	}// asignarPuntos
}
