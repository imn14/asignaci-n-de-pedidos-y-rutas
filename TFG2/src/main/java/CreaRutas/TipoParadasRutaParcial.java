package CreaRutas;

/**
 * Clase que contiene la estructura de datos de las paradas de las rutas
 * parciales.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class TipoParadasRutaParcial {

	/**
	 * Array de 1 a máximo de paradas por ruta, la posición 0 la desperdiciamos.
	 */
	private int paradasrutaparcial[];

	/**
	 * Constructor de la clase.
	 * 
	 * @param maximasparadas
	 *            Máximas paradas permitidas por cada ruta parcial (Ida o
	 *            vuelta)
	 */
	public TipoParadasRutaParcial(int maximasparadas) {
		paradasrutaparcial = new int[maximasparadas + 1];
	}

	/**
	 * Inserta una parada concreta en una posición determinada.
	 * 
	 * @param posicion
	 *            Posición donde va a ser insertada la parada
	 * @param parada
	 *            Parada que vamos a insertar en la ruta parcial
	 * @throws java.lang.Exception
	 *             Lanza la Excepción en caso de no poder insertar la parada.
	 */
	public void setParadasRutaParcial(int posicion, int parada)
			throws Exception {
		paradasrutaparcial[posicion] = parada;
	}

	/**
	 * Devuelve la parada concreta que se encuentra en la posición determinada.
	 * 
	 * @param posicion
	 *            Posición de la parada que queremos obtener.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 * @return Devuelve una parada concreta.
	 */
	public int getParadaConcretaRutaParcial(int posicion) throws Exception {
		return paradasrutaparcial[posicion];
	}

	/**
	 * Obtiene número máximo de paradas permitido para la ruta parcial.
	 * 
	 * @return Número de paradas de la ruta
	 */
	public int getParadasMaximas() {
		return paradasrutaparcial.length;
	}
}
