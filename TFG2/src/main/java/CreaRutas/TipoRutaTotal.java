package CreaRutas;

/**
 * Clase que contiene la estructura de datos de las rutas totales.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class TipoRutaTotal {

	/**
	 * Ruta de ida.
	 */
	private TipoRutaParcial rutainicial;

	/**
	 * Ruta de vuelta.
	 */
	private TipoRutaParcial rutafinal;

	/**
	 * Constructor.
	 * 
	 * @param maximasparadasida
	 *            Entero con las máximas paradas permitidas para la ruta de ida.
	 * @param maximasparadasvuelta
	 *            Entero con las máximas paradas permitidas para la ruta de
	 *            vuelta.
	 */
	public TipoRutaTotal(int maximasparadasida, int maximasparadasvuelta) {
		rutainicial = new TipoRutaParcial(maximasparadasida);
		rutafinal = new TipoRutaParcial(maximasparadasvuelta);
	}

	/**
	 * Establece una ruta parcial como ruta de ida.
	 * 
	 * @param ruta
	 *            Ruta de ida.
	 * @see #getRutaInicial
	 */
	public void setRutaInicial(TipoRutaParcial ruta) {
		rutainicial = ruta;
	}

	/**
	 * Establece una ruta parcial como ruta de vuelta.
	 * 
	 * @param ruta
	 *            Ruta de vuelta.
	 * @see #getRutaFinal
	 */
	public void setRutaFinal(TipoRutaParcial ruta) {
		rutafinal = ruta;
	}

	/**
	 * Consulta la ruta de ida.
	 * 
	 * @return Devuelve un objeto de tipo ruta parcial con la ruta de ida.
	 * @see #setRutaInicial
	 */
	public TipoRutaParcial getRutaInicial() {
		return rutainicial;
	}

	/**
	 * Consulta la ruta de vuelta.
	 * 
	 * @return Devuelve un objeto de tipo ruta parcial con la ruta de vuelta.
	 * @see #setRutaFinal
	 */
	public TipoRutaParcial getRutaFinal() {
		return rutafinal;
	}

	/**
	 * Consulta una ruta parcial en funcion del sentido.
	 * 
	 * @param sentido
	 *            Sentido de la ruta a obtener. 1 ida, 2 vuelta.
	 * @return Devuelve un objeto de tipo ruta parcial.
	 */
	public TipoRutaParcial getRuta(int sentido) {
		if (sentido == 1) {
			return getRutaInicial();
		} else {
			return getRutaFinal();
		}
	}
}
