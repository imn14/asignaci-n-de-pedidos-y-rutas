package CreaRutas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Clase que contiene los datos y métodos para crear las rutas.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class Grasp {

	/**
	 * Valor establecido para infinito.
	 */
	protected int inf = 9999;

	/**
	 * Booleano que indica si la ruta ha de volver o no.
	 */
	protected boolean volver;

	/**
	 * Número máximo de paradas por ruta. Según se van necesitando se van
	 * pidiendo más.
	 */
	protected int maximasparadas = 3;

	/**
	 * Maximas paradas permitidas por ruta.
	 */
	protected int maximasparadaspermitidas;

	/**
	 * Matriz con los datos sobre las demandas.
	 */
	protected int demanda[][];

	/**
	 * Matriz con los datos sobre las distancias.
	 */
	protected int distancia[][];

	/**
	 * Matriz con los datos sobre las distancias originales.
	 */
	protected int distanciaoriginal[][];

	/**
	 * Objeto que almacena los datos iniciales de la red.
	 */
	protected LeerDatos datosred;

	/**
	 * Array que contiene todas las rutas (con sus caminos de ida y vuelta).
	 */
	protected TipoRutaTotal ST[];

	/**
	 * Número de rutas totales (ida y vuelta).
	 */
	protected int lineastotales;

	/**
	 * Demanda total cubierta con las paradas que están asignadas en cada
	 * momento en todas las lineas.
	 */
	protected int demandacubierta;

	/**
	 * Demanda cubierta con las paradas que están asignadas a una linea.
	 */
	protected int[] demandaporlinea;

	/**
	 * Tiempo máximo de recorrido de una linea.
	 */
	protected int tmaxlinea;

	/**
	 * Tiempo mínimo de recorrido de una linea.
	 */
	protected int tminlinea;

	/**
	 * Variable de control que indica que paradas estan en la ruta inicial.
	 */
	protected boolean paradasenrutainicial[][];

	/**
	 * Variable de control que indica que paradas estan en la ruta final.
	 */
	protected boolean paradasenrutafinal[][];

	/**
	 * Variable que indica que si un par de nodos están conectados.
	 */
	protected boolean rutasCubiertas[][];

	/**
	 * Variable de control que indica que pares de paradas estan cubiertos y la
	 * ruta que los cubre.
	 */
	protected int parescubiertos[][];

	/**
	 * Variable de control que indica las rutas que cubren un par de paradas
	 * usado cuando se utilizan rutas manuales.
	 */
	protected ArrayList<Integer>[][] paresC;

	/**
	 * Numero de iteraciones del algoritmo de busqueda local.
	 */
	protected int numiteracionesbl;

	/**
	 * Demanda que permitimos dejar sin cubrir, es un double que se multiplica
	 * por la demanda total.
	 */
	protected double demandapermitida;

	/**
	 * Tiempo medio que tarda en cargar o descargar en cada parada una unidad
	 * transportada.
	 */
	protected int tiempomedioparada;

	/**
	 * ArrayList con las rutas resultado y sus tiempos.
	 */
	protected ArrayList<ArrayList<Integer>> rutasfinales;

	/**
	 * Objeto con el algoritmo de floyd para poder calcular los caminos mínimos.
	 */
	protected DistanciasMinimas floyd;

	/**
	 * String con el contenido del fichero log que muestra como se crearon las
	 * rutas.
	 */
	protected String logstring = "";

	/**
	 * String con la ruta del fichero log que muestra como se crearon las rutas.
	 */
	protected String logpath;

	/**
	 * String con el contenido del fichero log detallado que muestra como se
	 * crearon las rutas de forma detallada.
	 */
	protected String logdetalladostring = "";

	/**
	 * String con la ruta del fichero log detallado que muestra como se crearon
	 * las rutas de forma detallada.
	 */
	protected String logdetalladopath;

	/**
	 * Variable utilizada para calcular el tiempo de cada ejecución de la
	 * creación de rutas.
	 */
	protected long tiemp;

	/**
	 * Matriz que indica si una demanda está cubierta o no.
	 */
	protected boolean demandacub[][];

	/**
	 * Expande la ruta dada en una ruta extendida.
	 * 
	 * @param ruta
	 *            Indice de la ruta.
	 * @param sentido
	 *            Sentido de la ruta.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de dar un error en tiempo de ejecución.
	 * @return Devuelve un ArrayList con la ruta extendida.
	 */
	protected ArrayList<Integer> expandirRuta(int ruta, int sentido)
			throws Exception {
		ArrayList<Integer> expand = new ArrayList<Integer>();
		for (int j = 1; j < ST[ruta].getRuta(sentido).getNumeroParadas(); j++) {
			expand.addAll(floyd.getRecorrido(ST[ruta].getRuta(sentido)
					.getParadasRutaParcial().getParadaConcretaRutaParcial(j),
					ST[ruta].getRuta(sentido).getParadasRutaParcial()
							.getParadaConcretaRutaParcial(j + 1)));
			expand.remove(expand.size() - 1);
		}
		expand.add(ST[ruta]
				.getRuta(sentido)
				.getParadasRutaParcial()
				.getParadaConcretaRutaParcial(
						ST[ruta].getRuta(sentido).getNumeroParadas()));
		return expand;
	}

	/**
	 * Pasa un objeto de tipo ArrayList a un array.
	 * 
	 * @param aL
	 *            ArrayList a pasar.
	 * @return Devuelve un array con los objetos del ArrayList.
	 */
	protected int[] pasarArrayListToArray(ArrayList<Integer> aL) {
		ListIterator<Integer> iterador;
		int array[], i = 0;
		iterador = aL.listIterator();
		array = new int[aL.size()];
		while (iterador.hasNext()) {
			array[i] = iterador.next();
			i++;
		}
		return array;
	}

	/**
	 * Expande la ruta de un ArrayList.
	 * 
	 * @param aL
	 *            ArrayList a expandir.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 * @return Devuelve el ArrayList expandido.
	 */
	protected ArrayList<Integer> expandirArrayList(ArrayList<Integer> aL)
			throws Exception {
		ArrayList<Integer> expand = new ArrayList<Integer>();
		int array[];
		array = pasarArrayListToArray(aL);
		for (int j = 0; j < array.length - 1; j++) {
			expand.addAll(floyd.getRecorrido(array[j], array[j + 1]));
			expand.remove(expand.size() - 1);
		}
		expand.add(array[array.length - 1]);

		return expand;
	}

	/**
	 * Obtiene las matrices de la red.
	 * 
	 * @param pathred
	 *            Localización del archivo de red.
	 * @param pathdemandas
	 *            Localización del archivo de demandas.
	 * @param pathflota
	 *            Localización del archivo de flota.
	 * @param pathparadas
	 *            Localización del archivo de paradas.
	 * @param pathtiempo
	 *            Localización del archivo de tiempo.
	 * @param tiempomaxlinea
	 *            Tiempo máximo que puede durar una ruta completa.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos.
	 */
	protected void obtenerMatrices(String pathred, String pathdemandas,
			String pathflota, String pathparadas, String pathtiempo,
			int tiempomaxlinea) throws IOException {
		datosred = new LeerDatos(pathred, pathdemandas, pathflota, pathparadas,
				pathtiempo, tiempomaxlinea);
		demanda = datosred.getDemanda();
	}// obtenerMatrices

	/**
	 * Obtiene las matrices de la red.
	 * 
	 * @param pathred
	 *            Localización del archivo de red.
	 * @param pathdemandas
	 *            Localización del archivo de demandas.
	 * @param pathflota
	 *            Localización del archivo de flota.
	 * @param pathparadas
	 *            Localización del archivo de paradas.
	 * @param tiempomaxlinea
	 *            Tiempo máximo que puede durar una ruta completa.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos.
	 */
	protected void obtenerMatrices(String pathred, String pathdemandas,
			String pathflota, String pathparadas, int tiempomaxlinea)
			throws IOException {
		datosred = new LeerDatos(pathred, pathdemandas, pathflota, pathparadas,
				tiempomaxlinea);
		demanda = datosred.getDemanda();
	}// obtenerMatrices

	/**
	 * Obtiene las matrices de la red.
	 * 
	 * @param pathred
	 *            Localización del archivo de red.
	 * @param pathdemandas
	 *            Localización del archivo de demandas.
	 * @param pathflota
	 *            Localización del archivo de flota.
	 * @param pathparadas
	 *            Localización del archivo de paradas.
	 * @param pathtiempo
	 *            Localización del archivo de tiempo.
	 * @param pathsemi
	 *            Localización del archivo de las rutas semiautomáticas.
	 * @param tiempomaxlinea
	 *            Tiempo máximo que puede durar una ruta completa.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos.
	 */
	protected void obtenerMatrices(String pathred, String pathdemandas,
			String pathflota, String pathparadas, String pathtiempo,
			String pathsemi, int tiempomaxlinea) throws IOException {
		datosred = new LeerDatos(pathred, pathdemandas, pathflota, pathparadas,
				pathtiempo, pathsemi, tiempomaxlinea);
		demanda = datosred.getDemanda();
	}// obtenerMatricesSemi

	/**
	 * Obtiene las matrices de la red.
	 * 
	 * @param pathred
	 *            Localización del archivo de red.
	 * @param pathdemandas
	 *            Localización del archivo de demandas.
	 * @param pathflota
	 *            Localización del archivo de flota.
	 * @param pathparadas
	 *            Localización del archivo de paradas.
	 * @param pathtiempo
	 *            Localización del archivo de tiempo.
	 * @param tiempomaxlinea
	 *            Tiempo máximo que puede durar una ruta completa.
	 * @param pathmanual
	 *            Localización del archivo de las rutas manuales.
	 * @throws java.io.IOException
	 *             Lanza la excepción en caso de no encontrar alguno de los
	 *             archivos.
	 */
	protected void obtenerMatrices(String pathred, String pathdemandas,
			String pathflota, String pathparadas, String pathtiempo,
			int tiempomaxlinea, String pathmanual) throws IOException {
		datosred = new LeerDatos(pathred, pathdemandas, pathflota, pathparadas,
				pathtiempo, tiempomaxlinea, pathmanual);
		demanda = datosred.getDemanda();
	}// obtenerMatricesManual

	/**
	 * Crea la matriz que indica si dos paradas estan unidas directamente
	 */
	protected void rutasDirectas() {
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDistancia()[i][j] < inf || i == j) {
					rutasCubiertas[i][j] = true;
				} else {
					rutasCubiertas[i][j] = false;
				}
			}
		}
	}// rutasDirectas

	/**
	 * Incia el algoritmo con las rutas manuales.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void rutasInicialesManuales() throws Exception {
		int[][] rutasmanuales = datosred.getRutasSemi();
		ST = new TipoRutaTotal[rutasmanuales.length + 1];
		parescubiertos = new int[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				parescubiertos[i][j] = 0;
			}// for
		}// for
			// Se crean las rutas utilizando los datos de rutasSemiatomáticas.
		for (int i = 0; i < rutasmanuales.length; i++) {
			ST[i + 1] = new TipoRutaTotal(maximasparadas, maximasparadas);
			ST[i + 1].getRutaInicial().setNumeroParadas(2);
			ST[i + 1].getRutaFinal().setNumeroParadas(2);
			ST[i + 1].getRutaInicial().getParadasRutaParcial()
					.setParadasRutaParcial(1, rutasmanuales[i][0]);
			ST[i + 1].getRutaInicial().getParadasRutaParcial()
					.setParadasRutaParcial(2, rutasmanuales[i][1]);
			ST[i + 1].getRutaFinal().getParadasRutaParcial()
					.setParadasRutaParcial(1, rutasmanuales[i][1]);
			ST[i + 1].getRutaFinal().getParadasRutaParcial()
					.setParadasRutaParcial(2, rutasmanuales[i][2]);

			parescubiertos[ST[i + 1].getRutaInicial().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(1)][ST[i + 1]
					.getRutaInicial().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(2)] = (i) * 2 + 1;
			parescubiertos[ST[i + 1].getRutaFinal().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(1)][ST[i + 1].getRutaFinal()
					.getParadasRutaParcial().getParadaConcretaRutaParcial(2)] = (i) * 2 + 2;
		}
	}// rutasInicialesManuales

	/**
	 * Inicializa algunas variables de control.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void iniciar() throws Exception {

		// indica si las paradas estan dentro de una linea en la ruta ida
		paradasenrutainicial = new boolean[datosred.getNumeroNodos()][lineastotales + 1];
		// indica si las paradas estan dentro de una linea en la ruta vuelta
		paradasenrutafinal = new boolean[datosred.getNumeroNodos()][lineastotales + 1];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 1; j <= lineastotales; j++) {
				paradasenrutainicial[i][j] = false;
				paradasenrutafinal[i][j] = false;
			}// for
		}// for
		for (int j = 1; j <= lineastotales; j++) {
			paradasenrutainicial[ST[j].getRutaInicial().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(1)][j] = true;
			paradasenrutainicial[ST[j].getRutaInicial().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(2)][j] = true;
			paradasenrutafinal[ST[j].getRutaFinal().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(1)][j] = true;
			paradasenrutafinal[ST[j].getRutaFinal().getParadasRutaParcial()
					.getParadaConcretaRutaParcial(2)][j] = true;
		}// for
		demandacubierta = 0;
		demandaporlinea = new int[lineastotales * 2];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (parescubiertos[i][j] != 0) {
					demandacubierta += demanda[i][j];
					demandaporlinea[parescubiertos[i][j] - 1] += demanda[i][j];
				}// if
			}// for
		}// for
	}// iniciar

	/**
	 * Calcula el tiempo de la primera a la ultima parada de una ruta cuando
	 * solo existen estas dos paradas.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void calcularTiempoInicial() throws Exception {

		for (int j = 1; j <= lineastotales; j++) {
			ST[j].getRutaInicial().aumentarTiempo(
					distancia[ST[j].getRutaInicial().getParadasRutaParcial()
							.getParadaConcretaRutaParcial(1)][ST[j]
							.getRutaInicial().getParadasRutaParcial()
							.getParadaConcretaRutaParcial(2)]
							+ (tiempomedioparada * demanda[ST[j]
									.getRutaInicial().getParadasRutaParcial()
									.getParadaConcretaRutaParcial(1)][ST[j]
									.getRutaInicial().getParadasRutaParcial()
									.getParadaConcretaRutaParcial(2)]));
			ST[j].getRutaFinal().aumentarTiempo(
					distancia[ST[j].getRutaFinal().getParadasRutaParcial()
							.getParadaConcretaRutaParcial(1)][ST[j]
							.getRutaFinal().getParadasRutaParcial()
							.getParadaConcretaRutaParcial(2)]
							+ (tiempomedioparada * demanda[ST[j].getRutaFinal()
									.getParadasRutaParcial()
									.getParadaConcretaRutaParcial(1)][ST[j]
									.getRutaFinal().getParadasRutaParcial()
									.getParadaConcretaRutaParcial(2)]));
		}// for
	}// calcularTiempoInicial

	/**
	 * Comprueba si una parada está en una ruta.
	 * 
	 * @param parada
	 *            Parada que queremos saber si está en la ruta.
	 * @param indiceruta
	 *            Indice de la ruta.
	 * @param sentido
	 *            Sentido de la ruta (1 ida, 2 vuelta).
	 * @param posicion
	 *            Posicion donde empezar a buscar.
	 * @return True si la parada se encuentra en la ruta. False en caso
	 *         contrario.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected boolean estaEnRuta(int parada, int indiceruta, int sentido,
			int posicion) throws Exception {

		boolean estaenruta = false;
		ArrayList ruta, expand;
		expand = expandirRuta(indiceruta, sentido);
		expand.add(posicion - 1, parada);
		expand = expandirArrayList(expand);
		for (int i = 0; i < expand.size(); i++) {
			for (int j = 0; j < expand.size(); j++) {
				if (i != j) {
					if (expand.get(j) == expand.get(i)
							&& expand.get(j).equals(parada))
						estaenruta = true;
				}
			}
		}
		return estaenruta;
	}// estaEnRuta

	/**
	 * Comprueba si una parada está cubierta dentro de la ventana de tiempo
	 * correspondiente.
	 * 
	 * @param parada
	 *            Parada que queremos saber si está cubierta a tiempo.
	 * @param distancia
	 *            Tiempo transcurrido desde el inicio.
	 * @return True si la parada esta cubierta a tiempo. False en caso
	 *         contrario.
	 */
	protected boolean demandaATiempo(int parada, int distancia) {
		if ((datosred.getHorainicio().plusMinutes(distancia)
				.isAfter(datosred.getTiempos()[parada][0]) && datosred
				.getHorainicio().plusMinutes(distancia)
				.isBefore(datosred.getTiempos()[parada][1]))
				|| datosred.getHorainicio().plusMinutes(distancia)
						.equals(datosred.getTiempos()[parada][0])
				|| datosred.getHorainicio().plusMinutes(distancia)
						.equals(datosred.getTiempos()[parada][1])) {
			return true;
		} else {
			return false;
		}
	}// demandaATiempo

	/**
	 * Escribe el fichero log.
	 * 
	 * @param nuevo
	 *            Indica si el fichero ha de reescribirse (True) o ha de
	 *            respetar el contenido (False).
	 * @throws java.io.IOException
	 *             Lanzada si no encuentra la ruta.
	 */
	public void escribeLog(boolean nuevo) throws IOException {
		PrintWriter fichero = new PrintWriter(new BufferedWriter(
				new FileWriter(logpath, !nuevo)));
		fichero.print(logstring + "\r\n\r\nTiempo empleado: " + tiemp
				+ " milisegundos.");
		fichero.close();
	}

	/**
	 * Escribe el fichero logdetallado.
	 * 
	 * @param nuevo
	 *            Indica si el fichero ha de reescribirse (True) o ha de
	 *            respetar el contenido (False).
	 * @throws java.io.IOException
	 *             Lanzada si no encuentra la ruta.
	 */
	public void escribeLogDetallado(boolean nuevo) throws IOException {
		PrintWriter fichero = new PrintWriter(new BufferedWriter(
				new FileWriter(logdetalladopath, !nuevo)));
		fichero.print(logdetalladostring + "\r\n\r\nTiempo empleado: " + tiemp
				+ " milisegundos.");
		fichero.close();
	}

	/**
	 * Consulta las rutas.
	 * 
	 * @return ArrayList de rutas con su tiempo.
	 */
	public ArrayList<ArrayList<Integer>> getRutasFinales() {
		return rutasfinales;
	}

	/**
	 * Modifica las rutas.
	 * 
	 * @param rutasfinales
	 *            ArrayList de rutas con su tiempo.
	 */
	public void setRutasFinales(ArrayList<ArrayList<Integer>> rutasfinales) {
		this.rutasfinales = rutasfinales;
	}

	/**
	 * Consulta la demanda cubierta por cada linea.
	 * 
	 * @return Array de demanda cubierta por cada linea.
	 */
	public int[] getDemandaPorLinea() {
		return demandaporlinea;
	}

	/**
	 * Modifica la demanda cubierta por cada linea.
	 * 
	 * @param demandaporlinea
	 *            Array de demanda cubierta por cada linea.
	 */
	public void setDemandaPorLinea(int[] demandaporlinea) {
		this.demandaporlinea = demandaporlinea;
	}

	/**
	 * Consulta la matriz de lineas que cubren una demanda.
	 * 
	 * @return Matriz de pares cubiertos.
	 */
	public int[][] getParescubiertos() {
		return parescubiertos;
	}

	/**
	 * Modifica la matriz de lineas que cubren una demanda.
	 * 
	 * @param parescubiertos
	 *            Matriz de pares cubiertos.
	 */
	public void setParescubiertos(int[][] parescubiertos) {
		this.parescubiertos = parescubiertos;
	}

	/**
	 * Consulta el ArrayList de lineas que cubren una demanda.
	 * 
	 * @return ArrayList de rutas que cubren un par de paradas utilizado cuando
	 *         se usan rutas manuales.
	 */
	public ArrayList<Integer>[][] getParesC() {
		return paresC;
	}

	/**
	 * Modifica el objeto de distancias minimas.
	 * 
	 * @param floyd
	 *            Objeto de distancias minimas.
	 */
	public void setFloyd(DistanciasMinimas floyd) {
		this.floyd = floyd;
	}

	/**
	 * Consulta el objeto de los datos leidos.
	 * 
	 * @return Objeto de los datos leidos de la red.
	 */
	public LeerDatos getDatosRed() {
		return datosred;
	}

	/**
	 * Consulta la demanda cubierta entre todas las lineas.
	 * 
	 * @return Demanda cubierta total.
	 */
	public int getDemandaCubierta() {
		return demandacubierta;
	}

	/**
	 * Consulta la demanda total de la red.
	 * 
	 * @return Demanda total de la red.
	 */
	public int getDemandaTotal() {
		return datosred.getDemandaTotal();
	}

	/**
	 * Consulta la demanda no cubierta permitida.
	 * 
	 * @return Demanda no cubierta permitida.
	 */
	public double getDemandaPermitida() {
		return demandapermitida;
	}
}
