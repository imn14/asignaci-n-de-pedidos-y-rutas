package CreaRutas;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * Clase que se encarga de dar apoyo a la construción de rutas de descarga.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class ApoyoGraspDescarga extends Grasp {

	/**
	 * Cadena de texto que informa del tiempo en espera para que se cumplan las
	 * ventanas de tiempo.
	 */
	private String esperaString = "";

	/**
	 * Datos de los tiempos de espera antes de cada parada.
	 */
	private ArrayList<ArrayList<Integer>> esperas;

	/**
	 * Comprueba que las demandas sigan cubiertas.
	 */
	protected void siguenCubiertas(){
		//Como al evaluar se pueden eliminar paradas se comprueba que sigan cubiertas.
		for(int i=0;i<datosred.getNumeroNodos();i++){
			for(int j=0;j<datosred.getNumeroNodos();j++){
				if(parescubiertos[i][j]>0){
					if(pasarArrayListToArray((ArrayList)((ArrayList)rutasfinales.get(parescubiertos[i][j]-1)).get(0))[0]!=i){
						parescubiertos[i][j]=0;
					}else{
						boolean esta=false;
						int[] r=pasarArrayListToArray((ArrayList)((ArrayList)rutasfinales.get(parescubiertos[i][j]-1)).get(0));
						for(int k=1;k<r.length;k++){
							if(j==r[k]){
								esta=true;
							}
						}
						if(!esta){
							parescubiertos[i][j]=0;
						}
					}// else if		
				}
			}						
		}//for
	}//siguenCubiertas
	
	/**
	 * Reorganiza las rutas para que exista una mayor eficiencia entre ellas.
	 * 
	 * @param j0
	 *            Ruta a procesar.
	 * @param parini
	 *            Parada inicial.
	 * @throws java.lang.Exception
	 *             Lanzada en caso de que no podamos modificar la ruta.
	 */
	protected void postProcesamiento(int j0, boolean parini) throws Exception {

		TipoRutaParcial rutaparcial = new TipoRutaParcial(maximasparadas);
		TipoParadasRutaParcial paradasrutaparcial = new TipoParadasRutaParcial(
				maximasparadas);
		int i, i1;
		int im = 0, i1m = 0, aux;
		long mejordistancia, maximamejordistancia;

		if (parini) {
			rutaparcial = ST[j0].getRutaInicial();
		} else {
			rutaparcial = ST[j0].getRutaFinal();
		}// else

		paradasrutaparcial = rutaparcial.getParadasRutaParcial();
		// Se reordenan las paradas dentro de una ruta parcial para que sea más
		// eficiente.
		do {
			maximamejordistancia = 0;
			for (i = 2; i <= rutaparcial.getNumeroParadas() - 2; i++) {
				for (i1 = i + 1; i1 <= rutaparcial.getNumeroParadas() - 1; i1++) {
					if (i1 == i + 1) {
						mejordistancia = distancia[paradasrutaparcial
								.getParadaConcretaRutaParcial(i - 1)][paradasrutaparcial
								.getParadaConcretaRutaParcial(i)]
								+ distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)]
								+ distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 2)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i - 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 2)];
					} else {
						mejordistancia = distancia[paradasrutaparcial
								.getParadaConcretaRutaParcial(i - 1)][paradasrutaparcial
								.getParadaConcretaRutaParcial(i)]
								+ distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)]
								+ distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i1 - 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i1)]
								+ distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i1 + 1)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i - 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i1)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i + 1)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i1 - 1)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i)]
								- distancia[paradasrutaparcial
										.getParadaConcretaRutaParcial(i)][paradasrutaparcial
										.getParadaConcretaRutaParcial(i1 + 1)];
					}// else
					if (mejordistancia > maximamejordistancia) {
						im = i;
						i1m = i1;
						maximamejordistancia = mejordistancia;
					}// if
				}// for
			}// for
			if (maximamejordistancia > 0) {
				aux = paradasrutaparcial.getParadaConcretaRutaParcial(im);
				paradasrutaparcial.setParadasRutaParcial(im,
						paradasrutaparcial.getParadaConcretaRutaParcial(i1m));
				paradasrutaparcial.setParadasRutaParcial(i1m, aux);
			}// if
		} while (maximamejordistancia != 0);

		rutaparcial.setRutaParcial(paradasrutaparcial);
		if (parini)
			ST[j0].setRutaInicial(rutaparcial);
		else
			ST[j0].setRutaFinal(rutaparcial);
	}// postProcesamiento

	/**
	 * Pasa todas las rutas a un objeto de tipo ArrayList.
	 * 
	 * @throws java.lang.Exception
	 *             Lanzada en caso de producir un error en tiempo de ejecución.
	 */
	protected void pasarRutasArrayList() throws Exception {
		rutasfinales = new ArrayList();
		for (int i = 1; i <= lineastotales; i++) {
			new ArrayList();
			ArrayList<Integer> rutaparcialida = new ArrayList<Integer>();
			ArrayList rutaparcial = new ArrayList();
			ArrayList<Integer> rutaparcialvta = new ArrayList<Integer>();
			rutaparcialida = expandirRuta(i, 1);
			rutaparcialvta = expandirRuta(i, 2);
			rutaparcial.add(rutaparcialida);
			rutaparcial
					.add(calcularTiempoRuta(rutaparcialida, 2 * (i - 1) + 1));
			rutasfinales.add(rutaparcial);
			rutaparcial = new ArrayList();
			rutaparcial.add(rutaparcialvta);
			rutaparcial
					.add(calcularTiempoRuta(rutaparcialvta, 2 * (i - 1) + 2));
			rutasfinales.add(rutaparcial);
		}// for
	}// pasarRutasArrayList

	/**
	 * Calcula el tiempo para una ruta.
	 * 
	 * @param ruta
	 *            ArrayList con la ruta expandida.
	 * @param nruta
	 *            Entero con el identificador numérico de la ruta.
	 * @return Devuelve el tiempo de recorrido de dicha ruta.
	 */
	protected int calcularTiempoRuta(ArrayList ruta, int nruta) {
		boolean pase[][] = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		// pase indica que esa demanda ya ha sido cubierta, para casos en los
		// que una ruta pase varias veces por la misma parada.
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				pase[i][j] = false;
			}
		}
		int tiempo = 0;
		int tiemporutatotal = 0;
		int array[] = pasarArrayListToArray(ruta);
		if (nruta % 2 == 0) {
			tiemporutatotal = calcularTiempoRuta(
					(ArrayList) ((ArrayList) rutasfinales.get(nruta - 2))
							.get(0),
					nruta - 1);
		}
		for (int i = 0; i < ruta.size() - 1; i++) {
			tiempo += distancia[array[i]][array[i + 1]];
			tiemporutatotal += distancia[array[i]][array[i + 1]];
			if (parescubiertos[array[0]][array[i + 1]] == nruta) {
				if (!pase[array[0]][array[i + 1]]) {
					if (demandaATiempo(array[i + 1], tiemporutatotal)) {
						tiempo += demanda[array[0]][array[i + 1]]
								* tiempomedioparada;
						tiemporutatotal += demanda[array[0]][array[i + 1]]
								* tiempomedioparada;
						pase[array[0]][array[i + 1]] = true;
					}
				}
			}
		}
		return tiempo;
	}// calcularTiempoRuta

	/**
	 * Evalua todas las rutas, mirando y corrigiendo posibles fallos en ellas,
	 * lo hace quitando paradas problemáticas y poniendo otras mejores.
	 */
	protected void evaluarParesParadas() {
		int numiter = 0;
		do {
			for (int i = 0; i < rutasfinales.size(); i++) {
				int primer = -1, ulti = -1;
				ArrayList rutaparcial = new ArrayList();
				rutaparcial = (ArrayList) ((ArrayList) rutasfinales.get(i))
						.get(0);
				if (rutaparcial.size() > 4) {
					for (int j = 0; j < 4; j++) {
						for (int k = j + 1; k <= 4; k++) {
							if (rutaparcial.get(j) == rutaparcial.get(k)) {
								primer = j;
							}// if
						}// for
					}// for
				}// if
				if (primer != -1) {
					for (int j = 1; j <= primer; j++) {
						rutaparcial.remove(1);
					}
				}
				if (rutaparcial.size() >= 5) {
					for (int j = rutaparcial.size() - 1; j > rutaparcial.size() - 5; j--) {
						for (int k = j - 1; k > rutaparcial.size() - 5; k--) {
							if (rutaparcial.get(j) == rutaparcial.get(k)) {
								ulti = j;
							}// if
						}// for
					}// for
				}// if
				if (ulti != -1) {
					for (int j = rutaparcial.size() - 2; j >= ulti; j--) {
						rutaparcial.remove(rutaparcial.size() - 2);
					}// for
				}// if

				primer = -1;
				ulti = -1;
				for (int j = 0; j < rutaparcial.size() - 1; j++) {
					for (int k = j + 1; k < rutaparcial.size() - 1; k++) {
						if (rutaparcial.get(j) == rutaparcial.get(k)) {
							primer = j;
							ulti = k;
						}// if
					}// for
				}// for
				if (ulti != -1) {
					for (int j = primer; j < ulti; j++) {
						rutaparcial.remove(primer + 1);
					}// for
				}// if
				for (int j = 0; j < rutaparcial.size() - 1; j++) {
					if (floyd.getRecorrido((int) rutaparcial.get(j),
							(int) rutaparcial.get(j + 1)).size() > 2) {
						for (int r = 1; r < floyd.getRecorrido(
								(int) rutaparcial.get(j),
								(int) rutaparcial.get(j + 1)).size() - 1; r++) {
							rutaparcial.add(
									j + 1,
									floyd.getRecorrido(
											(int) rutaparcial.get(j),
											(int) rutaparcial.get(j + 1))
											.get(r));
						}
					}
				}
			}// for
			recalcularTiempos();
			calcularDemandaCubierta();
			numiter++;
		} while (numiter < numiteracionesbl);
	}// evaluarParesParadas

	/**
	 * Recalcula todos los tiempos de todas las rutas.
	 */
	protected void recalcularTiempos() {
		boolean pase[][] = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				pase[i][j] = false;
			}
		}
		esperaString = "";
		esperas = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < rutasfinales.size(); i++) {
			esperas.add(new ArrayList<Integer>());
			for (int j = 0; j < pasarArrayListToArray((ArrayList) ((ArrayList) rutasfinales
					.get(i)).get(0)).length; j++) {
				esperas.get(i).add(0);
			}
		}// for
		int distanciaj = 0;
		for (int i = 0; i < rutasfinales.size(); i++) {
			int tiempo = 0;
			if (i % 2 == 0) {
				distanciaj = 0;
			} else {
				for (int k = 0; k < datosred.getNumeroNodos(); k++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						if (parescubiertos[k][j] == i + 1 && demandacub[k][j]) {
							tiempo += demanda[k][j] * tiempomedioparada;
							distanciaj += demanda[k][j] * tiempomedioparada;
						}
					}
				}
			}// else
			int array[] = pasarArrayListToArray((ArrayList) ((ArrayList) rutasfinales
					.get(i)).get(0));
			// El tiempo de espera solo puede darse si no se deja de cubrir
			// demanda en paradas posteriores debido al incumplimiento de las
			// ventanas de tiempo.
			for (int j = -1; j < array.length - 1; j++) {
				if (j != -1) {
					tiempo += distancia[array[j]][array[j + 1]];
					distanciaj += distancia[array[j]][array[j + 1]];
				}
				if (parescubiertos[array[0]][array[j + 1]] == i + 1) {
					if (!pase[array[0]][array[j + 1]]) {
						if (demandaATiempo(array[j + 1], distanciaj)) {
							tiempo += demanda[array[0]][array[j + 1]]
									* tiempomedioparada;
							distanciaj += demanda[array[0]][array[j + 1]]
									* tiempomedioparada;
							pase[array[0]][array[j + 1]] = true;
						} else if (datosred.getTiempos()[array[j + 1]][0]
								.isAfter(datosred.getHorainicio().plusMinutes(
										distanciaj))) {
							int esp = (int) ChronoUnit.MINUTES.between(datosred
									.getHorainicio().plusMinutes(distanciaj),
									datosred.getTiempos()[array[j + 1]][0]);
							boolean permite = true;
							int tiempoaux = distanciaj + esp;
							for (int k = j + 1; k < array.length - 1; k++) {
								tiempoaux += distancia[array[k]][array[k + 1]];
								if (parescubiertos[array[0]][array[k + 1]] == i + 1) {
									if (!pase[array[0]][array[k + 1]]) {
										if (demandaATiempo(array[k + 1],
												tiempoaux)) {
											tiempoaux += demanda[array[0]][array[k + 1]]
													* tiempomedioparada;
										} else if (demanda[array[0]][array[j + 1]] <= demanda[array[0]][array[k + 1]]
												|| datosred.getDemandaUrgente()[array[0]][array[k + 1]]) {
											permite = false;
										}
									}
								}
							}// for
							if (i % 2 == 0) {
								int arrayaux[] = pasarArrayListToArray((ArrayList) ((ArrayList) rutasfinales
										.get(i + 1)).get(0));
								for (int k = -1; k < arrayaux.length - 1; k++) {
									if (k != -1) {
										tiempoaux += distancia[arrayaux[k]][arrayaux[k + 1]];
									}
									if (parescubiertos[arrayaux[0]][arrayaux[k + 1]] == i + 2) {
										if (!pase[arrayaux[0]][arrayaux[j + 1]]) {
											if (demandaATiempo(arrayaux[k + 1],
													tiempoaux)) {
												tiempoaux += demanda[arrayaux[0]][arrayaux[k + 1]]
														* tiempomedioparada;
											} else if (demanda[array[0]][array[j + 1]] <= demanda[arrayaux[0]][arrayaux[k + 1]]
													|| datosred
															.getDemandaUrgente()[arrayaux[0]][arrayaux[k + 1]]) {
												permite = false;
											}
										}
									}
								}// for
							}// if
							if (permite == true) {
								tiempo += demanda[array[0]][array[j + 1]]
										* tiempomedioparada + esp;
								distanciaj += demanda[array[0]][array[j + 1]]
										* tiempomedioparada + esp;
								if (esperas.get(i).get(j + 1) != esp) {
									esperaString += "En la ruta "
											+ (i + 1)
											+ ", antes de descargar en la parada "
											+ array[j + 1]
											+ ", se ha de esperar "
											+ esp
											+ " minutos para cumplir con la ventana de tiempo.\n";
									esperas.get(i).set(j + 1, esp);
								}// if
							}
						} else if (distanciaj > tmaxlinea) {
							demandaporlinea[parescubiertos[array[0]][array[j + 1]] - 1] -= datosred
									.getDemanda()[array[0]][array[j + 1]];
							parescubiertos[array[0]][array[j + 1]] = 0;
						}
					}
				}
			}// for
			((ArrayList) rutasfinales.get(i)).remove(1);
			((ArrayList) rutasfinales.get(i)).add(1, tiempo);
		}// for
	}// recalcularTiempos

	/**
	 * Calcula la demanda cubierta.
	 */
	protected void calcularDemandaCubierta() {
		demandacubierta = 0;
		for (int i = 0; i < demandacub.length; i++) {
			for (int j = 0; j < demandacub[i].length; j++) {
				demandacub[i][j] = false;
			}
		}
		int demandacubiertaaux = 0;
		int distanciaj = 0;
		for (int i = 0; i < rutasfinales.size(); i++) {
			if (i % 2 == 0) {
				distanciaj = 0;
			} else {
				for (int k = 0; k < datosred.getNumeroNodos(); k++) {
					for (int j = 0; j < datosred.getNumeroNodos(); j++) {
						if (parescubiertos[k][j] == i + 1) {
							distanciaj += demanda[k][j] * tiempomedioparada;
						}
					}
				}
			}// else
			demandacubiertaaux = 0;
			int array[] = pasarArrayListToArray((ArrayList) ((ArrayList) rutasfinales
					.get(i)).get(0));
			if (demandaATiempo(array[0], distanciaj)) {
				for (int j = 0; j < array.length - 1; j++) {
					distanciaj += esperas.get(i).get(j + 1);
					distanciaj += distancia[array[j]][array[j + 1]];
					if (parescubiertos[array[0]][array[j + 1]] == i + 1) {
						if (demandaATiempo(array[j + 1], distanciaj)) {
							if (!demandacub[array[0]][array[j + 1]]) {
								demandacubiertaaux += demanda[array[0]][array[j + 1]];
								demandacub[array[0]][array[j + 1]] = true;
								distanciaj += demanda[array[0]][array[j + 1]]
										* tiempomedioparada;
							}
						} else {
							parescubiertos[array[0]][array[j + 1]] = 0;
						}
					}
				}
				demandaporlinea[i] = demandacubiertaaux;
				demandacubierta += demandacubiertaaux;
			} else {
				demandaporlinea[i] = 0;
			}
		}
	}// calcularDemandaCubierta

	/**
	 * Devuelve un string indicando las ventanas de tiempo no cumplidas.
	 * 
	 * @return nocumple String indicando las ventanas de tiempo no cumplidas.
	 */
	public String cumpleTiempo() {
		boolean pase[][] = new boolean[datosred.getNumeroNodos()][datosred
				.getNumeroNodos()];
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				pase[i][j] = false;
			}
		}
		String nocumple = "";
		for (int i = 0; i < datosred.getNumeroNodos(); i++) {
			for (int j = 0; j < datosred.getNumeroNodos(); j++) {
				if (datosred.getDemandaUrgente()[i][j]
						&& (!demandacub[i][j] || parescubiertos[i][j] < 1)) {
					nocumple += "La demanda urgente entre las paradas " + i
							+ " y " + j + " no es cubierta.\n";
				}
			}
		}
		int espaciorecorrido = 0;
		for (int i = 0; i < rutasfinales.size(); i++) {
			String[] rutaAux = ("" + rutasfinales.get(i).get(0))
					.split(", |\\[|\\]");
			ArrayList<Integer> ruta = new ArrayList<Integer>();
			for (int k = 1; k < rutaAux.length; k++) {
				ruta.add(Integer.parseInt(rutaAux[k]));
			}// for
			if (i % 2 == 0) {
				espaciorecorrido = 0;
			} else {
				espaciorecorrido += demandaporlinea[i] * tiempomedioparada;
			}// else
			if (demandaATiempo(ruta.get(0), espaciorecorrido)) {
				for (int j = 1; j < ruta.size(); j++) {
					espaciorecorrido += esperas.get(i).get(j);
					espaciorecorrido += distancia[ruta.get(j - 1)][ruta.get(j)];
					if (parescubiertos[ruta.get(0)][ruta.get(j)] == i + 1
							&& !pase[ruta.get(0)][ruta.get(j)]) {
						if (demandaATiempo(ruta.get(j), espaciorecorrido)) {
							espaciorecorrido += demanda[ruta.get(0)][ruta
									.get(j)] * tiempomedioparada;
							pase[ruta.get(0)][ruta.get(j)] = true;
						} else if (demanda[ruta.get(0)][ruta.get(j)] > 0) {
							nocumple += "En la ruta "
									+ (i + 1)
									+ " no se cumple la ventana de tiempo en la parada "
									+ ruta.get(j)
									+ " y por tanto no se cubre esa demanda.\n";
						}
					}// if
				}// for
				if (!demandaATiempo(ruta.get(ruta.size() - 1), espaciorecorrido)) {
					if (i % 2 != 0 && demandaporlinea[i] == 0) {
						ArrayList<Integer> r = new ArrayList<Integer>();
						r.add(0);
						r.add(0);
						ArrayList r2 = new ArrayList();
						r2.add(r);
						r2.add(0);
						rutasfinales.set(i, r2);
					}
				}
			} else {
				if (i % 2 != 0 && demandaporlinea[i] == 0) {
					// Si la ruta parcial sobrepasa el límite de tiempo máximo
					// de la ruta total se elimina.
					ArrayList<Integer> r = new ArrayList<Integer>();
					r.add(0);
					r.add(0);
					ArrayList r2 = new ArrayList();
					r2.add(r);
					r2.add(0);
					rutasfinales.set(i, r2);
				} else {
					nocumple += "La parada "
							+ ruta.get(0)
							+ ", que es la parada origen, no cumple la ventana de tiempo y por tanto no se cubre la demanda de la ruta "
							+ (i + 1) + ".\n";
				}
			}// else
		}// for
		nocumple += "\n" + esperaString;
		return nocumple;
	}// cumpleTiempo
}
