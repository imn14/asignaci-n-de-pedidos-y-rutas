package CreaRutas;

/**
 * Clase que contiene la estructura de datos de los transportes.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class TipoCamion {

	/**
	 * Número camiones de este tipo
	 */
	private int numeroCamiones;

	/**
	 * Tipo de camión
	 */
	private boolean[] tipo;

	/**
	 * Identificador de tipo de camión
	 */
	private int idtipo;

	/**
	 * Capacidad de los camiones de este tipo
	 */
	private int capacidad;

	/**
	 * Constructor.
	 * 
	 * @param numeroCamiones
	 *            Entero con el número de camiones de este tipo.
	 * @param tipo
	 *            Entero que indica el tipo de camión creado.
	 * @param capacidad
	 *            Entero con con la capacidad de los camiones de este tipo.
	 */
	public TipoCamion(int numeroCamiones, int tipo, int capacidad) {
		this.tipo = new boolean[3];
		this.numeroCamiones = numeroCamiones;
		idtipo = tipo;
		switch (tipo) {
		case 1:
			this.tipo[0] = false;
			this.tipo[1] = false;
			this.tipo[2] = false;
			break;
		case 2:
			this.tipo[0] = true;
			this.tipo[1] = false;
			this.tipo[2] = false;
			break;
		case 3:
			this.tipo[0] = false;
			this.tipo[1] = true;
			this.tipo[2] = false;
			break;
		case 4:
			this.tipo[0] = false;
			this.tipo[1] = false;
			this.tipo[2] = true;
			break;
		case 5:
			this.tipo[0] = true;
			this.tipo[1] = true;
			this.tipo[2] = false;
			break;
		case 6:
			this.tipo[0] = true;
			this.tipo[1] = false;
			this.tipo[2] = true;
			break;
		case 7:
			this.tipo[0] = false;
			this.tipo[1] = true;
			this.tipo[2] = true;
			break;
		case 8:
			this.tipo[0] = true;
			this.tipo[1] = true;
			this.tipo[2] = true;
			break;
		}
		this.capacidad = capacidad;
	}

	/**
	 * Consulta del número de camiones de este tipo.
	 * 
	 * @return Número de camiones de este tipo.
	 * @see #setNumeroCamiones
	 */
	public int getNumeroCamiones() {
		return numeroCamiones;
	}

	/**
	 * Consulta el identificador de tipo.
	 * 
	 * @return Identificador de este tipo.
	 */
	public int getIdTipo() {
		return idtipo;
	}

	/**
	 * Modifica el número de camiones de este tipo.
	 * 
	 * @param numero
	 *            Número de camiones de este tipo.
	 * @see #getNumeroCamiones
	 */
	public void setNumeroCamiones(int numero) {
		numeroCamiones = numero;
	}

	/**
	 * Consulta el array de boolean de este tipo.
	 * 
	 * @return Array de boolean de este tipo.
	 */
	public boolean[] getTipo() {
		return tipo;
	}

	/**
	 * Consulta la capaciadad de los camiones de este tipo.
	 * 
	 * @return Capaciadad de los camiones de este tipo.
	 */
	public int getCapacidad() {
		return capacidad;
	}
}
