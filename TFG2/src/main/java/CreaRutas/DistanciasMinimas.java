package CreaRutas;

import java.util.ArrayList;

/**
 * Clase que se encarga de calcular las distancias con el método de Floyd.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class DistanciasMinimas {

	/**
	 * Numero de nodos de la red.
	 */
	private int numNodos;

	/**
	 * Matriz con los datos de las distancias de la red.
	 */
	private int distancia[][];

	/**
	 * Matriz de caminos minimos.
	 */
	private int[][] matrizCaminosMin;

	/**
	 * ArayList con el recorrido minimo entre un par de nodos.
	 */
	private ArrayList<Integer> recorrido;

	/**
	 * Constructor.
	 * 
	 * @param numNodos
	 *            Recibe el número de nodos de la red.
	 * @param distancia
	 *            Recibe la matriz de distancias.
	 */
	public DistanciasMinimas(int numNodos, int distancia[][]) {
		this.numNodos = numNodos;
		this.distancia = distancia;
		recorrido = new ArrayList<Integer>();
	}// Constructor

	/**
	 * Realiza el algoritmo de Floyd para calcular el camino mínimo entre cada
	 * par de nodos.
	 */
	public void floyd() {
		// Matriz de caminos mínimos
		matrizCaminosMin = new int[numNodos][numNodos];
		for (int i = 0; i < numNodos; i++) {
			for (int j = 0; j < numNodos; j++) {
				matrizCaminosMin[i][j] = -1;
			}
		}
		// Matriz de distancias mínimas
		for (int i = 0; i < numNodos; i++) {
			for (int j = 0; j < numNodos; j++) {
				for (int k = 0; k < numNodos; k++) {
					if (distancia[j][k] > distancia[j][i] + distancia[i][k]) {
						distancia[j][k] = distancia[j][i] + distancia[i][k];
						matrizCaminosMin[j][k] = i;
					}
				}
			}
		}
		// El coste de cada nodo a sí mismo es 0.
		for (int i = 0; i < numNodos; i++) {
			distancia[i][i] = 0;
			matrizCaminosMin[i][i] = -1;
		}
	}

	/**
	 * Devuelve la matriz de distancias.
	 * 
	 * @return distancia Matriz con los datos de las distancias de la red.
	 */
	public int[][] getDistancia() {
		return distancia;
	}

	/**
	 * Devuelve el recorrido que existe entre un par de nodos.
	 * 
	 * @param origen
	 *            Nodo origen.
	 * @param destino
	 *            Nodo destino.
	 * @return recorrido ArrayList con el recorrido existente entre el origen y
	 *         el destino.
	 */
	public ArrayList<Integer> getRecorrido(int origen, int destino) {
		getCaminoMinimo(origen, destino, true);
		return recorrido;
	}

	/**
	 * Devuelve el camino minimo que existe entre un par de nodos.
	 * 
	 * @param origen
	 *            Nodo origen.
	 * @param destino
	 *            Nodo destino.
	 * @param inicio
	 *            Indica si es o no inicio de ruta.
	 */
	public void getCaminoMinimo(int origen, int destino, boolean inicio) {

		if (inicio)
			recorrido.clear();

		if (matrizCaminosMin[origen][destino] != -1) {
			getCaminoMinimo(origen, matrizCaminosMin[origen][destino], false);
			getCaminoMinimo(matrizCaminosMin[origen][destino], destino, false);
		} else
			recorrido.add(origen);

		if (inicio)
			recorrido.add(destino);
	}

}
