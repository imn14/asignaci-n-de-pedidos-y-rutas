package CreaRutas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Clase que se encarga de asignar transporte a las rutas.
 * 
 * @author Ismael Moral Núñez
 * 
 */
public class AsignaTransportes {

	/**
	 * Objeto Grasp con los datos del problema.
	 */
	private Grasp grasp;

	/**
	 * Matriz que indica las caracteristicas de las rutas según sus paradas.
	 */
	private boolean[][] limitacionesruta;

	/**
	 * Array con el tiempo máximo de cada ruta total.
	 */
	private int distanciamax[];

	/**
	 * Array con la capaciadad máxima de cada ruta total.
	 */
	private int capacidadmax[];

	/**
	 * ArrayList con los errores de la solución anterior.
	 */
	private ArrayList<ArrayList<Integer>> erroresanteriores;

	/**
	 * Entero con la demanda cubierta por la solución anterior.
	 */
	private int demandacubiertaanterior;

	/**
	 * Constructor, crea una nueva instancia del tipo AsignaTransportes.
	 * 
	 * @param grasp
	 *            Recibe el grasp con las rutas creadas y el resto de los datos.
	 */
	public AsignaTransportes(Grasp grasp) {
		this.grasp = grasp;
		distanciamax = new int[grasp.lineastotales];
		capacidadmax = new int[grasp.lineastotales];
		this.demandacubiertaanterior = 0;
		erroresanteriores = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < grasp.lineastotales; i++) {
			distanciamax[i] = grasp.tmaxlinea;
			capacidadmax[i] = grasp.getDatosRed().getMaximaCapacidad();
			erroresanteriores.add(new ArrayList<Integer>());
		}
	}

	/**
	 * Constructor, crea una nueva instancia del tipo AsignaTransportes.
	 * 
	 * @param grasp
	 *            Recibe el grasp con las rutas creadas y el resto de los datos.
	 * @param distanciamax
	 *            Recibe el array de distancias máximas por ruta.
	 * @param capacidadmax
	 *            Recibe el array de capacidades máximas por ruta.
	 * @param erroresanteriores
	 *            Recibe el ArrayList con los errores de la solución anterior.
	 * @param demandacubiertaanterior
	 *            Recibe la demanda cubierta por la solución anterior.
	 */
	public AsignaTransportes(Grasp grasp, int[] distanciamax,
			int[] capacidadmax,
			ArrayList<ArrayList<Integer>> erroresanteriores,
			int demandacubiertaanterior) {
		this.grasp = grasp;
		this.distanciamax = distanciamax;
		this.capacidadmax = capacidadmax;
		this.erroresanteriores = erroresanteriores;
		this.demandacubiertaanterior = demandacubiertaanterior;
	}

	/**
	 * Asigna los camiones a las rutas y lo escribe en el fichero.
	 * 
	 * @param ruta
	 *            Ruta del archivo donde se va a guardar la solución.
	 * @param rutarutas
	 *            Ruta del archivo donde se van a guardar las rutas creadas.
	 * @param grasp
	 *            Objeto grasp utilizado para calcular las rutas.
	 * @param camb
	 *            Booleano que indica si la anterior solución sufrió algún
	 *            cambio respecto de su anterior solución.
	 * @throws java.io.IOException
	 *             Lanzada si no encuentra la ruta.
	 */
	public void asigna(String ruta, String rutarutas, Grasp grasp, boolean camb)
			throws IOException {
		limitacionesRuta();
		escribirResultados(
				asignador(ordenaRutas(), ordenaCamiones(), ruta, rutarutas,
						camb), ruta, rutarutas);
	}

	/**
	 * Calcula las limitaciones que sufre cada ruta respecto a las diferentes
	 * restricciones de cada parada y lo almacena en limitacionesruta.
	 */
	private void limitacionesRuta() {
		boolean aux = false;
		boolean aux2 = false;
		String[] rutaAux;
		limitacionesruta = new boolean[grasp.getRutasFinales().size() / 2][3];
		for (int i = 0; i < grasp.getRutasFinales().size() / 2; i++) {
			for (int j = 0; j < 3; j++) {
				aux = false;
				aux2 = false;
				while (aux == false && aux2 == false) {
					rutaAux = ("" + grasp.getRutasFinales().get(2 * i).get(0))
							.split(", |\\[|\\]");
					for (int k = 1; k < rutaAux.length; k++) {
						if (grasp.getDatosRed().getParadas()[Integer
								.parseInt(rutaAux[k])][j]) {
							aux = true;
						}// if
					}// for
					rutaAux = ("" + grasp.getRutasFinales().get(2 * i + 1)
							.get(0)).split(", |\\[|\\]");
					for (int k = 1; k < rutaAux.length; k++) {
						if (grasp.getDatosRed().getParadas()[Integer
								.parseInt(rutaAux[k])][j]) {
							aux = true;
						}// if
					}// for
					aux2 = true;
				}// while
				limitacionesruta[i][j] = aux;
			}// for
		}// for
	}// limitacionesRuta

	/**
	 * Ordena las rutas primero por restricciones y si estas coinciden por
	 * demanda.
	 * 
	 * @return Lista de rutas ordenadas por restricciones y demanda.
	 */
	private ArrayList<Integer> ordenaRutas() {
		ArrayList<Integer> rutasordenadas = new ArrayList<Integer>();
		int aux, auxMax, auxj;
		for (int i = 0; i < grasp.getRutasFinales().size() / 2; i++) {
			auxMax = -1;
			auxj = -1;
			for (int j = 0; j < grasp.getRutasFinales().size() / 2; j++) {
				aux = 0;
				for (int k = 0; k < limitacionesruta[j].length; k++) {
					if (limitacionesruta[j][k] == true
							&& !rutasordenadas.contains(j)) {
						aux++;
					}// if
				}// for
				if (!rutasordenadas.contains(j)
						&& (aux > auxMax || (aux == auxMax && grasp
								.getDemandaPorLinea()[j * 2]
								+ grasp.getDemandaPorLinea()[2 * j + 1] > grasp
								.getDemandaPorLinea()[auxj * 2]
								+ grasp.getDemandaPorLinea()[2 * auxj + 1]))) {
					auxMax = aux;
					auxj = j;
				}// if
			}// for
			rutasordenadas.add(auxj);
		}// for
		return rutasordenadas;
	}// ordenaRutas

	/**
	 * Ordena los camiones primero por restricciones y si estas coinciden por
	 * capacidad.
	 * 
	 * @return Lista de camiones ordenados por restricciones y capacidad.
	 */
	private ArrayList<TipoCamion> ordenaCamiones() {
		ArrayList<TipoCamion> camionesordenados = new ArrayList<TipoCamion>();
		int aux, auxMax, auxj;
		for (int i = 0; i < grasp.getDatosRed().getCamiones().size(); i++) {
			auxMax = -1;
			auxj = -1;
			for (int j = 0; j < grasp.getDatosRed().getCamiones().size(); j++) {
				aux = 0;
				for (int k = 0; k < grasp.getDatosRed().getCamiones().get(j)
						.getTipo().length; k++) {
					if (grasp.getDatosRed().getCamiones().get(j).getTipo()[k] == true
							&& !camionesordenados.contains(grasp.getDatosRed()
									.getCamiones().get(j))) {
						aux++;
					}// if
				}// for
				if (!camionesordenados.contains(grasp.getDatosRed()
						.getCamiones().get(j))
						&& (aux > auxMax || (aux == auxMax && grasp
								.getDatosRed().getCamiones().get(j)
								.getCapacidad() < grasp.getDatosRed()
								.getCamiones().get(auxj).getCapacidad()))) {
					auxMax = aux;
					auxj = j;
				}// if
			}// for
			camionesordenados.add(grasp.getDatosRed().getCamiones().get(auxj));
		}// for
		return camionesordenados;
	}// ordenaCamiones

	/**
	 * Asigna los camiones a las rutas.
	 * 
	 * @param ordenaRutas
	 *            Rutas ordenadas por restricciones y demanda, en ese orden.
	 * @param ordenaCamiones
	 *            Camiones ordenados por restricciones y capacidad, en ese
	 *            orden.
	 * @param ruta
	 *            Ruta del archivo donde se va a guardar la solución.
	 * @param rutarutas
	 *            Ruta del archivo donde se van a guardar las rutas creadas.
	 * @param camb
	 *            Booleano que indica si la anterior solución sufrió algún
	 *            cambio respecto de su anterior solución.
	 * @return Camiones ordenados en el orden de las rutas a las que cubren.
	 */
	private TipoCamion[] asignador(ArrayList<Integer> ordenaRutas,
			ArrayList<TipoCamion> ordenaCamiones, String ruta,
			String rutarutas, boolean camb) {
		TipoCamion[] camiones = new TipoCamion[ordenaRutas.size()];
		TipoCamion[] camiones2 = new TipoCamion[ordenaRutas.size()];
		ArrayList<ArrayList<Integer>> errores = new ArrayList<ArrayList<Integer>>();
		// ArrayList con las paradas que no permiten asignar un transporte.
		for (int i = 0; i < ordenaRutas.size(); i++) {
			ArrayList<Integer> a = new ArrayList<Integer>();
			a.add(-1); // Si errores vale -1 significa que la ruta aún no tiene
						// transporte asignado ni comprobado.
			errores.add(a);
		}
		ArrayList<Integer> numcamiones = new ArrayList<Integer>();
		for (int i = 0; i < grasp.getDatosRed().getCamiones().size(); i++) {
			numcamiones.add(grasp.getDatosRed().getCamiones().get(i)
					.getNumeroCamiones());
		}
		int demandat = 0;
		boolean aux, cambio;
		for (int i = 0; i < ordenaRutas.size(); i++) {
			cambio = false;
			if (!cambio) {
				if (grasp.getDemandaPorLinea()[ordenaRutas.get(i) * 2]
						+ grasp.getDemandaPorLinea()[1 + ordenaRutas.get(i) * 2] > 0) {
					for (TipoCamion camion : ordenaCamiones) {
						if (numcamiones.get(grasp.getDatosRed().getCamiones()
								.indexOf(camion)) > 0
								&& !(camion.getCapacidad() < grasp
										.getDemandaPorLinea()[ordenaRutas
										.get(i) * 2] || camion.getCapacidad() < grasp
										.getDemandaPorLinea()[1 + ordenaRutas
										.get(i) * 2])) {
							aux = false;
							for (int j = 0; j < 3; j++) {
								if (limitacionesruta[ordenaRutas.get(i)][j] != camion
										.getTipo()[j]
										&& limitacionesruta[ordenaRutas.get(i)][j] == true) {
									aux = true;
									errores.get(ordenaRutas.get(i)).add(j);
									// Si errores vale un numero de 0 a 2
									// significa que la ruta tiene una
									// limitación respecto a las paradas.
								}// if
							}// for
							if (aux == false && camiones[i] == null) {
								camiones[i] = camion;
								numcamiones
										.set(grasp.getDatosRed().getCamiones()
												.indexOf(camion),
												numcamiones.get(grasp
														.getDatosRed()
														.getCamiones()
														.indexOf(camion)) - 1);
								cambio = true;
								demandat += grasp.getDemandaPorLinea()[ordenaRutas
										.get(i) * 2]
										+ grasp.getDemandaPorLinea()[1 + ordenaRutas
												.get(i) * 2];
								errores.get(ordenaRutas.get(i)).clear();
								errores.get(ordenaRutas.get(i)).add(-4);
								// Si errores vale -4 significa que la ruta
								// tiene transporte asignado.
							}// if
						} else {
							errores.get(ordenaRutas.get(i)).add(-3);
							// Si errores vale -3 significa que la ruta cubre
							// demasiada demanda.
						}
					}// for
				} else {
					errores.get(ordenaRutas.get(i)).add(-2);
					// Si errores vale -2 significa que la ruta no cubre
					// demanda.
				}
				if (cambio == false)
					camiones[i] = null;
			}// if
		}// for
		for (int i = 0; i < errores.size(); i++) {
			if (erroresanteriores.get(i).contains(-4)) {
				if (!errores.get(i).contains(-4)) {
					camb = true;
				}
			}
		}
		boolean quedan = false;
		for (TipoCamion camion : ordenaCamiones) {
			if (numcamiones.get(grasp.getDatosRed().getCamiones()
					.indexOf(camion)) > 0) {
				quedan = true;
			}
		}
		// Si no se ha cubierto toda la demanda, quedan transportes y la
		// solución anterior es diferente y peor se recalculan las rutas
		// marcadas con error.
		if (demandat < grasp.getDatosRed().getDemandaTotal() && camb && quedan
				&& demandat > demandacubiertaanterior) {
			for (int i = 0; i < ordenaRutas.size(); i++) {
				if (!errores.get(i).contains(-4)) {
					new ArreglaRuta(ordenaRutas, errores, grasp, ruta,
							rutarutas, distanciamax, capacidadmax, demandat);
					return null;
				}
			}
		}
		// Se ordenan los transportes de acuerdo a la ruta que cubren.
		for (int i = 0; i < ordenaRutas.size(); i++) {
			for (int j = 0; j < ordenaRutas.size(); j++) {
				if (ordenaRutas.get(j) == i) {
					camiones2[i] = camiones[j];
				}
			}// for
		}// for
		return camiones2;
	}// asignador

	/**
	 * Escribe los resultados en un fichero.
	 * 
	 * @param camiones
	 *            Array en el que se indica de forma ordenada el camión
	 *            utilizado para cubrir cada ruta.
	 * @param ruta
	 *            Ruta del archivo donde se va a guardar la solución.
	 * @param ruta
	 *            Ruta del archivo donde se van a guardar las rutas calculadas.
	 * @throws java.io.IOException
	 *             Lanzada si no encuentra la ruta.
	 */
	private void escribirResultados(TipoCamion[] camiones, String ruta,
			String rutarutas) throws IOException {
		if (camiones != null) {
			PrintWriter fichero = new PrintWriter(new BufferedWriter(
					new FileWriter(ruta)));
			PrintWriter ficherorutas = new PrintWriter(new BufferedWriter(
					new FileWriter(rutarutas)));
			int demandacubierta = 0;
			String cump = "\n";
			if (grasp instanceof GraspCarga) {
				cump += ((GraspCarga) grasp).cumpleTiempo();
			} else if (grasp instanceof GraspDescarga) {
				cump += ((GraspDescarga) grasp).cumpleTiempo();
			}
			if (!cump.equals("\n\n")) {
				System.out.println(cump);
				fichero.println(cump);
				System.out.println();
				fichero.println();
			} // if
			for (int i = 0; i < grasp.getRutasFinales().size(); i++) {
				int j = i + 1;
				if (j % 2 != 0) {
					if (camiones[i / 2] == null) {
						System.out
								.println("\n Ningún transporte asignado a esta ruta\n");
						fichero.println("\n Ningún transporte asignado a esta ruta\n");
					} else {
						System.out
								.println("\n Un camión de tipo "
										+ camiones[i / 2].getIdTipo()
										+ " con "
										+ camiones[i / 2].getCapacidad()
										+ " unidades de capacidad ha sido asignado a esta ruta.\n");
						fichero.println("\n Un camión de tipo "
								+ camiones[i / 2].getIdTipo()
								+ " con "
								+ camiones[i / 2].getCapacidad()
								+ " unidades de capacidad ha sido asignado a esta ruta.\n");
						demandacubierta += grasp.getDemandaPorLinea()[i];
						demandacubierta += grasp.getDemandaPorLinea()[i + 1];
					}// else
				}// if
				System.out.println("Ruta: " + j);
				fichero.println("Ruta: " + j);
				if (grasp.getRutasFinales().get(i).get(1) > 0) {
					System.out.println((((ArrayList) grasp.getRutasFinales()
							.get(i)).get(0)).toString());
					System.out.println("Tiempo: "
							+ (((ArrayList) grasp.getRutasFinales().get(i))
									.get(1)).toString());
					System.out.print("Demanda cubierta: "
							+ grasp.getDemandaPorLinea()[i]);
					System.out.print("\t");
					for (int m = 0; m < grasp.datosred.getNumeroNodos(); m++) {
						for (int n = 0; n < grasp.datosred.getNumeroNodos(); n++) {
							if (grasp.getDatosRed().getDemanda()[m][n] > 0
									&& grasp.getParescubiertos()[m][n] == j) {
								System.out.print("(" + m + "-" + n + ")\t");
							}
						}
					}
					System.out.println();
					fichero.println((((ArrayList) grasp.getRutasFinales()
							.get(i)).get(0)).toString());
					fichero.println("Tiempo: "
							+ (((ArrayList) grasp.getRutasFinales().get(i))
									.get(1)).toString());
					fichero.print("Demanda cubierta :"
							+ grasp.getDemandaPorLinea()[i]);
					fichero.print("\t");
					for (int m = 0; m < grasp.datosred.getNumeroNodos(); m++) {
						for (int n = 0; n < grasp.datosred.getNumeroNodos(); n++) {
							if (grasp.getDatosRed().getDemanda()[m][n] > 0
									&& grasp.getParescubiertos()[m][n] == j) {
								fichero.print("(" + m + "-" + n + ")\t");
							}
						}
					}
					fichero.println();
					ficherorutas.println("Ruta: " + j);
					for (int r = 0; r < ((ArrayList) (((ArrayList) grasp
							.getRutasFinales().get(i)).get(0))).size(); r++) {
						ficherorutas.print(((ArrayList) (((ArrayList) grasp
								.getRutasFinales().get(i)).get(0))).get(r)
								+ "\t");
					}
					ficherorutas.println();
				} else {
					if (i % 2 == 0) {
						System.out.println("No se realiza ruta de ida.");
						fichero.println("No se realiza ruta de ida.");
					} else {
						System.out.println("No se realiza ruta de vuelta.");
						fichero.println("No se realiza ruta de vuelta.");
					}
				}
				System.out.println();
				fichero.println();
			}// for
			System.out.println("Demanda Total : " + grasp.getDemandaTotal());
			System.out.println("Demanda Cubierta : " + demandacubierta);
			fichero.println("Demanda Total : " + grasp.getDemandaTotal());
			fichero.println("Demanda Cubierta : " + demandacubierta);
			System.out.println();
			fichero.println();
			if (demandacubierta < grasp.getDemandaTotal()
					* grasp.getDemandaPermitida()) {
				System.out
						.println("*ATENCION!!! DEMANDA MÍNIMA NO CUBIERTA POR LAS RUTAS*");
				fichero.println("*ATENCION!!! DEMANDA MÍNIMA NO CUBIERTA POR LAS RUTAS*");
			}// if
			fichero.close();
			ficherorutas.close();
		}// if
	}// escribirResultados
}
