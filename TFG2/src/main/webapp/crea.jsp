<!DOCTYPE html>
<%@page import="CreaRutas.*"%>
<%@page import="java.io.*"%>
<html>
<head>
<title>Visualizar soluci�n</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<%
		try {
			if (session.getAttribute("localizacion").equals("pais")) {
				RutaGrande rutagrande = new RutaGrande(
						(String) session.getAttribute("distancias"),
						(String) session.getAttribute("demandas"),
						(String) session.getAttribute("flota"),
						(String) session.getAttribute("paradas"),
						(String) session.getAttribute("log"),
						(String) session.getAttribute("logdetallado"),
						(String) session.getAttribute("save"),
						Integer.parseInt(request.getParameter("distmaxdia")),
						Integer.parseInt(request.getParameter("numiter")),
						Double.parseDouble(request.getParameter("demper")),
						Boolean.parseBoolean(request.getParameter("volver")),
						Boolean.parseBoolean(request.getParameter("carga")),
						Integer.parseInt(request.getParameter("tiemedpar")));
			} else if (session.getAttribute("localizacion")
					.equals("ciudad")) {
				if (Boolean.parseBoolean(request.getParameter("carga"))) {
					Grasp grasp;
					if ((Boolean) session.getAttribute("rutassemi")) {
						grasp = new GraspCarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								(String) session.getAttribute("semi"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("tiemax")),
								Integer.parseInt(request
										.getParameter("tiemin")), Integer
										.parseInt(request
												.getParameter("numiter")),
								Double.parseDouble(request
										.getParameter("demper")), Integer
										.parseInt(request
												.getParameter("tiemedpar")));
					} else if ((Boolean) session.getAttribute("manual")) {
						grasp = new GraspCarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("tiemax")),
								Integer.parseInt(request
										.getParameter("tiemin")), Double
										.parseDouble(request
												.getParameter("demper")),
								Integer.parseInt(request
										.getParameter("tiemedpar")),
								(String) session.getAttribute("saverutas"));
					} else {
						grasp = new GraspCarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("lintot")),
								Integer.parseInt(request
										.getParameter("tiemax")), Integer
										.parseInt(request
												.getParameter("tiemin")),
								Integer.parseInt(request
										.getParameter("numiter")), Double
										.parseDouble(request
												.getParameter("demper")),
								Boolean.parseBoolean(request
										.getParameter("volver")), Integer
										.parseInt(request
												.getParameter("tiemedpar")));
					}
					AsignaTransportes asignaTransportes = new AsignaTransportes(
							grasp);
					if ((Boolean) session.getAttribute("manual")) {
						if ((Boolean) session.getAttribute("mejmanual")) {
							asignaTransportes
									.asigna((String) session
											.getAttribute("save"),
											(String) session
													.getAttribute("saverutas"),
											grasp, true);
						} else {
							asignaTransportes.asigna((String) session
									.getAttribute("save"), (String) session
									.getAttribute("saverutas"), grasp,
									false);
						}
					} else if ((Boolean) session.getAttribute("rutassemi")) {
						if ((Boolean) session.getAttribute("mejsemi")) {
							asignaTransportes
									.asigna((String) session
											.getAttribute("save"),
											(String) session
													.getAttribute("saverutas"),
											grasp, true);
						} else {
							asignaTransportes.asigna((String) session
									.getAttribute("save"), (String) session
									.getAttribute("saverutas"), grasp,
									false);
						}
					} else {
						asignaTransportes.asigna(
								(String) session.getAttribute("save"),
								(String) session.getAttribute("saverutas"),
								grasp, true);
					}
				} else if (!Boolean.parseBoolean(request
						.getParameter("carga"))) {
					Grasp grasp;
					if ((Boolean) session.getAttribute("rutassemi")) {
						grasp = new GraspDescarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								(String) session.getAttribute("semi"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("tiemax")),
								Integer.parseInt(request
										.getParameter("tiemin")), Integer
										.parseInt(request
												.getParameter("numiter")),
								Double.parseDouble(request
										.getParameter("demper")), Integer
										.parseInt(request
												.getParameter("tiemedpar")));
					} else if ((Boolean) session.getAttribute("manual")) {
						grasp = new GraspDescarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("tiemax")),
								Integer.parseInt(request
										.getParameter("tiemin")), Double
										.parseDouble(request
												.getParameter("demper")),
								Integer.parseInt(request
										.getParameter("tiemedpar")),
								(String) session.getAttribute("saverutas"));
					} else {
						grasp = new GraspDescarga((String) session
								.getAttribute("distancias"),
								(String) session.getAttribute("demandas"),
								(String) session.getAttribute("flota"),
								(String) session.getAttribute("paradas"),
								(String) session.getAttribute("tiempo"),
								(String) session.getAttribute("log"),
								(String) session
										.getAttribute("logdetallado"),
								(String) session.getAttribute("save"),
								Integer.parseInt(request
										.getParameter("maxpara")), Integer
										.parseInt(request
												.getParameter("lintot")),
								Integer.parseInt(request
										.getParameter("tiemax")), Integer
										.parseInt(request
												.getParameter("tiemin")),
								Integer.parseInt(request
										.getParameter("numiter")), Double
										.parseDouble(request
												.getParameter("demper")),
								Boolean.parseBoolean(request
										.getParameter("volver")), Integer
										.parseInt(request
												.getParameter("tiemedpar")));
					}
					AsignaTransportes asignaTransportes = new AsignaTransportes(
							grasp);

					if ((Boolean) session.getAttribute("manual")) {
						if ((Boolean) session.getAttribute("mejmanual")) {
							asignaTransportes
									.asigna((String) session
											.getAttribute("save"),
											(String) session
													.getAttribute("saverutas"),
											grasp, true);
						} else {
							asignaTransportes.asigna((String) session
									.getAttribute("save"), (String) session
									.getAttribute("saverutas"), grasp,
									false);
						}
					} else if ((Boolean) session.getAttribute("rutassemi")) {
						if ((Boolean) session.getAttribute("mejsemi")) {
							asignaTransportes
									.asigna((String) session
											.getAttribute("save"),
											(String) session
													.getAttribute("saverutas"),
											grasp, true);
						} else {
							asignaTransportes.asigna((String) session
									.getAttribute("save"), (String) session
									.getAttribute("saverutas"), grasp,
									false);
						}
					} else {
						asignaTransportes.asigna(
								(String) session.getAttribute("save"),
								(String) session.getAttribute("saverutas"),
								grasp, true);
					}
				}//else if
			}//else if

			String s;
			BufferedReader archivoRed = new BufferedReader(new FileReader(
					(String) session.getAttribute("save")));
			s = archivoRed.readLine();// Lee la primera linea del archivo de red
			// Obtenemos el n�mero de nodos del archivo de red
			while ((s = archivoRed.readLine()) != null) {// Mientras haya lineas
				// leo la linea del fichero
				out.println(s);
	%><br>
	<%
		}// fin while

			archivoRed.close();
		} catch (FileNotFoundException e) {
			out.print("LOCALIZACI�N DE FICHEROS INCORRECTA. VUELVA A INTENTARLO");
	%><%@ include file="archivos.html"%>
	<%
		} catch (Exception e) {
			out.println("NO ES POSIBLE CALCULAR UNA SOLUCI�N CON ESTOS PAR�METROS");
	%><br>
	<%
		}
	%>

	<a href="index.html"><font color="blue">Inicio</font></a>
</body>
</html>
